package com.jfxtrade.account.balance.web;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 20-09-2014
 * Time: 0:49
 * To change this template use File | Settings | File Templates.
 */
public class ValueObject {

    private float value;
    private Long id;




    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }
}
