package com.jfxtrade.account.balance.web;

import com.jfxtrade.account.balance.Balance;
import com.jfxtrade.account.movements.model.Movement;
import com.jfxtrade.account.movements.model.MovementType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.mapreduce.MapReduceResults;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.match;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;
import static org.springframework.data.mongodb.core.query.Criteria.where;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 19-09-2014
 * Time: 23:11
 * To change this template use File | Settings | File Templates.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:testContext.xml"})
public class BalanceControllerTest {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Test
    public void testMovementsAggregation() {
        String mapFunction ="function() {" +
                "    emit(this.accountId, this.ammount);" +
                "};";

        String reduceFunction = "function(accountId, ammounts) {" +
                "    var ammount = 0;" +
                "    for(var i=0; i<ammounts.length; i++) {" +
                "        ammount +=ammounts[i];" +
                "    }" +
                "    return ammount" +
                "};";


        Query depositsQuery= new Query(where("accountId").is(250l).and("type").is("DEPOSIT"));
        Query withdrawQuery= new Query(where("accountId").is(250l).and("type").is(MovementType.WITHDRAW));
        Query tradeQuery= new Query(where("accountId").is(250l).and("type").is(MovementType.TRADE));

        MapReduceResults<ValueObject> results = mongoTemplate.mapReduce(depositsQuery, "movement", mapFunction, reduceFunction, ValueObject.class);
        for ( ValueObject valueObject : results ) {
            System.out.println("Deposits:" + valueObject.getValue());
        }
        results = mongoTemplate.mapReduce(withdrawQuery, "movement", mapFunction, reduceFunction, ValueObject.class);
        for ( ValueObject valueObject : results ) {
            System.out.println("Withdraws:" +valueObject.getValue());
        }
        results = mongoTemplate.mapReduce(tradeQuery, "movement", mapFunction, reduceFunction, ValueObject.class);
        for ( ValueObject valueObject : results ) {
            System.out.println("Tardes:"+ valueObject.getValue());
        }






    }
}
