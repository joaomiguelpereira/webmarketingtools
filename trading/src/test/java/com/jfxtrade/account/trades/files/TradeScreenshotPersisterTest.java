package com.jfxtrade.account.trades.files;

import com.jfxtrade.account.trades.model.Trade;
import com.jfxtrade.account.trades.model.TradeScreenshot;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.UUID;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 21-09-2014
 * Time: 19:27
 * To change this template use File | Settings | File Templates.
 */
public class TradeScreenshotPersisterTest {

    @Test
    public void testCreateBaseDir() {
        TradeScreenshotPersister persister = new TradeScreenshotPersister();
        persister.setBasePath("/jftrade_db/screenshots");
        Trade trade = new Trade();
        trade.setId(UUID.randomUUID().toString());
        trade.setAccountId(250);


        TradeScreenshot tradeScreenshot = new TradeScreenshot();
        tradeScreenshot.setId(UUID.randomUUID().toString());

        tradeScreenshot.setUrl("/account/"+250+"/trades/"+trade.getId()+"/screenshots/"+tradeScreenshot.getId());
        persister.saveScreenshot(trade, tradeScreenshot, null);


    }
}
