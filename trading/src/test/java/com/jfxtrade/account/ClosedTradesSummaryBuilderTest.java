package com.jfxtrade.account;

import static org.fest.assertions.Assertions.*;

import com.jfxtrade.account.trades.model.ClosedTradesSummaryBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 30-09-2014
 * Time: 0:33
 * To change this template use File | Settings | File Templates.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:testContext.xml"})
public class ClosedTradesSummaryBuilderTest {

    @Autowired
    private MongoTemplate mongoTemplate;
    private ClosedTradesSummaryBuilder sut;

    @Before
    public void setup() {
        this.sut = new ClosedTradesSummaryBuilder(mongoTemplate);
    }


    @Test
    public void shouldCount() {
        assertThat(sut.buildClosedSummary(450).getClosedTrades()).isEqualTo(3);
    }

    @Test
    public void shouldCountWinners() {
        assertThat(sut.buildClosedSummary(450).getWinners()).isEqualTo(2);
    }

    @Test
    public void shouldCountLoosers() {
        assertThat(sut.buildClosedSummary(450).getLoosers()).isEqualTo(1);
    }
    @Test
    public void shouldAverageRMultiple() {
        assertThat(sut.buildClosedSummary(450).getAverageRmultiple()).isEqualTo(0.3333333333333333);
    }
    @Test
    public void shouldAverageNetRMultiple() {
        assertThat(sut.buildClosedSummary(450).getAverageNetRmultiple()).isEqualTo(0.3233333428700765);
    }
    @Test
    public void shouldCountTradesPerDay() {
        assertThat(sut.buildClosedSummary(450).getTradesPerDay()).isEqualTo(0.12);
    }


    @Test
    public void shouldCalculateExpectunity() {

        assertThat(sut.buildClosedSummary(450).getExpectunity()).isEqualTo(0.03880000114440917);
    }
    @Test
    public void shouldCalculateNetexpectunity() {

        assertThat(sut.buildClosedSummary(450).getNetexpectunity()).isEqualTo(0.03880000114440917);
    }

    @Test
    public void shouldCalulateAverageWinningMaer() {
        assertThat(sut.buildClosedSummary(450).getAverageWinnerMaer()).isEqualTo(0.6149999797344208);

    }
    @Test
    public void shouldCalulateAverageLoosingMaer() {
        assertThat(sut.buildClosedSummary(450).getAverageLooserMaer()).isEqualTo(1);

    }


    @Test
    public void shouldCalulateAverageWinningDuration() {
        assertThat(sut.buildClosedSummary(450).getAverageWinnerDuration()).isEqualTo(0.5);

    }
    @Test
    public void shouldCalulateAverageLoosingDuration() {
        assertThat(sut.buildClosedSummary(450).getAverageLooserDuration()).isEqualTo(1);

    }
    @Test
    public void shouldCalulateAverageDuration() {
        assertThat(sut.buildClosedSummary(450).getAverageDuration()).isEqualTo(0.6666666666666666);

    }

    @Test
    public void shouldWinnerSum() {
        assertThat(sut.buildClosedSummary(450).getWinnerSum()).isEqualTo(17.729999542236328);
    }
    @Test
    public void shouldLooserSum() {
        assertThat(sut.buildClosedSummary(450).getLooserSum()).isEqualTo(-35.6510009765625);

    }

    @Test
    public void shouldCalculateBiggestWinner() {
        assertThat(sut.buildClosedSummary(450).getBiggestWinner()).isEqualTo(8.989999771118164);
    }

    @Test
    public void shouldCalculateBiggestLoose() {
        assertThat(sut.buildClosedSummary(450).getBiggestLooser()).isEqualTo(-35.6510009765625);

    }
    @Test
    public void shouldCalculateTransactionCosts() {

        assertThat(sut.buildClosedSummary(450).getTransactionCosts()).isEqualTo(-0.2200000025331974);
    }

    @Test
    public void shouldCalculateTotalWinnerRmultiple() {

        assertThat(sut.buildClosedSummary(450).getTotalWinnerRmultiple()).isEqualTo(-0.2200000025331974);

    }

    @Test
    public void shouldCalculateTotalWinnerNetRmultiple() {

        assertThat(sut.buildClosedSummary(450).getTotalWinnerNetrmultiple()).isEqualTo(1.9700000286102295);

    }


    @Test
    public void shouldCalculateTotalLooserRmultiple() {

        assertThat(sut.buildClosedSummary(450).getTotalLooserRmultiple()).isEqualTo(-1);

    }

    @Test
    public void shouldCalculateTotalLooserNetRmultiple() {

        assertThat(sut.buildClosedSummary(450).getTotalLooserNetrmultiple()).isEqualTo(-1);

    }


    @Test
    public void shouldCalculateAverageWinnerrmultiple() {

        assertThat(sut.buildClosedSummary(450).getAverageWinnerRmultiple()).isEqualTo(1);

    }

    @Test
    public void shouldCalculateAverageLooserrmultiple() {

        assertThat(sut.buildClosedSummary(450).getAverageLooserRmultiple()).isEqualTo(-1);

    }












}
