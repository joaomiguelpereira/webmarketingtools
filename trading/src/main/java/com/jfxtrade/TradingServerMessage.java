package com.jfxtrade;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 19-09-2014
 * Time: 21:54
 * To change this template use File | Settings | File Templates.
 */
public abstract class TradingServerMessage {
    private final String message;


    public TradingServerMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
    public String getType() {
        return this.getClass().getSimpleName();
    }
}
