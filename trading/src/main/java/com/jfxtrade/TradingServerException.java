package com.jfxtrade;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 22-09-2014
 * Time: 22:39
 * To change this template use File | Settings | File Templates.
 */
public class TradingServerException extends RuntimeException{
    public TradingServerException(String s) {
        super(s);
    }

    public TradingServerException(String s, IOException e) {
        super(s,e);
    }
}
