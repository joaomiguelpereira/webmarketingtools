package com.jfxtrade;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.embedded.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import javax.servlet.MultipartConfigElement;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 16-09-2014
 * Time: 21:37
 * To change this template use File | Settings | File Templates.
 */
@Configuration
@EnableMongoRepositories(basePackages = "com.jfxtrade")
public class TradingConfig {


}
