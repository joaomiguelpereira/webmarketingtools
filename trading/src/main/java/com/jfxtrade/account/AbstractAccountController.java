package com.jfxtrade.account;

import com.pluralsense.mkktools.accounts.web.AccountUserAwareController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.transaction.Transactional;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 18-09-2014
 * Time: 22:11
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping(value = "/api/trading/account/{accountId}/")
@Transactional
public abstract class AbstractAccountController extends AccountUserAwareController  {



}
