package com.jfxtrade.account.instruments.web;

import com.jfxtrade.TradingServerSuccessMessage;
import com.jfxtrade.account.AbstractAccountController;
import com.jfxtrade.account.instruments.model.Instrument;
import com.jfxtrade.account.instruments.model.InstrumentsRepository;
import com.pluralsense.mkktools.accounts.Account;
import com.pluralsense.mkktools.accounts.AccountUser;
import com.pluralsense.mkktools.accounts.web.AccountUserAwareController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 16-09-2014
 * Time: 20:56
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class InstrumentsController extends AbstractAccountController {


    @Autowired
    private InstrumentsRepository repository;


    @RequestMapping(method = RequestMethod.GET, value = "/instruments")
    @ResponseBody
    public List<Instrument> index(@PathVariable("accountId") Long accountId) {
        Account account = getAccount(accountId);
        return repository.findByAccountId(accountId);
    }

    /*
    @RequestMapping(method = RequestMethod.GET, value = "/instruments")
    @ResponseBody
    public List<Instrument> mergeInstruments(@PathVariable("accountId") Long accountId) {
        Account account = getAccount(accountId);
        return repository.findByAccountId(accountId);

    } */

    @RequestMapping(method = RequestMethod.POST, value = "/instruments")
    @ResponseBody
    public TradingServerSuccessMessage save(@PathVariable("accountId") Long accountId, @RequestBody Instrument instrument) {
        Account account = getAccount(accountId);


        instrument.setAccountId(account.getId());
        repository.save(instrument);
        return new TradingServerSuccessMessage("Instrument Created!");

    }
    @RequestMapping(value = "/instruments/{symbol}", method = RequestMethod.POST)
    @ResponseBody
    public TradingServerSuccessMessage update(@PathVariable("accountId") Long accountId, @RequestBody Instrument instrument) {
        Account account = getAccount(accountId);
        instrument.setAccountId(accountId);
        repository.save(instrument);
        return new TradingServerSuccessMessage("Instrument Updated!");

    }


    @RequestMapping(value = "/instruments/{symbol}", method = RequestMethod.GET)
    @ResponseBody
    public Instrument findBySymbol(@PathVariable("accountId") Long accountId, @PathVariable(value = "symbol") String symbol) {
        //TODO: MOVE TO INTERCEPTOR
        Account account = getAccount(accountId);
        return repository.findByAccountIdAndSymbol(account.getId(), symbol);
    }

    @RequestMapping(value = "/instruments/{symbol}", method = RequestMethod.DELETE)
    @ResponseBody
    public TradingServerSuccessMessage delete(@PathVariable("accountId") Long accountId, @PathVariable(value = "symbol") String symbol) {

        Instrument instrument = repository.findByAccountIdAndSymbol(getAccount(accountId).getId(), symbol);
        if ( instrument != null) {
            repository.delete(instrument);
            return new TradingServerSuccessMessage("Instrument removed!");
        }
        return new TradingServerSuccessMessage("Instrument not found");


    }




}
