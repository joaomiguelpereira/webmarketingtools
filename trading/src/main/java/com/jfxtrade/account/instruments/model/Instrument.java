package com.jfxtrade.account.instruments.model;

import com.pluralsense.mkktools.persistence.AbstractMongoEntity;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 16-09-2014
 * Time: 21:01
 * To change this template use File | Settings | File Templates.
 */

public class Instrument extends AbstractMongoEntity {


    private String symbol;
    private float pipPrice;
    private float pipSize;
    private String description;
    private Long accountId;

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public float getPipPrice() {
        return pipPrice;
    }

    public void setPipPrice(float pipPrice) {
        this.pipPrice = pipPrice;
    }

    public float getPipSize() {
        return pipSize;
    }

    public void setPipSize(float pipSize) {
        this.pipSize = pipSize;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }
}
