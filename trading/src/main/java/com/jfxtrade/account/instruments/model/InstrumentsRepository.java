package com.jfxtrade.account.instruments.model;

import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 16-09-2014
 * Time: 21:02
 * To change this template use File | Settings | File Templates.
 */

public interface InstrumentsRepository extends PagingAndSortingRepository<Instrument, String> {


    Instrument findByAccountIdAndSymbol(Long accountId, String symbol);

    List<Instrument> findByAccountId(Long accountId);
}
