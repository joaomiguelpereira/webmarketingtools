package com.jfxtrade.account.summary;

import com.jfxtrade.account.trades.model.Trade;
import com.jfxtrade.account.trades.model.TradeStatus;
import com.pluralsense.mkktools.accounts.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import static org.springframework.data.mongodb.core.query.Criteria.where;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 04-10-2014
 * Time: 15:58
 * To change this template use File | Settings | File Templates.
 */
@Component
public class AccountSummaryBuilder {
    @Autowired
    private MongoTemplate mongoTemplate;

    public AccountSummary buildFor(long account) {
        AccountSummary summary;

        summary = new AccountSummary();
        summary.setOpenTrades(openTrades(account));
        summary.setClosedTrades(closedTrades(account));
        summary.setPlannedTrades(plannedTrades(account));

        return summary;

    }

    private long closedTrades(long id) {

        Query closedTrades = new Query(where("accountId").is(id).and("status").is(TradeStatus.CLOSED));
        return mongoTemplate.count(closedTrades, Trade.class);

    }

    private long plannedTrades(long id) {

        Query closedTrades = new Query(where("accountId").is(id).and("status").is(TradeStatus.PLANNED));
        return mongoTemplate.count(closedTrades, Trade.class);

    }


    private long openTrades(long id) {

            Query closedTrades = new Query(where("accountId").is(id).and("status").is(TradeStatus.OPEN));
            return mongoTemplate.count(closedTrades, Trade.class);


    }

}
