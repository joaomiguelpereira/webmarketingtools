package com.jfxtrade.account.summary;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 04-10-2014
 * Time: 15:55
 * To change this template use File | Settings | File Templates.
 */
public class AccountSummary {
    private long openTrades;
    private long closedTrades;
    private long plannedTrades;

    public void setOpenTrades(long openTrades) {
        this.openTrades = openTrades;
    }

    public long getOpenTrades() {
        return openTrades;
    }

    public void setClosedTrades(long closedTrades) {
        this.closedTrades = closedTrades;
    }

    public long getClosedTrades() {
        return closedTrades;
    }

    public void setPlannedTrades(long plannedTrades) {
        this.plannedTrades = plannedTrades;
    }

    public long getPlannedTrades() {
        return plannedTrades;
    }
}
