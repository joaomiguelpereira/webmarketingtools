package com.jfxtrade.account.summary.web;

import com.jfxtrade.account.AbstractAccountController;
import com.jfxtrade.account.summary.AccountSummary;
import com.jfxtrade.account.summary.AccountSummaryBuilder;
import com.pluralsense.mkktools.accounts.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.transaction.Transactional;


/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 04-10-2014
 * Time: 15:55
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class AccountSummaryController extends AbstractAccountController {


    @Autowired
    private AccountSummaryBuilder builder;

    @RequestMapping(method = RequestMethod.GET, value = "/summary")
    @ResponseBody
    @Transactional
    public AccountSummary index(@PathVariable("accountId") Long accountId) {

        Account account = getAccount(accountId);
        return builder.buildFor(accountId);

    }
}
