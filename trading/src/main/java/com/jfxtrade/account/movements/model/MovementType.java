package com.jfxtrade.account.movements.model;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 19-09-2014
 * Time: 22:51
 * To change this template use File | Settings | File Templates.
 */
public enum MovementType {
    WITHDRAW,
    DEPOSIT,
    TRADE,
    ROLLOVER;
}
