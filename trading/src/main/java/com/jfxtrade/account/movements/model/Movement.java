package com.jfxtrade.account.movements.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.jfxtrade.utils.json.CustomJsonDateDeserializer;
import com.jfxtrade.utils.json.CustomJsonDateSerializer;
import com.pluralsense.mkktools.persistence.AbstractMongoEntity;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 18-09-2014
 * Time: 22:06
 * To change this template use File | Settings | File Templates.
 */
public class Movement extends AbstractMongoEntity {


    private long accountId;
    private double ammount;
    private MovementType type;

    @JsonDeserialize(using = CustomJsonDateDeserializer.class)
    @JsonSerialize(using = CustomJsonDateSerializer.class)
    private Date date;
    private String description;

    public double getAmmount() {
        return ammount;
    }

    public void setAmmount(double ammount) {
        this.ammount = ammount;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    public MovementType getType() {
        return type;
    }

    public void setType(MovementType type) {
        this.type = type;
    }
}
