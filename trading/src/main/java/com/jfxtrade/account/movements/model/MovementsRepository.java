package com.jfxtrade.account.movements.model;

import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 18-09-2014
 * Time: 22:08
 * To change this template use File | Settings | File Templates.
 */
public interface MovementsRepository extends PagingAndSortingRepository<Movement, String> {

    List<Movement> findByAccountId(Long accountId);

    Movement findByAccountIdAndId(Long accountId, String movementId);
}
