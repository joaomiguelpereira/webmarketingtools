package com.jfxtrade.account.movements.web;

import com.jfxtrade.TradingServerErrorMessage;
import com.jfxtrade.TradingServerMessage;
import com.jfxtrade.TradingServerSuccessMessage;
import com.jfxtrade.account.AbstractAccountController;
import com.jfxtrade.account.movements.model.Movement;
import com.jfxtrade.account.movements.model.MovementsRepository;
import com.pluralsense.mkktools.accounts.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 18-09-2014
 * Time: 22:10
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class MovementsController extends AbstractAccountController {

    @Autowired
    private MovementsRepository repository;


    @RequestMapping(method = RequestMethod.GET, value = "/movements")
    @ResponseBody
    @Transactional
    public List<Movement> index(@PathVariable("accountId") Long accountId) {

        //TODO: Create interceptor
        Account account = getAccount(accountId);
        return  repository.findByAccountId(accountId);


    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/movements/{id}")
    @ResponseBody
    @Transactional
    public TradingServerMessage delete(@PathVariable("accountId") Long accountId, @PathVariable("id") String movementId) {

        //TODO: Create interceptor
        Account account = getAccount(accountId);
        Movement movement = repository.findByAccountIdAndId(accountId, movementId);
        if (movement != null) {
            repository.delete(movement);
            return new TradingServerSuccessMessage("Movement deleted");
        }
        return new  TradingServerErrorMessage("The Movement could not be removed");



    }


    @RequestMapping(method = RequestMethod.POST, value = "/movements")
    @ResponseBody
    @Transactional
    public Movement save(@PathVariable("accountId") Long accountId, @RequestBody Movement theMovement) {
        Account account = getAccount(accountId);
        theMovement.setAccountId(accountId);
        return repository.save(theMovement);
    }

    /*@InitBinder
    private void dateBinder(HttpServletRequest request, ServletRequestDataBinder binder) {
        //The date format to parse or output your dates
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        //Create a new CustomDateEditor
        CustomDateEditor editor = new CustomDateEditor(dateFormat, true) {
            @Override
            public void setValue(Object value) {
                super.setValue(value);    //To change body of overridden methods use File | Settings | File Templates.
            }
        };
        //Register it as custom editor for the Date type
        binder.registerCustomEditor(Date.class, editor);
    } */


}
