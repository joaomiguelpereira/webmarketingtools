package com.jfxtrade.account.balance;

import com.jfxtrade.account.movements.model.MovementType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.mapreduce.MapReduceResults;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import static org.springframework.data.mongodb.core.query.Criteria.where;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 20-09-2014
 * Time: 11:11
 * To change this template use File | Settings | File Templates.
 */
@Component
public class BalanceBuilder {


    @Autowired
    private MongoTemplate mongoTemplate;

    //TODO: LEARN NOSQL Document
    public Balance buildForAccount(Long accountId) {
        String mapFunction ="function() {" +
                "    emit(this.accountId, this.ammount);" +
                "};";

        String reduceFunction = "function(accountId, ammounts) {" +
                "    var ammount = 0;" +
                "    for(var i=0; i<ammounts.length; i++) {" +
                "        ammount +=ammounts[i];" +
                "    }" +
                "    return ammount" +
                "};";


        Query depositsQuery= new Query(where("accountId").is(accountId).and("type").is(MovementType.DEPOSIT));
        Query rolloverQuery= new Query(where("accountId").is(accountId).and("type").is(MovementType.ROLLOVER));
        Query withdrawQuery= new Query(where("accountId").is(accountId).and("type").is(MovementType.WITHDRAW));
        Query tradeQuery= new Query(where("accountId").is(accountId).and("type").is(MovementType.TRADE));

        MapReduceResults<ValueObject> results = mongoTemplate.mapReduce(depositsQuery, "movement", mapFunction, reduceFunction, ValueObject.class);

        Balance balance = new Balance();

        for ( ValueObject valueObject : results ) {

            balance.setDeposits(valueObject.getValue());
            System.out.println("Deposits:" + valueObject.getValue());
        }
        results = mongoTemplate.mapReduce(withdrawQuery, "movement", mapFunction, reduceFunction, ValueObject.class);
        for ( ValueObject valueObject : results ) {

            balance.setWithdraws(valueObject.getValue());
        }

        results = mongoTemplate.mapReduce(tradeQuery, "movement", mapFunction, reduceFunction, ValueObject.class);
        for ( ValueObject valueObject : results ) {
            balance.setTrade(valueObject.getValue());
        }
        results = mongoTemplate.mapReduce(rolloverQuery, "movement", mapFunction, reduceFunction, ValueObject.class);
        for ( ValueObject valueObject : results ) {
            balance.setRollover(valueObject.getValue());
        }



        return balance;


    }
}
