package com.jfxtrade.account.balance;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 19-09-2014
 * Time: 22:21
 * To change this template use File | Settings | File Templates.
 */
public class Balance {
    private double deposits;
    private double withdraws;
    private double trade;
    private double rollover;
    private double balance = 0.0f;
    private double profit = 0.0f;

    public double getTotalGain() {
        return profit/(deposits-withdraws);
    }
    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public double getProfit() {
        return profit;
    }

    public void setProfit(double profit) {
        this.profit = profit;
    }

    public void setDeposits(double deposits) {
        this.deposits = deposits;
        this.balance += deposits;
    }

    public double getDeposits() {
        return deposits;
    }

    public void setWithdraws(double withdraws) {
        this.withdraws = withdraws;
        this.balance -= withdraws;
    }

    public double getWithdraws() {
        return withdraws;
    }

    public void setTrade(double trade) {
        this.trade = trade;
        this.balance += trade;
        this.profit += trade;
    }

    public double getTrade() {
        return trade;
    }

    public void setRollover(double rollover) {
        this.rollover = rollover;
        this.balance += rollover;
        this.profit += rollover;
    }

    public double getRollover() {
        return rollover;
    }


}
