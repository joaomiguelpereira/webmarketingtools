package com.jfxtrade.account.balance.web;

import com.jfxtrade.account.AbstractAccountController;
import com.jfxtrade.account.balance.Balance;
import com.jfxtrade.account.balance.BalanceBuilder;
import com.jfxtrade.account.movements.model.Movement;
import com.jfxtrade.account.movements.model.MovementType;
import com.jfxtrade.account.movements.model.MovementsRepository;
import com.pluralsense.mkktools.accounts.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.transaction.Transactional;

import static org.springframework.data.domain.Sort.Direction.*;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.*;
import static org.springframework.data.mongodb.core.query.Criteria.*;


/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 19-09-2014
 * Time: 22:23
 * To change this template use File | Settings | File Templates.
 */

@Controller
public class BalanceController extends AbstractAccountController {




    @Autowired
    private BalanceBuilder balanceBuilder;

    @RequestMapping(method = RequestMethod.GET, value = "/balance")
    @ResponseBody
    @Transactional
    public Balance index(@PathVariable("accountId") Long accountId) {

        //TODO: Create interceptor
        Account account = getAccount(accountId);


        return balanceBuilder.buildForAccount(accountId);

    }

}
