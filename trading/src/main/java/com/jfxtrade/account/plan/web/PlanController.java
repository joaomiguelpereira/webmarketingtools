package com.jfxtrade.account.plan.web;

import com.jfxtrade.account.plan.model.Plan;
import com.jfxtrade.account.plan.model.PlanRepository;
import com.pluralsense.mkktools.accounts.Account;
import com.pluralsense.mkktools.accounts.web.AccountUserAwareController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 18-09-2014
 * Time: 0:19
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping(value = "/api/trading/account/{accountId}/plan")
public class PlanController  extends AccountUserAwareController{

    @Autowired
    private PlanRepository repository;

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    @Transactional
    public Plan index(@PathVariable("accountId") Long accountId) {

        //just for checking security and stuff
        Account account = getAccount(accountId);

        Plan plan = repository.findByAccountId(accountId);

        if ( plan == null ) {
            plan = new Plan();
            plan.setAccountId(account.getId());
            repository.save(plan);
        }

        return plan;

    }
    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    @Transactional
    public Plan save(@PathVariable("accountId") Long accountId, @RequestBody Plan thePlan) {

        getAccount(accountId);


        thePlan.setAccountId(accountId);
        return repository.save(thePlan);
    }

}
