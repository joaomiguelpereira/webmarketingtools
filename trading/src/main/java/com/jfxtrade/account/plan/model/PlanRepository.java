package com.jfxtrade.account.plan.model;

import org.springframework.data.repository.CrudRepository;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 18-09-2014
 * Time: 0:25
 * To change this template use File | Settings | File Templates.
 */
public interface PlanRepository extends CrudRepository<Plan, String>{
    Plan findByAccountId(Long accountId);
}
