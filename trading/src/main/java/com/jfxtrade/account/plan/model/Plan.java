package com.jfxtrade.account.plan.model;

import com.pluralsense.mkktools.persistence.AbstractMongoEntity;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 18-09-2014
 * Time: 0:19
 * To change this template use File | Settings | File Templates.
 */
public class Plan extends AbstractMongoEntity {

    private long accountId;
    private String timeFrame;
    private String  entryStrategyName;
    private String entryStrategyDescription;

    private String exitStrategyName;
    private String exitStrategyDescription;

    public float getRmultiple() {
        return rmultiple;
    }

    public void setRmultiple(float rmultiple) {
        this.rmultiple = rmultiple;
    }

    public float getReliability() {
        return reliability;
    }

    public void setReliability(float reliability) {
        this.reliability = reliability;
    }

    private float rmultiple;
    private float reliability;

    private float monthlyReturn;
    private float anualReturn;

    private int maxConsecutiveLosses;

    public String getTimeFrame() {
        return timeFrame;
    }

    public void setTimeFrame(String timeFrame) {
        this.timeFrame = timeFrame;
    }

    public String getEntryStrategyName() {
        return entryStrategyName;
    }

    public void setEntryStrategyName(String entryStrategyName) {
        this.entryStrategyName = entryStrategyName;
    }

    public String getEntryStrategyDescription() {
        return entryStrategyDescription;
    }

    public void setEntryStrategyDescription(String entryStrategyDescription) {
        this.entryStrategyDescription = entryStrategyDescription;
    }

    public String getExitStrategyName() {
        return exitStrategyName;
    }

    public void setExitStrategyName(String exitStrategyName) {
        this.exitStrategyName = exitStrategyName;
    }

    public String getExitStrategyDescription() {
        return exitStrategyDescription;
    }

    public void setExitStrategyDescription(String exitStrategyDescription) {
        this.exitStrategyDescription = exitStrategyDescription;
    }

    public float getMonthlyReturn() {
        return monthlyReturn;
    }

    public void setMonthlyReturn(float monthlyReturn) {
        this.monthlyReturn = monthlyReturn;
    }

    public float getAnualReturn() {
        return anualReturn;
    }

    public void setAnualReturn(float anualReturn) {
        this.anualReturn = anualReturn;
    }

    public int getMaxConsecutiveLosses() {
        return maxConsecutiveLosses;
    }

    public void setMaxConsecutiveLosses(int maxConsecutiveLosses) {
        this.maxConsecutiveLosses = maxConsecutiveLosses;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    private String summary;


    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }
}

