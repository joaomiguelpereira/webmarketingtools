package com.jfxtrade.account.risk.model;

import org.springframework.data.repository.CrudRepository;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 16-09-2014
 * Time: 23:37
 * To change this template use File | Settings | File Templates.
 */

public interface RiskParametersRepository extends CrudRepository<RiskParameters, String> {

    RiskParameters findByAccountId(Long accountId);

}
