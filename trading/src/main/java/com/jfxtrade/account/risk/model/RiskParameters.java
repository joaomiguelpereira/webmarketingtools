package com.jfxtrade.account.risk.model;

import com.pluralsense.mkktools.persistence.AbstractMongoEntity;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 16-09-2014
 * Time: 23:35
 * To change this template use File | Settings | File Templates.
 */
public class RiskParameters extends AbstractMongoEntity {

    private long accountId;
    private float allowedRiskPerTrade;
    private float allowedRisk;

    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    public float getAllowedRiskPerTrade() {
        return allowedRiskPerTrade;
    }

    public void setAllowedRiskPerTrade(float allowedRiskPerTrade) {
        this.allowedRiskPerTrade = allowedRiskPerTrade;
    }

    public float getAllowedRisk() {
        return allowedRisk;
    }

    public void setAllowedRisk(float allowedRisk) {
        this.allowedRisk = allowedRisk;
    }
}
