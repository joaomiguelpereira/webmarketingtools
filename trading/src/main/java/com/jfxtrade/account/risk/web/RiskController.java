package com.jfxtrade.account.risk.web;

import com.jfxtrade.account.AbstractAccountController;
import com.jfxtrade.account.risk.model.RiskParameters;
import com.jfxtrade.account.risk.model.RiskParametersRepository;
import com.pluralsense.mkktools.accounts.Account;
import com.pluralsense.mkktools.accounts.AccountUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 16-09-2014
 * Time: 23:33
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class RiskController extends AbstractAccountController {

    @Autowired
    private RiskParametersRepository repository;
    

    @RequestMapping(method = RequestMethod.GET, value = "/risk")
    @ResponseBody
    @Transactional
    public RiskParameters index(@PathVariable("accountId") Long accountId) {

       Account account = getAccount(accountId);

        RiskParameters riskParameters = repository.findByAccountId(accountId);
        if ( riskParameters == null ) {
            riskParameters = new RiskParameters();
            riskParameters.setAccountId(accountId);
            riskParameters.setAllowedRisk(0.06f);
            riskParameters.setAllowedRiskPerTrade(0.02f);
            repository.save(riskParameters);
        }

        return riskParameters;
    }


    @RequestMapping(method = RequestMethod.POST, value = "/risk")
    @ResponseBody
    @Transactional
    public RiskParameters save(@PathVariable("accountId") Long accountId, @RequestBody RiskParameters theRiskParameters) {

        AccountUser user = getUser();
        Account account = accountRepository.findOne(accountId);

        if (account == null) {
            throw new RuntimeException("Account not found");
        }

        if (!user.getManagedAccounts().contains(account)) {
            throw new RuntimeException("Do not belong");
        }

        theRiskParameters.setAccountId(accountId);
        return repository.save(theRiskParameters);
    }


}
