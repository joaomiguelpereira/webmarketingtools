package com.jfxtrade.account.trades.model;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 20-09-2014
 * Time: 18:24
 * To change this template use File | Settings | File Templates.
 */
public interface TradesRepository extends PagingAndSortingRepository<Trade, String>{
    Trade findByAccountIdAndId(long accountId, String tradeId);

    List<Trade> findByAccountId(Long accountId, Pageable pageable);

    List<Trade> findByAccountIdAndStatus(Long accountId, TradeStatus status, Pageable pageable);
}
