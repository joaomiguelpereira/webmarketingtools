package com.jfxtrade.account.trades.model;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 30-09-2014
 * Time: 1:25
 * To change this template use File | Settings | File Templates.
 */
public class FloatValueObject {

    private double value;
    private Long id;




    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }
}

