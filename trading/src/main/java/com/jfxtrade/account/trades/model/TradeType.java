package com.jfxtrade.account.trades.model;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 21-09-2014
 * Time: 16:51
 * To change this template use File | Settings | File Templates.
 */
public enum TradeType {
    BUY,
    SELL
}
