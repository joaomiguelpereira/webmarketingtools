package com.jfxtrade.account.trades.web;

import com.jfxtrade.TradingServerErrorMessage;
import com.jfxtrade.TradingServerException;
import com.jfxtrade.TradingServerMessage;
import com.jfxtrade.TradingServerSuccessMessage;
import com.jfxtrade.account.AbstractAccountController;
import com.jfxtrade.account.balance.Balance;
import com.jfxtrade.account.balance.BalanceBuilder;
import com.jfxtrade.account.instruments.model.Instrument;
import com.jfxtrade.account.instruments.model.InstrumentsRepository;
import com.jfxtrade.account.movements.model.Movement;
import com.jfxtrade.account.movements.model.MovementType;
import com.jfxtrade.account.movements.model.MovementsRepository;
import com.jfxtrade.account.trades.files.TradeScreenshotPersister;
import com.jfxtrade.account.trades.model.*;
import com.pluralsense.mkktools.accounts.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 20-09-2014
 * Time: 18:23
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class TradesController extends AbstractAccountController {

    @Autowired
    private TradesRepository repository;

    @Autowired
    InstrumentsRepository instrumentsRepository;

    @Autowired
    MovementsRepository movementsRepository;

    @Autowired
    BalanceBuilder balanceBuilder;

    @Autowired
    TradeScreenshotPersister tradeScreenshotPersister;
    @Autowired
    private ClosedTradesSummaryBuilder closeTradesSummaryBuilder;


    @RequestMapping(method = RequestMethod.GET, value = "/trades")
    @ResponseBody
    public List<Trade> index(@PathVariable("accountId") Long accountId, @RequestParam("direction") String direction,
                             @RequestParam("fieldName") String fieldName,
                             @RequestParam("page") int page,
                             @RequestParam("pageSize") int pageSize,
                             @RequestParam("type") String type
                             ) {
        Account account = getAccount(accountId);

        TradeStatus status;
        Sort sort = new Sort(Sort.Direction.valueOf(direction),fieldName);
        Pageable pageable = new PageRequest(page, pageSize,sort);
        if ( !type.equals("ALL")) {
            status = TradeStatus.valueOf(type);
            return repository.findByAccountIdAndStatus(accountId, status,pageable);
        }
        return repository.findByAccountId(accountId,pageable);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/trades/closeSummary")
    @ResponseBody
    public CloseTradesSummary closeTradesSummary(@PathVariable("accountId") Long accountId) {
        Account account = getAccount(accountId);


        return closeTradesSummaryBuilder.buildClosedSummary(accountId);

        //return repository.findByAccountId(accountId);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/trades")
    @ResponseBody
    public Trade save(@PathVariable("accountId") Long accountId, @RequestBody Trade trade) {
        Account account = getAccount(accountId);
        trade.setAccountId(account.getId());
        //Time dependency
        updateTrade(trade);
        return repository.save(trade);


    }

    @RequestMapping(value = "/trades/{id}", method = RequestMethod.POST)
    @ResponseBody
    public Trade update(@PathVariable("accountId") Long accountId, @RequestBody Trade trade) {
        Account account = getAccount(accountId);
        trade.setAccountId(accountId);
        //Time dependency
        updateTrade(trade);
      return repository.save(trade);


    }

    private void updateTrade(Trade trade) {
        //Improve
        if (trade.getStatus().equals(TradeStatus.OPEN) && trade.getOpenRisk() == 0) {
            trade.setOpenRisk(trade.getRisk());

            for (TradeOrder tradeOrder : trade.getOrders() ) {
                tradeOrder.open();
            }
        }
        if (trade.getInstrumentAtClose() == null && trade.getStatus().equals(TradeStatus.CLOSED)) {

            Instrument instrument = instrumentsRepository.findByAccountIdAndSymbol(trade.getAccountId(), trade.getSymbol());
            if (instrument==null) {
                throw new TradingServerException("Instrument not found");
            }
            Instrument instrumentAtClose = new Instrument();
            instrumentAtClose.setSymbol(instrument.getSymbol());
            instrumentAtClose.setDescription(instrument.getDescription());
            instrumentAtClose.setPipPrice(instrument.getPipPrice());
            instrumentAtClose.setPipSize(instrument.getPipSize());
            trade.setInstrumentAtClose(instrumentAtClose);

            Movement movement = new Movement();
            movement.setAccountId(trade.getAccountId());
            movement.setDescription(trade.getType() + trade.getSymbol());
            movement.setDate(trade.getCloseTime());
            movement.setAmmount(trade.getProfitLoss());
            movement.setType(MovementType.TRADE);


            if (trade.getProfitLoss() > 0 ) {
                trade.setWinner(true);
            } else {
                trade.setWinner(false);
            }
            movementsRepository.save(movement);


        }


        //improve workflow
        long duration = 0;

        long orderDuration = 0;
        for (TradeOrder tradeOrder : trade.getOrders()) {
            orderDuration = tradeOrder.updateDuration();
            if ( orderDuration > duration ) {
                duration = orderDuration;
            }

        }

        trade.setDuration(duration);



        fixOpenRisk(trade);
    }

    private void fixOpenRisk(Trade trade) {
        if (trade.getOpenRisk() > 0 ) {
            for (TradeOrder order: trade.getOrders() ) {
                if (order.getOpenRisk() == 0 ) {
                    order.setOpenRisk(trade.getOpenRisk());
                }
            }
        }
    }


    @RequestMapping(value = "/trades/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Trade findBySymbol(@PathVariable("accountId") Long accountId, @PathVariable(value = "id") String id) {
        //TODO: MOVE TO INTERCEPTOR
        Account account = getAccount(accountId);
        return repository.findByAccountIdAndId(account.getId(), id);
    }

    @RequestMapping(value = "/trades/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public TradingServerMessage delete(@PathVariable("accountId") Long accountId, @PathVariable(value = "id") String id) {
        //todo: fix into intercptor
        Account account = getAccount(accountId);

        Trade trade = repository.findByAccountIdAndId(accountId, id);

        if (trade != null) {
            repository.delete(trade);
            tradeScreenshotPersister.removeTrade(accountId, id);


            return new TradingServerSuccessMessage("Trade removed!");
        }

        return new TradingServerErrorMessage("Trade not found");


    }



    /*
    Move into other controller
     */

    @RequestMapping(value = "/trades/{id}/screenshots/upload", method = RequestMethod.POST)
    @ResponseBody
    public TradeScreenshot uploadScreenShot(@PathVariable("accountId") Long accountId, @PathVariable("id") String id, @RequestParam("file") MultipartFile file) {
        Account account = getAccount(accountId);

        Trade trade = repository.findByAccountIdAndId(accountId, id);
        if (trade == null) {
            throw new TradingServerException("Account not found");
        }


        String screenshotId = UUID.randomUUID().toString();

        String url = String.format("/api/trading/account/%s/trades/%s/screenshots/%s", account.getId(), id, screenshotId);
        TradeScreenshot tradeScreenshot = new TradeScreenshot();
        tradeScreenshot.setUrl(url);
        tradeScreenshot.setTakenAt(new Date());
        tradeScreenshot.setId(screenshotId);
        tradeScreenshot.setOriginalFileName(file.getOriginalFilename());

        trade.addScreenshot(tradeScreenshot);
        repository.save(trade);


        tradeScreenshotPersister.saveScreenshot(trade, tradeScreenshot, file);


        return tradeScreenshot;

    }

    @RequestMapping(value = "/trades/{id}/screenshots/{screenshotId}", method = RequestMethod.DELETE)
    @ResponseBody
    public TradingServerMessage deleteScreenshot(@PathVariable("accountId") Long accountId, @PathVariable("id") String id, @PathVariable("screenshotId") String screenshotId) {
        Account account = getAccount(accountId);

        Trade trade = repository.findByAccountIdAndId(accountId, id);
        if (trade == null) {
            throw new TradingServerException("Account not found");
        }
        trade.removeScreenshot(screenshotId);
        repository.save(trade);

        tradeScreenshotPersister.remove(accountId, id, screenshotId);

        return new TradingServerSuccessMessage("Screenshot removed");


    }


    @RequestMapping(value = "/trades/{id}/screenshots/{screenshotId}", method = RequestMethod.GET)
    public ResponseEntity<byte[]> getScreenshot(@PathVariable("accountId") Long accountId, @PathVariable("id") String id, @PathVariable("screenshotId") String screenshotId) {
        Account account = getAccount(accountId);

        //
        Trade trade = repository.findByAccountIdAndId(accountId, id);
        if (trade == null) {
            throw new TradingServerException("Account not found");
        }


        byte[] file = null;
        //TODO: fix
        for (TradeScreenshot screenshot : trade.getScreenshots()) {
            if (screenshot.getId().equals(screenshotId)) {
                file = tradeScreenshotPersister.loadFile(accountId, id, screenshot);
                break;
            }
        }

        if (file == null) {
            throw new TradingServerException("Screenshot not found");
        }
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.IMAGE_PNG.IMAGE_PNG);

        return new ResponseEntity<byte[]>(file, headers, HttpStatus.CREATED);


    }


}
