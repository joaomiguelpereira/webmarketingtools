package com.jfxtrade.account.trades.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.jfxtrade.account.instruments.model.Instrument;
import com.jfxtrade.utils.json.CustomJsonDateDeserializer;
import com.jfxtrade.utils.json.CustomJsonDateSerializer;
import com.pluralsense.mkktools.persistence.AbstractMongoEntity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 20-09-2014
 * Time: 18:24
 * To change this template use File | Settings | File Templates.
 */
public class Trade extends AbstractMongoEntity {


    private double riskAmmount;
    private long accountId;
    private double entryPrice;
    private double previousPeriodHigh;
    private double previousPeriodLow;
    private int risk;
    private double rmultiple;
    private double slPrice;
    private double slackEntry;
    private double slackExit;

    @JsonDeserialize(using = CustomJsonDateDeserializer.class)
    @JsonSerialize(using = CustomJsonDateSerializer.class)
    private Date openTime;

    @JsonDeserialize(using = CustomJsonDateDeserializer.class)
    @JsonSerialize(using = CustomJsonDateSerializer.class)
    private Date closeTime;

    private int openRisk = 0;
    private int closeReward = 0;
    private int openDurationInMinutes;

    private TradeStatus status;
    private String symbol;
    private TradeType type;
    private List<TradeOrder> orders = new ArrayList<TradeOrder>(1);
    private List<TradeScreenshot> screenshots = new ArrayList<TradeScreenshot>(1);
    private String log;

    private double spread;
    private double comission;
    private double swap;
    private double profitLoss;
    private double rollover;
    private double maximumAdverseExcursion;
    private long duration;

    private double  transactionCostCurrency;
    private double transactionCostPips;
    private double afterCostsRmultiple;
    private boolean winner;

    public boolean isWinner() {
        return winner;
    }

    public void setWinner(boolean winner) {
        this.winner = winner;
    }

    public double getRiskAmmount() {
        return riskAmmount;
    }

    public void setRiskAmmount(double riskAmmount) {
        this.riskAmmount = riskAmmount;
    }

    public double getTransactionCostCurrency() {
        return transactionCostCurrency;
    }

    public void setTransactionCostCurrency(double transactionCostCurrency) {
        this.transactionCostCurrency = transactionCostCurrency;
    }

    public double getTransactionCostPips() {
        return transactionCostPips;
    }

    public void setTransactionCostPips(double transactionCostPips) {
        this.transactionCostPips = transactionCostPips;
    }

    public double getAfterCostsRmultiple() {
        return afterCostsRmultiple;
    }

    public void setAfterCostsRmultiple(double afterCostsRmultiple) {
        this.afterCostsRmultiple = afterCostsRmultiple;
    }

    private Instrument instrumentAtClose;

    public Instrument getInstrumentAtClose() {
        return instrumentAtClose;
    }

    public void setInstrumentAtClose(Instrument instrumentAtClose) {
        this.instrumentAtClose = instrumentAtClose;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public double getMaximumAdverseExcursion() {
        return maximumAdverseExcursion;
    }

    public void setMaximumAdverseExcursion(double maximumAdverseExcursion) {
        this.maximumAdverseExcursion = maximumAdverseExcursion;
    }

    public double getRollover() {
        return rollover;
    }

    public void setRollover(double rollover) {
        this.rollover = rollover;
    }

    public double getSpread() {
        return spread;
    }

    public void setSpread(double spread) {
        this.spread = spread;
    }

    public double getComission() {
        return comission;
    }

    public void setComission(double comission) {
        this.comission = comission;
    }

    public double getSwap() {
        return swap;
    }

    public void setSwap(double swap) {
        this.swap = swap;
    }

    public double getProfitLoss() {
        return profitLoss;
    }

    public void setProfitLoss(double profitLoss) {
        this.profitLoss = profitLoss;
    }

    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    public double getEntryPrice() {
        return entryPrice;
    }

    public void setEntryPrice(double entryPrice) {
        this.entryPrice = entryPrice;
    }

    public double getPreviousPeriodHigh() {
        return previousPeriodHigh;
    }

    public void setPreviousPeriodHigh(double previousPeriodHigh) {
        this.previousPeriodHigh = previousPeriodHigh;
    }

    public int getRisk() {
        return risk;
    }

    public void setRisk(int risk) {
        this.risk = risk;
    }

    public double getRmultiple() {
        return rmultiple;
    }

    public void setRmultiple(double rmultiple) {
        this.rmultiple = rmultiple;
    }

    public double getSlPrice() {
        return slPrice;
    }

    public void setSlPrice(double slPrice) {
        this.slPrice = slPrice;
    }

    public double getSlackEntry() {
        return slackEntry;
    }

    public void setSlackEntry(double slackEntry) {
        this.slackEntry = slackEntry;
    }

    public double getSlackExit() {
        return slackExit;
    }

    public void setSlackExit(double slackExit) {
        this.slackExit = slackExit;
    }

    public TradeStatus getStatus() {
        return status;
    }

    public void setStatus(TradeStatus status) {
        this.status = status;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public TradeType getType() {
        return type;
    }

    public void setType(TradeType type) {
        this.type = type;
    }


    public Date getOpenTime() {
        return openTime;
    }

    public void setOpenTime(Date openTime) {
        this.openTime = openTime;
    }

    public Date getCloseTime() {
        return closeTime;
    }

    public void setCloseTime(Date closeTime) {
        this.closeTime = closeTime;
    }

    public int getOpenRisk() {
        return openRisk;
    }

    public void setOpenRisk(int openRisk) {
        this.openRisk = openRisk;
    }

    public int getCloseReward() {
        return closeReward;
    }

    public void setCloseReward(int closeReward) {
        this.closeReward = closeReward;
    }

    public int getOpenDurationInMinutes() {
        return openDurationInMinutes;
    }

    public void setOpenDurationInMinutes(int openDurationInMinutes) {
        this.openDurationInMinutes = openDurationInMinutes;
    }


    public List<TradeOrder> getOrders() {
        return orders;
    }

    public void setOrders(List<TradeOrder> orders) {
        this.orders = orders;
    }

    public double getPreviousPeriodLow() {
        return previousPeriodLow;
    }

    public void setPreviousPeriodLow(double previousPeriodLow) {
        this.previousPeriodLow = previousPeriodLow;
    }

    public String getLog() {
        return log;
    }

    public void setLog(String log) {
        this.log = log;
    }

    public List<TradeScreenshot> getScreenshots() {
        return screenshots;
    }



    public void setScreenshots(List<TradeScreenshot> screenshots) {
        this.screenshots = screenshots;
    }

    public void addScreenshot(TradeScreenshot tradeScreenshot) {
        if (this.screenshots == null) {
            this.screenshots = new ArrayList<TradeScreenshot>(1);
        }
        this.screenshots.add(tradeScreenshot);

    }

    public void removeScreenshot(String screenshotId) {
        TradeScreenshot theScreenshot = null;
        for (TradeScreenshot screenshot : screenshots) {
            if (screenshot.getId().equals(screenshotId)) {
                theScreenshot = screenshot;
                break;
            }
        }
        if (theScreenshot != null) {
            screenshots.remove(theScreenshot);
        }
    }
}
