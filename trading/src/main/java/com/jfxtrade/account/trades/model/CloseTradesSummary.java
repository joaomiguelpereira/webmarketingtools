package com.jfxtrade.account.trades.model;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 28-09-2014
 * Time: 22:13
 * To change this template use File | Settings | File Templates.
 */
public class CloseTradesSummary {

    private long closedTrades;
    private long winners;
    private long loosers;

    private double averageRmultiple;
    private double averageNetRmultiple;

    private double tradesPerDay;

    private double expectunity;
    private double netexpectunity;

    private double averageWinnerMaer;
    private double averageLooserMaer;

    private double averageWinnerDuration;
    private double averageLooserDuration;
    private double averageDuration;

    private double winnerSum;
    private double looserSum;

    private double biggestWinner;
    private double biggestLooser;

    private double transactionCosts;


    private double totalWinnerRmultiple;
    private double totalLooserRmultiple;

    private double totalWinnerNetrmultiple;
    private double totalLooserNetrmultiple;



    public double getWinnerRatio() {

        double ratio = 0;
        if ( closedTrades !=0 ) {
            ratio = Double.valueOf(winners)/Double.valueOf(closedTrades);
        }
        return ratio;


    }



    public double getLooserRatio() {
        double ratio = 0;
        if ( closedTrades!= 0 ) {

            ratio = Double.valueOf(loosers)/Double.valueOf(closedTrades);
        }
        return ratio;
    }

    public double getTraderRatio() {
        double ratio = 0;
        if ( biggestLooser > 0 ) {
            ratio = winnerSum/biggestLooser;
        }
        return ratio;
    }

    public double getAverageWinnerRmultiple() {
        return totalWinnerRmultiple/winners;
    }

    public double getAverageLooserRmultiple() {
        return totalLooserRmultiple/loosers;
    }

    public double getTotalLooserRmultiple() {
        return totalLooserRmultiple;
    }

    public void setTotalLooserRmultiple(double totalLooserRmultiple) {
        this.totalLooserRmultiple = totalLooserRmultiple;
    }

    public double getTotalWinnerNetrmultiple() {
        return totalWinnerNetrmultiple;
    }

    public double getTotalLooserNetrmultiple() {
        return totalLooserNetrmultiple;
    }

    public void setTotalLooserNetrmultiple(double totalLooserNetrmultiple) {
        this.totalLooserNetrmultiple = totalLooserNetrmultiple;
    }

    public void setTotalWinnerNetrmultiple(double totalWinnerNetrmultiple) {
        this.totalWinnerNetrmultiple = totalWinnerNetrmultiple;
    }


    public double getLooserSum() {
        return looserSum;
    }

    public void setLooserSum(double looserSum) {
        this.looserSum = looserSum;
    }

    public double getExpectunity() {
        return this.averageRmultiple * tradesPerDay;
    }
    public double getNetexpectunity() {
        return this.averageNetRmultiple * tradesPerDay;
    }



    public long getClosedTrades() {
        return closedTrades;
    }

    public void setClosedTrades(long closedTrades) {
        this.closedTrades = closedTrades;
    }

    public long getWinners() {
        return winners;
    }

    public void setWinners(long winners) {
        this.winners = winners;
    }

    public long getLoosers() {
        return loosers;
    }

    public void setLoosers(long loosers) {
        this.loosers = loosers;
    }

    public double getAverageRmultiple() {
        return averageRmultiple;
    }

    public void setAverageRmultiple(double averageRmultiple) {
        this.averageRmultiple = averageRmultiple;
    }

    public double getAverageNetRmultiple() {
        return averageNetRmultiple;
    }

    public void setAverageNetRmultiple(double averageNetRmultiple) {
        this.averageNetRmultiple = averageNetRmultiple;
    }

    public double getTradesPerDay() {
        return tradesPerDay;
    }

    public void setTradesPerDay(double tradesPerDay) {
        this.tradesPerDay = tradesPerDay;

    }



    public double getAverageWinnerMaer() {
        return averageWinnerMaer;
    }

    public void setAverageWinnerMaer(double averageWinnerMaer) {
        this.averageWinnerMaer = averageWinnerMaer;
    }

    public double getAverageLooserMaer() {
        return averageLooserMaer;
    }

    public void setAverageLooserMaer(double averageLooserMaer) {
        this.averageLooserMaer = averageLooserMaer;
    }

    public double getAverageWinnerDuration() {
        return averageWinnerDuration;
    }

    public void setAverageWinnerDuration(double averageWinnerDuration) {
        this.averageWinnerDuration = averageWinnerDuration;
    }

    public double getAverageLooserDuration() {
        return averageLooserDuration;
    }

    public void setAverageLooserDuration(double averageLooserDuration) {
        this.averageLooserDuration = averageLooserDuration;
    }

    public double getAverageDuration() {
        return averageDuration;
    }

    public void setAverageDuration(double averageDuration) {
        this.averageDuration = averageDuration;
    }

    public double getWinnerSum() {
        return winnerSum;
    }

    public void setWinnerSum(double winnerSum) {
        this.winnerSum = winnerSum;
    }

    public double getBiggestLooser() {
        return biggestLooser;
    }

    public void setBiggestLooser(double biggestLooser) {
        this.biggestLooser = biggestLooser;
    }

    public double getBiggestWinner() {
        return biggestWinner;
    }

    public void setBiggestWinner(double biggestWinner) {
        this.biggestWinner = biggestWinner;
    }

    public double getTransactionCosts() {
        return transactionCosts;
    }

    public void setTransactionCosts(double transactionCosts) {
        this.transactionCosts = transactionCosts;
    }


    public double getTotalWinnerRmultiple() {
        return totalWinnerRmultiple;
    }

    public void setTotalWinnerRmultiple(double totalWinnerRmultiple) {
        this.totalWinnerRmultiple = totalWinnerRmultiple;
    }

}
