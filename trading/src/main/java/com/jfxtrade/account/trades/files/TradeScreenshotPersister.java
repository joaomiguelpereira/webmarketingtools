package com.jfxtrade.account.trades.files;

import com.jfxtrade.TradingServerException;
import com.jfxtrade.account.trades.model.Trade;
import com.jfxtrade.account.trades.model.TradeScreenshot;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 21-09-2014
 * Time: 19:20
 * To change this template use File | Settings | File Templates.
 */
@Component
public class TradeScreenshotPersister {


    @Value("${trade.screenshots.path}")
    private String basePath;

    public String getBasePath() {
        return basePath;
    }

    public void setBasePath(String basePath) {
        this.basePath = basePath;
    }

    public void saveScreenshot(Trade trade, TradeScreenshot tradeScreenshot, MultipartFile multipartFile) {
        String path = buildPath(String.valueOf(trade.getAccountId()), trade.getId());
        File folder = new File(path);

        if (!folder.exists()) {
            if (!folder.mkdirs()) {
                throw new RuntimeException("Could not create base dir");
            }
        }


        if (!multipartFile.isEmpty()) {
            try {

                byte[] bytes = multipartFile.getBytes();
                String fileName = path + File.separator + tradeScreenshot.getId() + getExtension(multipartFile.getOriginalFilename());
                BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(fileName)));
                stream.write(bytes);
                stream.close();

            } catch (Exception e) {
                throw new RuntimeException("Could not save file", e);
            }

        }
        //Check if base path exists


    }

    private String buildPath(String accountId, String tradeId) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(basePath).append(File.separator).append(accountId).append(File.separator).append(tradeId);
        return stringBuilder.toString();

    }

    public byte[] loadFile(Long accountId, String tradeId, TradeScreenshot screenshot) {
        String path = buildPath(accountId.toString(), tradeId);

        String fileName = screenshot.getId() + getExtension(screenshot.getOriginalFileName());
        File file = new File(path + File.separator + fileName);

        if (!file.exists()) {
            throw new TradingServerException("The file " + file.getAbsolutePath() + " does not exists");
        }

        try {
            return FileUtils.readFileToByteArray(file);

        } catch (IOException e) {
            throw new TradingServerException("Error reading file", e);
        }


    }

    private String getExtension(String originalFileName) {
        String extension = originalFileName.substring(originalFileName.lastIndexOf(".") + 1);
        if (extension.length() != 3) {
            throw new TradingServerException("Not a valid extension");
        }
        if (!extension.toLowerCase().equals("png")) {
            throw new TradingServerException("Only pngs");
        }
        return "." + extension;
    }

    public void removeTrade(Long accountId, String tradeId) {
        String path = buildPath(String.valueOf(accountId), tradeId);
        try {
            FileUtils.deleteDirectory(new File(path));
        } catch (IOException e) {
            throw new TradingServerException("Directory " + path + " could not be removed", e);
        }

    }

    public void remove(Long accountId, String tradeId, String screenshotId) {
        String path = buildPath(String.valueOf(accountId), tradeId);
        //Fix this harcodeded stuff
        try {
            FileUtils.forceDelete(new File(path + File.separator + screenshotId + ".png"));
        } catch (IOException e) {
            throw new TradingServerException("Error deleting file", e);

        }
    }
}
