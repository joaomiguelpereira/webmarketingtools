package com.jfxtrade.account.trades.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.jfxtrade.utils.json.CustomJsonDateDeserializer;
import com.jfxtrade.utils.json.CustomJsonDateSerializer;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Hours;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 21-09-2014
 * Time: 16:53
 * To change this template use File | Settings | File Templates.
 */
public class TradeOrder {

    private double riskAmmount;
    private double entryPrice;
    private int expectedReward;
    private int number;
    private double profitTarget;
    private int risk;
    private double rmultiple;
    private double size;
    private double slPrice;
    private double closePrice;

    @JsonDeserialize(using = CustomJsonDateDeserializer.class)
    @JsonSerialize(using = CustomJsonDateSerializer.class)
    private Date closeTime;

    @JsonDeserialize(using = CustomJsonDateDeserializer.class)
    @JsonSerialize(using = CustomJsonDateSerializer.class)
    private Date openTime;
    private int openRisk;
    private int closeReward;
    private double swap;
    private double comission;
    private double spread;
    private double profitLoss;
    private double rollover;
    private double adversePrice;
    private double maximumAdverseExcursion;
    private long duration;

    private double  transactionCostCurrency;
    private double transactionCostPips;
    private double afterCostsRmultiple;


    public double getTransactionCostCurrency() {
        return transactionCostCurrency;
    }

    public void setTransactionCostCurrency(double transactionCostCurrency) {
        this.transactionCostCurrency = transactionCostCurrency;
    }

    public double getTransactionCostPips() {
        return transactionCostPips;
    }

    public void setTransactionCostPips(double transactionCostPips) {
        this.transactionCostPips = transactionCostPips;
    }

    public double getAfterCostsRmultiple() {
        return afterCostsRmultiple;
    }

    public void setAfterCostsRmultiple(double afterCostsRmultiple) {
        this.afterCostsRmultiple = afterCostsRmultiple;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public double getAdversePrice() {
        return adversePrice;
    }

    public void setAdversePrice(double adversePrice) {
        this.adversePrice = adversePrice;
    }

    public double getMaximumAdverseExcursion() {
        return maximumAdverseExcursion;
    }

    public void setMaximumAdverseExcursion(double maximumAdverseExcursion) {
        this.maximumAdverseExcursion = maximumAdverseExcursion;
    }

    public double getRollover() {
        return rollover;
    }

    public void setRollover(double rollover) {
        this.rollover = rollover;
    }

    public double getProfitLoss() {
        return profitLoss;
    }

    public void setProfitLoss(double profitLoss) {
        this.profitLoss = profitLoss;
    }

    public double getSwap() {
        return swap;
    }

    public void setSwap(double swap) {
        this.swap = swap;
    }

    public double getComission() {
        return comission;
    }

    public void setComission(double comission) {
        this.comission = comission;
    }

    public double getSpread() {
        return spread;
    }

    public void setSpread(double spread) {
        this.spread = spread;
    }


    public double getClosePrice() {
        return closePrice;
    }

    public void setClosePrice(double closePrice) {
        this.closePrice = closePrice;
    }

    public Date getCloseTime() {
        return closeTime;
    }

    public void setCloseTime(Date closeTime) {
        this.closeTime = closeTime;
    }

    public Date getOpenTime() {
        return openTime;
    }

    public void setOpenTime(Date openTime) {
        this.openTime = openTime;
    }

    public int getOpenRisk() {
        return openRisk;
    }

    public void setOpenRisk(int openRisk) {
        this.openRisk = openRisk;
    }

    public int getCloseReward() {
        return closeReward;
    }

    public void setCloseReward(int closeReward) {
        this.closeReward = closeReward;
    }


    public double getRiskAmmount() {
        return riskAmmount;
    }

    public void setRiskAmmount(double riskAmmount) {
        this.riskAmmount = riskAmmount;
    }

    public double getEntryPrice() {
        return entryPrice;
    }

    public void setEntryPrice(double entryPrice) {
        this.entryPrice = entryPrice;
    }

    public int getExpectedReward() {
        return expectedReward;
    }

    public void setExpectedReward(int expectedReward) {
        this.expectedReward = expectedReward;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public double getProfitTarget() {
        return profitTarget;
    }

    public void setProfitTarget(double profitTarget) {
        this.profitTarget = profitTarget;
    }

    public int getRisk() {
        return risk;
    }

    public void setRisk(int risk) {
        this.risk = risk;
    }

    public double getRmultiple() {
        return rmultiple;
    }

    public void setRmultiple(double rmultiple) {
        this.rmultiple = rmultiple;
    }

    public double getSize() {
        return size;
    }

    public void setSize(double size) {
        this.size = size;
    }

    public double getSlPrice() {
        return slPrice;
    }

    public void setSlPrice(double slPrice) {
        this.slPrice = slPrice;
    }



    public long updateDuration() {
        if ( this.openTime!= null) {
            DateTime openDateTime = new DateTime(this.openTime);
            DateTime closeDateTime = null;
            if ( this.closeTime!=null) {
                closeDateTime= new DateTime(this.closeTime);
            } else {
                closeDateTime = new DateTime();
            }

            this.duration = Days.daysBetween(openDateTime.toLocalDateTime(), closeDateTime.toLocalDateTime()).getDays();
            long hours = Hours.hoursBetween(openDateTime.toLocalDateTime(), closeDateTime.toLocalDateTime()).getHours();
            System.out.println("Hours: "+hours);
            if (hours<24) {

                this.duration = 1;//Use hours field
            }
        }
        return this.duration;
    }

    public void open() {
        this.openRisk = risk;

    }
}
