package com.jfxtrade.account.trades.model;

import com.jfxtrade.TradingServerException;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.mapreduce.MapReduceResults;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

import static org.springframework.data.mongodb.core.query.Criteria.where;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 30-09-2014
 * Time: 0:08
 * To change this template use File | Settings | File Templates.
 */
@Component
public class ClosedTradesSummaryBuilder {

    private final MongoTemplate mongoTemplate;


    @Autowired
    public ClosedTradesSummaryBuilder(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    public CloseTradesSummary buildClosedSummary(long accountId) {
        CloseTradesSummary summary = new CloseTradesSummary();
        summary.setClosedTrades(countClosedTrades(accountId));
        summary.setWinners(countWinningTrades(accountId));
        summary.setLoosers(countLoosingTrades(accountId));
        summary.setAverageRmultiple(averageRMultiple(accountId));
        summary.setAverageNetRmultiple(averageNetRmultiple(accountId));
        summary.setTradesPerDay(tradesPerDay(accountId));

        summary.setAverageWinnerMaer(averageWinningMaer(accountId));
        summary.setAverageLooserMaer(averageLoosingMaer(accountId));

        summary.setAverageWinnerDuration(averageDuration(accountId, true));
        summary.setAverageLooserDuration(averageDuration(accountId, false));
        summary.setAverageDuration(averageDuration(accountId));

        summary.setWinnerSum(sumAmmount(accountId, true));
        summary.setLooserSum(sumAmmount(accountId, false));


        summary.setBiggestWinner(biggest(accountId, true));
        summary.setBiggestLooser(biggest(accountId, false));

        summary.setTransactionCosts(transactionCosts(accountId));
        summary.setTotalWinnerRmultiple(totalRmultiple(accountId, true));
        summary.setTotalLooserRmultiple(totalRmultiple(accountId, false));

        summary.setTotalWinnerNetrmultiple(totalNetrmultiple(accountId, true));
        summary.setTotalLooserNetrmultiple(totalNetrmultiple(accountId, false));

        return summary;

    }

    private double totalNetrmultiple(long accountId, boolean winner) {

        String mapFunction = "function() {" +
                "    emit(this.accountId, this.afterCostsRmultiple);" +
                "};";

        String reduceFunction = "function(accountId, rmultiples) {" +
                "    var r = 0;" +
                "    for(var i=0; i<rmultiples.length; i++) {" +
                "        r+=rmultiples[i];" +
                "    }" +
                "    return r" +
                "};";


        Query closedTrades = new Query(where("accountId").is(accountId).and("status").is(TradeStatus.CLOSED).and("winner").is(winner));
        MapReduceResults<DoubleValueObject> results = mongoTemplate.mapReduce(closedTrades, "trade", mapFunction, reduceFunction, DoubleValueObject.class);
        double r = 0;
        for (DoubleValueObject valueObject : results) {
            System.out.println("Adding: "+valueObject.getValue());
            r += valueObject.getValue();
        }


        return r;



    }

    private double totalRmultiple(long accountId, boolean winner) {
        String mapFunction = "function() {" +
                "    emit(this.accountId, this.rmultiple);" +
                "};";

        String reduceFunction = "function(accountId, rmultiples) {" +
                "    var r = 0;" +
                "    for(var i=0; i<rmultiples.length; i++) {" +
                "        r+=rmultiples[i];" +
                "    }" +
                "    return r" +
                "};";


        Query closedTrades = new Query(where("accountId").is(accountId).and("status").is(TradeStatus.CLOSED).and("winner").is(winner));
        MapReduceResults<DoubleValueObject> results = mongoTemplate.mapReduce(closedTrades, "trade", mapFunction, reduceFunction, DoubleValueObject.class);
        double r = 0;
        for (DoubleValueObject valueObject : results) {
            System.out.println("Adding: "+valueObject.getValue());
            r += valueObject.getValue();
        }


        return r;



    }

    private double transactionCosts(long accountId) {

        String mapFunction = "function() {" +
                "    emit(this.accountId, this.transactionCostCurrency);" +
                "};";

        String reduceFunction = "function(accountId, tcosts) {" +
                "    var pl = 0;" +
                "    for(var i=0; i<tcosts.length; i++) {" +
                "        pl+=tcosts[i];" +
                "    }" +
                "    return pl" +
                "};";


        Query closedTrades = new Query(where("accountId").is(accountId).and("status").is(TradeStatus.CLOSED));
        MapReduceResults<DoubleValueObject> results = mongoTemplate.mapReduce(closedTrades, "trade", mapFunction, reduceFunction, DoubleValueObject.class);
        double r = 0;
        for (DoubleValueObject valueObject : results) {
            r += valueObject.getValue();
        }


        return r;

    }

    private double biggest(long accountId, boolean winner) {
        Query query = new Query(where("accountId").is(accountId).and("status").is(TradeStatus.CLOSED).and("winner").is(winner));
        query.with(new Sort(winner?Sort.Direction.DESC:Sort.Direction.ASC, "profitLoss")).limit(1);
        List<Trade> trades = mongoTemplate.find(query, Trade.class);
        if (trades.size() > 0) {
            return trades.get(0).getProfitLoss();
        }

        return 0;
    }

    private double sumAmmount(long accountId, boolean winner) {
        String mapFunction = "function() {" +
                "    emit(this.accountId, this.profitLoss);" +
                "};";

        String reduceFunction = "function(accountId, profitLosses) {" +
                "    var pl = 0;" +
                "    for(var i=0; i<profitLosses.length; i++) {" +
                "        pl+=profitLosses[i];" +
                "    }" +
                "    return pl" +
                "};";


        Query closedTrades = new Query(where("accountId").is(accountId).and("status").is(TradeStatus.CLOSED).and("winner").is(winner));
        MapReduceResults<DoubleValueObject> results = mongoTemplate.mapReduce(closedTrades, "trade", mapFunction, reduceFunction, DoubleValueObject.class);
        double r = 0;
        for (DoubleValueObject valueObject : results) {
            r += valueObject.getValue();
        }


        return r;


    }


    private double averageLoosingMaer(long accountId) {
        return averageMaer(accountId, false);
    }


    private double tradesPerDay(long accountId) {
        Query firstTrade = new Query(where("accountId").is(accountId).and("status").is(TradeStatus.CLOSED).and("openTime").exists(true));
        firstTrade.with(new Sort(Sort.Direction.ASC, "openTime")).limit(1);
        List<Trade> trades = mongoTemplate.find(firstTrade, Trade.class);
        Date firstDate = null;
        if (trades.size() > 0) {

            firstDate = trades.get(0).getOpenTime();
        } else {
            return 0;
        }

        firstTrade = new Query(where("accountId").is(accountId).and("status").is(TradeStatus.CLOSED).and("closeTime").exists(true));
        firstTrade.with(new Sort(Sort.Direction.DESC, "closeTime")).limit(1);
        trades = mongoTemplate.find(firstTrade, Trade.class);
        Date lastDate = null;
        if (trades.size() > 0) {

            lastDate = trades.get(0).getCloseTime();
        } else {
            return 0;
        }

        int days = Days.daysBetween(new DateTime(firstDate), new DateTime(lastDate)).getDays();
        if (days == 0) {
            throw new TradingServerException("No days between");
        }

        double tradeCount = countClosedTrades(accountId);

        return tradeCount / days;
    }


    private double averageDuration(long accountId, boolean winner) {

        String mapFunction = "function() {" +
                "    emit(this.accountId, this.duration);" +
                "};";

        String reduceFunction = "function(accountId, durations) {" +
                "    var duration = 0;" +
                "    for(var i=0; i<durations.length; i++) {" +
                "        duration+=durations[i];" +
                "    }" +
                "    return duration" +
                "};";


        Query closedTrades = new Query(where("accountId").is(accountId).and("status").is(TradeStatus.CLOSED).and("winner").is(winner));
        MapReduceResults<DoubleValueObject> results = mongoTemplate.mapReduce(closedTrades, "trade", mapFunction, reduceFunction, DoubleValueObject.class);
        double duration = 0;
        for (DoubleValueObject valueObject : results) {
            duration += valueObject.getValue();
        }

        long count = mongoTemplate.count(closedTrades, Trade.class);
        return duration / count;
    }

    private double averageDuration(long accountId) {
        String mapFunction = "function() {" +
                "    emit(this.accountId, this.duration);" +
                "};";

        String reduceFunction = "function(accountId, durations) {" +
                "    var duration = 0;" +
                "    for(var i=0; i<durations.length; i++) {" +
                "        duration+=durations[i];" +
                "    }" +
                "    return duration" +
                "};";


        Query closedTrades = new Query(where("accountId").is(accountId).and("status").is(TradeStatus.CLOSED));
        MapReduceResults<DoubleValueObject> results = mongoTemplate.mapReduce(closedTrades, "trade", mapFunction, reduceFunction, DoubleValueObject.class);
        double duration = 0;
        for (DoubleValueObject valueObject : results) {
            duration += valueObject.getValue();
        }

        long count = mongoTemplate.count(closedTrades, Trade.class);
        return duration / count;


    }


    private double averageMaer(long accountId, boolean winner) {
        String mapFunction = "function() {" +
                "    emit(this.accountId, this.maximumAdverseExcursion);" +
                "};";

        String reduceFunction = "function(accountId, maximumAdverseExcursions) {" +
                "    var mae = 0;" +
                "    for(var i=0; i<maximumAdverseExcursions.length; i++) {" +
                "        mae+=maximumAdverseExcursions[i];" +
                "    }" +
                "    return mae" +
                "};";

        Query winningClosedTrades = new Query(where("accountId").is(accountId).and("status").is(TradeStatus.CLOSED).and("winner").is(winner));
        MapReduceResults<FloatValueObject> results = mongoTemplate.mapReduce(winningClosedTrades, "trade", mapFunction, reduceFunction, FloatValueObject.class);
        double mae = 0.0f;
        for (FloatValueObject valueObject : results) {
            mae += valueObject.getValue();
        }

        long count = mongoTemplate.count(winningClosedTrades, Trade.class);
        return mae / count;

    }

    private double averageWinningMaer(long accountId) {
        return averageMaer(accountId, true);
    }

    private double averageNetRmultiple(long accountId) {
        String mapFunction = "function() {" +
                "    emit(this.accountId, this.afterCostsRmultiple);" +
                "};";

        String reduceFunction = "function(accountId, afterCostsRmultiples) {" +
                "    var rmultiple = 0;" +
                "    for(var i=0; i<afterCostsRmultiples.length; i++) {" +
                "        rmultiple+=afterCostsRmultiples[i];" +
                "    }" +
                "    return rmultiple" +
                "};";

        Query closedTrades = new Query(where("accountId").is(accountId).and("status").is(TradeStatus.CLOSED));

        long count = mongoTemplate.count(closedTrades, Trade.class);
        MapReduceResults<FloatValueObject> results = mongoTemplate.mapReduce(closedTrades, "trade", mapFunction, reduceFunction, FloatValueObject.class);
        double averageRMultiple = 0.0f;
        for (FloatValueObject valueObject : results) {
            averageRMultiple += valueObject.getValue();
        }

        return averageRMultiple / count;


    }

    private double averageRMultiple(long accountId) {
        String mapFunction = "function() {" +
                "    emit(this.accountId, this.rmultiple);" +
                "};";

        String reduceFunction = "function(accountId, rmultiples) {" +
                "    var rmultiple = 0;" +
                "    for(var i=0; i<rmultiples.length; i++) {" +
                "        rmultiple+=rmultiples[i];" +
                "    }" +
                "    return rmultiple" +
                "};";

        Query closedTrades = new Query(where("accountId").is(accountId).and("status").is(TradeStatus.CLOSED));

        long count = mongoTemplate.count(closedTrades, Trade.class);
        MapReduceResults<FloatValueObject> results = mongoTemplate.mapReduce(closedTrades, "trade", mapFunction, reduceFunction, FloatValueObject.class);
        double averageRMultiple = 0.0;
        for (FloatValueObject valueObject : results) {
            averageRMultiple += valueObject.getValue();
        }

        return averageRMultiple / count;


    }

    private long countLoosingTrades(long accountId) {
        Query closedTrades = new Query(where("accountId").is(accountId).and("status").is(TradeStatus.CLOSED).and("winner").is(false));
        return mongoTemplate.count(closedTrades, Trade.class);

    }

    private long countWinningTrades(long accountId) {
        Query closedTrades = new Query(where("accountId").is(accountId).and("status").is(TradeStatus.CLOSED).and("winner").is(true));
        return mongoTemplate.count(closedTrades, Trade.class);

    }

    private long countClosedTrades(long accountId) {
        Query closedTrades = new Query(where("accountId").is(accountId).and("status").is(TradeStatus.CLOSED));
        return mongoTemplate.count(closedTrades, Trade.class);

    }


}
