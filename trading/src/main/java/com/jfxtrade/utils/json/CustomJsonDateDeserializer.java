package com.jfxtrade.utils.json;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 19-09-2014
 * Time: 0:39
 * To change this template use File | Settings | File Templates.
 */
public class CustomJsonDateDeserializer extends JsonDeserializer<Date> {


    private static SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    @Override
    public Date deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {

        String date = jp.getText();
        try {
            return format.parse(date);
        } catch (ParseException e) {
            return null;
            //throw new RuntimeException(e);
        }

    }
}
