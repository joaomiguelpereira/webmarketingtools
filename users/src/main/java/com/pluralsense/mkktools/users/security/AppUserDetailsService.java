package com.pluralsense.mkktools.users.security;

import com.pluralsense.mkktools.users.security.model.Role;
import com.pluralsense.mkktools.users.security.model.User;
import com.pluralsense.mkktools.users.security.model.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 6/2/14
 * Time: 11:16 PM
 * To change this template use File | Settings | File Templates.
 */
@Component
public class AppUserDetailsService implements UserDetailsService{

    @Autowired
    private UsersRepository usersRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = usersRepository.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("Username "+username+" not found");
        }

        List<GrantedAuthority>  grantedAuthorities = new ArrayList<GrantedAuthority>();
        for (Role role : user.getRoles() ) {
            grantedAuthorities.add(new SimpleGrantedAuthority(role.getRole()));
        }
        UserDetails userDetails = new AppUserDetails(grantedAuthorities, user.getUsername(), user.getPasswordHash(), false, false, false, false);

        return userDetails;
    }
}
