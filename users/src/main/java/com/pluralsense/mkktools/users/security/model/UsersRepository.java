package com.pluralsense.mkktools.users.security.model;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 6/2/14
 * Time: 11:39 PM
 * To change this template use File | Settings | File Templates.
 */
public interface UsersRepository extends CrudRepository<User, String> {

    @Query("select u from User u where u.username = ?")
    User findByUsername(String username);

}
