package com.pluralsense.mkktools.users.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 6/2/14
 * Time: 11:16 PM
 * To change this template use File | Settings | File Templates.
 */
public class AppUserDetails implements UserDetails{

    private final String username;
    private final Collection<? extends GrantedAuthority> gratedAuthorities;
    private final boolean isAccountExpired;
    private final boolean isAccountLocked;
    private final boolean isCredentialsExpired;
    private final boolean isEnabled;
    private final String password;

    public AppUserDetails(Collection<? extends GrantedAuthority> grantedAuthorities,
                          String userName,
                          String password,
                          boolean isAccountExpired,
                          boolean isAccountLocked,
                          boolean isCredentialsExpired,
                          boolean isEnabled) {
        this.username = userName;
        this.gratedAuthorities = grantedAuthorities;
        this.isAccountExpired = isAccountExpired;
        this.isAccountLocked = isAccountLocked;
        this.isCredentialsExpired = isCredentialsExpired;
        this.isEnabled = isEnabled;
        this.password = password;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return gratedAuthorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return !isAccountExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return !isAccountLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return !isCredentialsExpired;
    }

    @Override
    public boolean isEnabled() {
        return !isEnabled;
    }
}
