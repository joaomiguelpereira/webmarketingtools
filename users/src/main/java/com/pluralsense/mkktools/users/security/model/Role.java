package com.pluralsense.mkktools.users.security.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 6/3/14
 * Time: 10:41 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "ROLES")
public class Role {

    @Id
    private String role;

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
