package com.pluralsense.mkktools.users.security.model;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 6/4/14
 * Time: 12:17 AM
 * To change this template use File | Settings | File Templates.
 */
@Component
public class AppBCryptPasswordEncoder extends BCryptPasswordEncoder {

}
