package com.pluralsense.mkktools.persistence;

import org.springframework.data.annotation.Id;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 5/21/14
 * Time: 10:51 AM
 * To change this template use File | Settings | File Templates.
 */
public abstract class AbstractMongoEntity {

    @Id
    private String id;

    public String getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AbstractMongoEntity that = (AbstractMongoEntity) o;

        if (!id.equals(that.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }


    public void setId(String id) {
        this.id = id;
    }
}
