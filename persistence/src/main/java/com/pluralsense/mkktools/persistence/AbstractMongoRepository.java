package com.pluralsense.mkktools.persistence;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.lang.reflect.ParameterizedType;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 5/21/14
 * Time: 11:38 AM
 * To change this template use File | Settings | File Templates.
 */
public abstract class AbstractMongoRepository<T> {

    protected final MongoTemplate mongoTemplate;

    private Class<T> type;

    protected AbstractMongoRepository(MongoTemplate mongoTemplate) {
        ParameterizedType superclass = (ParameterizedType)
                getClass().getGenericSuperclass();

        type = (Class<T>)superclass.getActualTypeArguments()[0];

        this.mongoTemplate = mongoTemplate;
    }

    public void save(T entity) {
        mongoTemplate.save(entity);

    }

    public List<T> findAll(int page, int size) {
        Query query = Query.query(Criteria.where("id").exists(true)).with(new PageRequest(page,size, Sort.Direction.DESC,"eventTime"));
        return  mongoTemplate.find(query, type);

    }

    public long countAll() {
        Query query = Query.query(Criteria.where("id").exists(true));
        return  mongoTemplate.count(query, type);


    }
}
