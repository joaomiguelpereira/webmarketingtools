package com.pluralsense.mkktools.twitter.model;

import com.pluralsense.mkktools.persistence.AbstractMongoRepository;
import org.springframework.data.mongodb.core.MongoTemplate;

public class TwitterUserToFollowRepository extends AbstractMongoRepository<TwitterUserToFollow> {

    public TwitterUserToFollowRepository(MongoTemplate mongoTemplate) {
        super(mongoTemplate);
    }



}
