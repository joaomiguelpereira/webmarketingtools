package com.pluralsense.mkktools.twitter.routes;

import com.pluralsense.mkktools.twitter.model.TwitterUserToFollow;
import com.pluralsense.mkktools.twitter.model.TwitterUserToFollowRepository;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 4/21/14
 * Time: 12:07 PM
 * To change this template use File | Settings | File Templates.
 */
public class FriendsFetcherRouter extends RouteBuilder {
    private static final Logger LOG = LoggerFactory.getLogger(FriendsFetcherRouter.class);

    private final String cronExpression;
    private final boolean enabled;
    private final TwitterUserToFollowRepository repository;


    public FriendsFetcherRouter(String cronExpression, boolean enabled, TwitterUserToFollowRepository repository) {
        this.cronExpression = cronExpression;
        this.enabled = enabled;
        this.repository = repository;

    }

    @Override
    public void configure() throws Exception {

        if (enabled) {
            from("quartz2://myGroup/myTimerName?cron=" + cronExpression).process(new Processor() {
                @Override
                public void process(Exchange exchange) throws Exception {

                    TwitterUserToFollow twitterUserToFollow = new TwitterUserToFollow();
                    repository.save(twitterUserToFollow);
                    LOG.info("Running.........");

                }
            });
        }


    }
}
