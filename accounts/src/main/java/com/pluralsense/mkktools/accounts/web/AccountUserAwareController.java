package com.pluralsense.mkktools.accounts.web;

import com.pluralsense.mkktools.accounts.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 6/6/14
 * Time: 8:48 PM
 * To change this template use File | Settings | File Templates.
 */

public abstract class AccountUserAwareController {


    @Autowired
    protected AccountRepository accountRepository;


    @Autowired
    protected AccountUserRepository accountUserrepository;


    protected AccountUser getUser() {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (authentication == null) {
            throw new AccountsException("No authentication present!");
        }

        AccountUser user = accountUserrepository.findByUsername(authentication.getName());
        if (user == null) {
            throw new AccountsException("Could not find any Account User with this username");
        }
        return user;

    }

    protected Account getAccount(Long accountId) {

        AccountUser user = getUser();
        Account account = accountRepository.findOne(accountId);
        if ( !user.getManagedAccounts().contains(account)) {
            throw new AccountsException("Account does not belong to that user!");
        }

        return account;
    }




}
