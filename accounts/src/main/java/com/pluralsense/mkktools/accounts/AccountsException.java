package com.pluralsense.mkktools.accounts;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 6/5/14
 * Time: 10:59 PM
 * To change this template use File | Settings | File Templates.
 */
public class AccountsException extends RuntimeException {
    public AccountsException(String message) {
        super(message);
    }
}
