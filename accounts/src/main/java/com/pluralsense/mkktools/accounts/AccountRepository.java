package com.pluralsense.mkktools.accounts;

import org.springframework.data.repository.CrudRepository;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 6/6/14
 * Time: 1:02 AM
 * To change this template use File | Settings | File Templates.
 */
public interface AccountRepository extends CrudRepository<Account, Long> {

}
