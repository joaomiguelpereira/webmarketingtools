package com.pluralsense.mkktools.accounts.web;

import com.pluralsense.mkktools.accounts.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 6/5/14
 * Time: 12:16 AM
 * To change this template use File | Settings | File Templates.
 */

@Controller
@RequestMapping(value = "/api/accounts")
public class AccountsController extends AccountUserAwareController {



    /**
     * Simply serves index.jsp
     *
     * @return view with name 'hello'
     */
    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    @Transactional
    public List<Account> index() {

        AccountUser user = getUser();

        List<Account> accounts = new ArrayList<Account>(user.getManagedAccounts().size());
        for (Account account : user.getManagedAccounts()) {
            accounts.add(account);
        }
        return accounts;


    }



    @Transactional
    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public String save(@RequestBody Account account) {

        AccountUser user = getUser();
        if ( account.getId()!=0) {
          Account savedAccount =  user.getManagedAccount(account.getId());
            if (savedAccount!=null) {
                savedAccount.setDescription(account.getDescription());
                savedAccount.setName(account.getName());
            }
        } else {
            user.addManagedAccount(account);
        }

        accountUserrepository.save(user);
        return "Account created!";
    }

    @Transactional
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Account getAccount(@PathVariable(value = "id") Long id) {


        AccountUser user = getUser();

        Account account = accountRepository.findOne(id);
        if (account==null) {
            throw new AccountsException("Account not found");
        }
        if ( !user.getManagedAccounts().contains(account)) {
            throw new AccountsException("Account not avaiable!");
        }
        return account;


    }
    @Transactional
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public String delete(@PathVariable(value = "id") Long id) {


        AccountUser user = getUser();

        Account account = accountRepository.findOne(id);
        if (account==null) {
            throw new AccountsException("Account not found");
        }
        if ( !user.getManagedAccounts().contains(account)) {
            throw new AccountsException("Account not avaiable!");
        }
        user.removeManagedAccount(account);
        accountRepository.delete(id);
        accountUserrepository.save(user);
        return "Account removed";


    }

}
