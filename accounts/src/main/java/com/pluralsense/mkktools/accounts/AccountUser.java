package com.pluralsense.mkktools.accounts;

import javax.persistence.*;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 6/4/14
 * Time: 11:10 PM
 * To change this template use File | Settings | File Templates.
 */

@Entity
@Table(name = "ACCOUNTUSERS")
public class AccountUser {

    @Id
    private String username;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "ACCOUNTUSERS_ACCOUNTS", joinColumns = @JoinColumn(name = "username"), inverseJoinColumns = @JoinColumn(name = "account_id"))
    private List<Account> managedAccounts;


    public String getUsername() {
        return username;
    }

    public List<Account> getManagedAccounts() {
        return managedAccounts;
    }

    public void addManagedAccount(Account account) {
        this.managedAccounts.add(account);
    }

    public void removeManagedAccount(Account account) {
        this.managedAccounts.remove(account);
    }

    public Account getManagedAccount(long id) {
        for (Account account : managedAccounts ) {
            if ( account.getId() == id) {
                return account;
            }
        }
        return null;

    }
}
