package com.pluralsense.mkktools.accounts;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 6/4/14
 * Time: 11:10 PM
 * To change this template use File | Settings | File Templates.
 */
@MappedSuperclass
public abstract class AbstractJpaEntity {
    @Id
    private long id;

    public long getId() {
        return id;
    }


}
