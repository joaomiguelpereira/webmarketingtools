package com.pluralsense.mkktools.accounts;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 6/5/14
 * Time: 12:14 AM
 * To change this template use File | Settings | File Templates.
 */
public interface AccountUserRepository extends CrudRepository<AccountUser, String>{

    @Query("select u from AccountUser u where u.username = ?")
    AccountUser findByUsername(String username);

}
