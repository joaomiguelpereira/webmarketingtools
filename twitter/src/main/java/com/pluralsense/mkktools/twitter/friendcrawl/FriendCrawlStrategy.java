package com.pluralsense.mkktools.twitter.friendcrawl;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 6/8/14
 * Time: 9:56 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "TWITTER_FRIEND_CRAWLER_STRATEGY")
public class FriendCrawlStrategy implements Serializable{


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TWITTER_FRIEND_CRAWLER_STRATEGY_ID_SEQ")
    @SequenceGenerator(name = "TWITTER_FRIEND_CRAWLER_STRATEGY_ID_SEQ", sequenceName = "TWITTER_FRIEND_CRAWLER_STRATEGY_ID_SEQ")
    private Long id;

    @Enumerated(value = EnumType.STRING)
    private StrategyName name;

    private String options;
    private boolean active;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOptions() {
        return options;
    }

    public void setOptions(String options) {
        this.options = options;
    }

    public boolean isActive() {
        return active;
    }

    public StrategyName getName() {
        return name;
    }

    public void setName(StrategyName name) {
        this.name = name;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public String toString() {
        return "FriendCrawlStrategy{" +
                "id=" + id +
                ", name=" + name +
                ", options='" + options + '\'' +
                ", active=" + active +
                '}';
    }
}
