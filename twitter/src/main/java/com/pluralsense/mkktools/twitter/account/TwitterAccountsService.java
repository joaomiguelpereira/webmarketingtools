package com.pluralsense.mkktools.twitter.account;

import com.pluralsense.mkktools.twitter.TwitterIntegrationException;
import com.pluralsense.mkktools.twitter.friendcrawl.FriendCrawl;
import com.pluralsense.mkktools.twitter.friendcrawl.FriendCrawlRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collection;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 6/10/14
 * Time: 10:58 AM
 * To change this template use File | Settings | File Templates.
 */
@Component
public class TwitterAccountsService {
    @Autowired
    private TwitterAccountRepository twitterAccountRepository;

    @Autowired FriendCrawlRepository friendCrawlRepository;

    @Autowired
    private TwitterAccountSynchronizer twitterAccountSynchronizer;

    public Collection<TwitterAccount> findByAccountId(long accountId) {
        return twitterAccountRepository.findByAccountId(accountId);
    }

    public void delete(long accountId, Long twitterAccountId) {


        TwitterAccount twitterAccount = twitterAccountRepository.findOne(twitterAccountId);
        if ( twitterAccount.getAccountId() != accountId ) {
            throw new TwitterIntegrationException("This Twitter account does not belong to the Account");
        }

        FriendCrawl friendCrawl = friendCrawlRepository.findByTwitterAccountId(twitterAccountId);

        if (friendCrawl!=null) {
            friendCrawlRepository.delete(friendCrawl);
        }

        twitterAccountRepository.delete(twitterAccountId);

    }

    public TwitterAccount find(Long accountId, Long id, boolean refresh) {


        TwitterAccount twitterAccount = twitterAccountRepository.findOne(id);
        if ( twitterAccount.getAccountId() != accountId ) {
            throw new TwitterIntegrationException("This Twitter account does not belong to the Account");
        }
        if (refresh) {
            twitterAccountSynchronizer.synchronizeAccount(twitterAccount);
            twitterAccountRepository.save(twitterAccount);
        }
        return twitterAccount;
    }
}
