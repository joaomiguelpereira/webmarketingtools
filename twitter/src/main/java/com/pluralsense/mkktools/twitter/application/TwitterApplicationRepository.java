package com.pluralsense.mkktools.twitter.application;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 6/6/14
 * Time: 11:54 PM
 * To change this template use File | Settings | File Templates.
 */
public interface TwitterApplicationRepository extends CrudRepository<TwitterApplication, Long>{


    @Query("select t from TwitterApplication t where t.accountId= ?")
    TwitterApplication findByAccountId(Long accountId);
}
