package com.pluralsense.mkktools.twitter.friendcrawl.logging;

import com.pluralsense.mkktools.persistence.AbstractMongoEntity;

import java.util.Calendar;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 6/10/14
 * Time: 10:32 PM
 * To change this template use File | Settings | File Templates.
 */
public class FriendCrawlLog extends AbstractMongoEntity {

    private final long accountId;
    private final String accountName;
    private final long twitterAccountId;
    private final String twitterAccountScreenName;
    private final String strategyName;
    private final String message;
    private final long eventTime;

    public FriendCrawlLog(long accountId,
                          String accountName,
                          long twitterAccountId,
                          String twitterAccountScreenName,
                          String strategyName,
                          String message) {

        this.accountId = accountId;
        this.accountName = accountName;
        this.twitterAccountId = twitterAccountId;
        this.twitterAccountScreenName = twitterAccountScreenName;
        this.strategyName = strategyName;
        this.message = message;
        this.eventTime = Calendar.getInstance().getTimeInMillis();


    }

    public long getAccountId() {
        return accountId;
    }

    public String getAccountName() {
        return accountName;
    }

    public long getTwitterAccountId() {
        return twitterAccountId;
    }

    public String getTwitterAccountScreenName() {
        return twitterAccountScreenName;
    }

    public String getStrategyName() {
        return strategyName;
    }

    public String getMessage() {
        return message;
    }

    public long getEventTime() {
        return eventTime;
    }
}
