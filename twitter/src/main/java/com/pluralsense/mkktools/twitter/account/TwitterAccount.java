package com.pluralsense.mkktools.twitter.account;

import javax.persistence.*;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 5/28/14
 * Time: 10:21 AM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "TWITTER_ACCOUNT")
public class TwitterAccount {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TWITTER_ACCOUNT_ID_SEQ")
    @SequenceGenerator(name = "TWITTER_ACCOUNT_ID_SEQ", sequenceName = "TWITTER_ACCOUNT_ID_SEQ")
    private Long id;

    private Long accountId;

    @Embedded
    private TwitterAccountAuthorization twitterAccountAuthorization;

    private String screenName;
    private String name;
    private String status;
    private int statusesCount;
    private int rateLimit;
    private int rateLimitRemaining;
    private int rateLimitResetTimeInSecond;
    private int rateLimitSecondsUntilReset;
    private String profileImageUrl;
    private int friendsCount;
    private int followersCount;
    private long twitterId;
    private Date statusDate;


    public Long getId() {
        return id;
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public TwitterAccountAuthorization getTwitterAccountAuthorization() {
        return twitterAccountAuthorization;
    }

    public void setTwitterAccountAuthorization(TwitterAccountAuthorization twitterAccountAuthorization) {
        this.twitterAccountAuthorization = twitterAccountAuthorization;
    }

    public void setScreenName(String screenName) {
        this.screenName = screenName;
    }

    public String getScreenName() {
        return screenName;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatusesCount(int statusesCount) {
        this.statusesCount = statusesCount;
    }

    public int getStatusesCount() {
        return statusesCount;
    }

    public void setRateLimit(int rateLimit) {
        this.rateLimit = rateLimit;
    }

    public int getRateLimit() {
        return rateLimit;
    }

    public void setRateLimitRemaining(int rateLimitRemaining) {
        this.rateLimitRemaining = rateLimitRemaining;
    }

    public int getRateLimitRemaining() {
        return rateLimitRemaining;
    }

    public void setRateLimitResetTimeInSecond(int rateLimitResetTimeInSecond) {
        this.rateLimitResetTimeInSecond = rateLimitResetTimeInSecond;
    }

    public int getRateLimitResetTimeInSecond() {
        return rateLimitResetTimeInSecond;
    }

    public void setRateLimitSecondsUntilReset(int rateLimitSecondsUntilReset) {
        this.rateLimitSecondsUntilReset = rateLimitSecondsUntilReset;
    }

    public int getRateLimitSecondsUntilReset() {
        return rateLimitSecondsUntilReset;
    }

    public void setProfileImageUrl(String profileImageUrl) {
        this.profileImageUrl = profileImageUrl;
    }

    public String getProfileImageUrl() {
        return profileImageUrl;
    }

    public void setFriendsCount(int friendsCount) {
        this.friendsCount = friendsCount;
    }

    public int getFriendsCount() {
        return friendsCount;
    }

    public void setFollowersCount(int followersCount) {
        this.followersCount = followersCount;
    }

    public int getFollowersCount() {
        return followersCount;
    }

    public void setTwitterId(long twitterId) {
        this.twitterId = twitterId;
    }

    public long getTwitterId() {
        return twitterId;
    }

    public void setStatusDate(Date statusDate) {
        this.statusDate = statusDate;
    }

    public Date getStatusDate() {
        return statusDate;
    }
}
