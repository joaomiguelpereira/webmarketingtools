package com.pluralsense.mkktools.twitter.integration.model;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 6/7/14
 * Time: 7:51 PM
 * To change this template use File | Settings | File Templates.
 */
public interface ApplicationRequestTokenRepository extends CrudRepository<ApplicationRequestToken, Long> {


    @Query("select token from ApplicationRequestToken token where token.token=?")
    ApplicationRequestToken findByToken(String requestToken);
}
