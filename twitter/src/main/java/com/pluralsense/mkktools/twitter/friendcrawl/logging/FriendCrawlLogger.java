package com.pluralsense.mkktools.twitter.friendcrawl.logging;

import com.pluralsense.mkktools.twitter.friendcrawl.FriendCrawlStrategy;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 6/10/14
 * Time: 10:33 PM
 * To change this template use File | Settings | File Templates.
 */
public interface FriendCrawlLogger {

    void logEvent(Long friendCrawlId, FriendCrawlStrategy strategy, String message);
}
