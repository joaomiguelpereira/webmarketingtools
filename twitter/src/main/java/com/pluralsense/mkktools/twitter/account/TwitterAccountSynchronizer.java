package com.pluralsense.mkktools.twitter.account;

import com.pluralsense.mkktools.twitter.TwitterIntegrationException;
import com.pluralsense.mkktools.twitter.application.TwitterApplication;
import com.pluralsense.mkktools.twitter.application.TwitterApplicationRepository;
import com.pluralsense.mkktools.twitter.integration.TwitterApi;
import com.pluralsense.mkktools.twitter.integration.TwitterApiFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 6/9/14
 * Time: 11:25 PM
 * To change this template use File | Settings | File Templates.
 */
@Component
public class TwitterAccountSynchronizer {

    @Autowired
    private TwitterApplicationRepository twitterApplicationRepository;

    @Autowired
    private TwitterApiFactory apiFactory;

    @Autowired
    private TwitterAccountFactory twitterAccountFactory;

    public TwitterAccount synchronizeAccount(TwitterAccount twitterAccount) {

        TwitterApplication application = twitterApplicationRepository.findByAccountId(twitterAccount.getAccountId());
        if ( application == null)  {
            throw new TwitterIntegrationException("Could not find twitter application!");
        }


        TwitterAccountAuthorization authorization = twitterAccount.getTwitterAccountAuthorization();
        TwitterApi api = apiFactory.get(application.getApiKey(), application.getApiSecret(), authorization.getAccessToken(), authorization.getAccessTokenSecret());
        return twitterAccountFactory.sychronizeAccount(twitterAccount, api);

    }
}
