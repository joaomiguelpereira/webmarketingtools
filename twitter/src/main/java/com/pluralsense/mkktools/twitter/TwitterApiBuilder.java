package com.pluralsense.mkktools.twitter;

import com.pluralsense.mkktools.accounts.Account;
import com.pluralsense.mkktools.accounts.AccountRepository;
import com.pluralsense.mkktools.twitter.account.TwitterAccount;
import com.pluralsense.mkktools.twitter.account.TwitterAccountAuthorization;
import com.pluralsense.mkktools.twitter.account.TwitterAccountRepository;
import com.pluralsense.mkktools.twitter.application.TwitterApplication;
import com.pluralsense.mkktools.twitter.application.TwitterApplicationRepository;
import com.pluralsense.mkktools.twitter.friendcrawl.FriendCrawl;
import com.pluralsense.mkktools.twitter.friendcrawl.FriendCrawlRepository;
import com.pluralsense.mkktools.twitter.integration.TwitterApi;
import com.pluralsense.mkktools.twitter.integration.TwitterApiFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 6/16/14
 * Time: 10:03 PM
 * To change this template use File | Settings | File Templates.
 */
@Component
public class TwitterApiBuilder {

    @Autowired
    private TwitterApiFactory apiFactory;
    @Autowired
    private FriendCrawlRepository friendCrawlRepository;
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private TwitterAccountRepository twitterAccountRepository;
    @Autowired
    private TwitterApplicationRepository twitterApplicationRepository;

    public TwitterApi buildFromFriendCrawlId(Long friendCrawlId) {

        FriendCrawl friendCrawl = friendCrawlRepository.findOne(friendCrawlId);
        if (friendCrawl == null ) {
            throw new TwitterIntegrationException("Could not find a friend crawl with ID "+friendCrawlId);
        }

        TwitterAccount twitterAccount = twitterAccountRepository.findOne(friendCrawl.getTwitterAccountId());

        if ( twitterAccount == null) {
            throw new TwitterIntegrationException("Could not find a Twitter Account with ID "+friendCrawl.getTwitterAccountId());

        }
        Account account = accountRepository.findOne(twitterAccount.getAccountId());
        if (account == null) {
            throw new TwitterIntegrationException("Could not find Account");
        }

        TwitterApplication twitterApplication = twitterApplicationRepository.findByAccountId(account.getId());

        if ( twitterApplication == null) {
            throw new TwitterIntegrationException("Could not find a twitter account for account "+account.getId());
        }


        TwitterAccountAuthorization authorization = twitterAccount.getTwitterAccountAuthorization();
        return apiFactory.get(twitterApplication.getApiKey(), twitterApplication.getApiSecret(), authorization.getAccessToken(), authorization.getAccessTokenSecret());


    }
}
