package com.pluralsense.mkktools.twitter.friendcrawl.strategyrunning;

import com.pluralsense.mkktools.twitter.friendcrawl.FriendCrawlStrategy;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 6/10/14
 * Time: 9:51 PM
 * To change this template use File | Settings | File Templates.
 */
public class ProcessStrategyMessage implements Serializable{
    private final Long friendCrawlId;
    private final FriendCrawlStrategy strategy;

    public ProcessStrategyMessage(Long friendCrawlId, FriendCrawlStrategy strategy) {
        this.friendCrawlId = friendCrawlId;
        this.strategy = strategy;
    }

    public Long getFriendCrawlId() {
        return friendCrawlId;
    }

    public FriendCrawlStrategy getStrategy() {
        return strategy;
    }

    @Override
    public String toString() {
        return "ProcessStrategyMessage{" +
                "friendCrawlId=" + friendCrawlId +
                ", strategy=" + strategy +
                '}';
    }
}

