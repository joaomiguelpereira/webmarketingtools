package com.pluralsense.mkktools.twitter.account;

import com.pluralsense.mkktools.accounts.Account;
import com.pluralsense.mkktools.accounts.web.AccountUserAwareController;
import com.pluralsense.mkktools.twitter.TwitterIntegrationException;
import com.pluralsense.mkktools.twitter.application.TwitterApplicationRepository;
import com.pluralsense.mkktools.twitter.friendcrawl.FriendCrawlRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.Collection;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 6/7/14
 * Time: 8:04 PM
 * To change this template use File | Settings | F  ile Templates.
 */
@Controller
@RequestMapping(value = "/api/accounts/{accountId}/twitter/accounts")
public class TwitterAccountController extends AccountUserAwareController {

    @Autowired
    private TwitterAccountsService twitterAccountsService;


    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    @Transactional
    public Collection<TwitterAccount> twitterAccounts(@PathVariable("accountId") Long accountId) {
        Account account = getAccount(accountId);
        return twitterAccountsService.findByAccountId(account.getId());


    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    @Transactional
    public String delete(@PathVariable("accountId") Long accountId, @PathVariable("id") Long id) {
        Account account = getAccount(accountId);
        twitterAccountsService.delete(account.getId(), id);
        return "Twitter Account removed";

    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    @Transactional
    public TwitterAccount get(@PathVariable("accountId") Long accountId, @PathVariable("id") Long id,  @RequestParam(defaultValue = "false", required = false, value = "refresh") boolean refresh) {
        Account account = getAccount(accountId);
        return twitterAccountsService.find(account.getId(), id, refresh);


    }
}
