package com.pluralsense.mkktools.twitter.friendcrawl;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 6/8/14
 * Time: 7:31 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "TWITTER_FRIEND_CRAWL")
public class FriendCrawl {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TWITTER_FRIEND_CRAWL_ID_SEQ")
    @SequenceGenerator(name = "TWITTER_FRIEND_CRAWL_ID_SEQ", sequenceName = "TWITTER_FRIEND_CRAWL_ID_SEQ")
    private Long id;

    private Long twitterAccountId;

    @Embedded
    private FriendCrawlExecutionParams friendCrawlExecutionParams;

    @Enumerated(EnumType.STRING)
    private FriendCrawlStatus status = FriendCrawlStatus.STOPPED;

    private Integer runs;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "friendcrawlid", nullable = false)
    private List<FriendCrawlStrategy> strategies = new ArrayList();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTwitterAccountId() {
        return twitterAccountId;
    }

    public void setTwitterAccountId(Long twitterAccountId) {
        this.twitterAccountId = twitterAccountId;
    }


    public List<FriendCrawlStrategy> getStrategies() {
        return strategies;
    }

    public void setStrategies(List<FriendCrawlStrategy> strategies) {
        this.strategies = strategies;
    }


    public void addStrategy(FriendCrawlStrategy strategy) {
         strategies.add(strategy);

    }

    public FriendCrawlExecutionParams getFriendCrawlExecutionParams() {
        return friendCrawlExecutionParams;
    }

    public void setFriendCrawlExecutionParams(FriendCrawlExecutionParams friendCrawlExecutionParams) {
        this.friendCrawlExecutionParams = friendCrawlExecutionParams;
    }

    public FriendCrawlStatus getStatus() {
        return status;
    }
    public void setStatus(FriendCrawlStatus status) {
        this.status = status;
    }

    public Integer getRuns() {
        return runs;
    }

    public void setRuns(Integer runs) {
        this.runs = runs;
    }
}

