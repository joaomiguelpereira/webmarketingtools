package com.pluralsense.mkktools.twitter.friendcrawl.strategyrunning;

import com.pluralsense.mkktools.twitter.friendcrawl.StrategyName;
import org.apache.camel.Exchange;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 6/10/14
 * Time: 9:57 PM
 * To change this template use File | Settings | File Templates.
 */

public abstract class AbstractFriendCrawlStrategyProcessor implements FriendCrawlStrategyProcessor {

    @Override
    public void process(Exchange exchange) throws Exception {
        if (exchange.getIn().getBody()!= null && exchange.getIn().getBody() instanceof ProcessStrategyMessage) {
            ProcessStrategyMessage message = (ProcessStrategyMessage)exchange.getIn().getBody();
            if ( supports(message.getStrategy().getName()))  {
                process(message);
            }
        }

    }

    protected abstract void process(ProcessStrategyMessage message);

    protected abstract boolean supports(StrategyName name);
}
