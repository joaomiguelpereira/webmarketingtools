package com.pluralsense.mkktools.twitter.application;

import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 6/6/14
 * Time: 11:52 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "TWITTER_APPLICATION")
public class TwitterApplication {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TWITTER_APPLICATION_ID_SEQ")
    @SequenceGenerator(name = "TWITTER_APPLICATION_ID_SEQ", sequenceName = "TWITTER_APPLICATION_ID_SEQ")
    private Long id;

    @Column(name = "accountId")
    private Long accountId;

    private String description;
    private String apiKey;
    private String apiSecret;



    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getApiSecret() {
        return apiSecret;
    }

    public void setApiSecret(String apiSecret) {
        this.apiSecret = apiSecret;
    }
}
