package com.pluralsense.mkktools.twitter.authorization;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 6/7/14
 * Time: 7:20 PM
 * To change this template use File | Settings | File Templates.
 */
public class AuthorizationDetails {
    private final String authorizationURL;
    private final String authenticationURL;

    public AuthorizationDetails(String authorizationURL, String authenticationURL) {
        this.authorizationURL = authorizationURL;
        this.authenticationURL = authenticationURL;

    }

    public String getAuthorizationURL() {
        return authorizationURL;
    }

    public String getAuthenticationURL() {
        return authenticationURL;
    }
}
