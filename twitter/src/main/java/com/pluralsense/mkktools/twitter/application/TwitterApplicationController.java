package com.pluralsense.mkktools.twitter.application;

import com.pluralsense.mkktools.accounts.Account;
import com.pluralsense.mkktools.accounts.web.AccountUserAwareController;
import com.pluralsense.mkktools.twitter.TwitterIntegrationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 6/6/14
 * Time: 11:43 PM
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping("/api/accounts/{accountId}/twitter/application")
public class TwitterApplicationController extends AccountUserAwareController {

    @Autowired
    private TwitterApplicationRepository twitterApplicationRepository;



    @RequestMapping(method = RequestMethod.GET )
    @Transactional
    @ResponseBody
    public TwitterApplication get(@PathVariable("accountId") Long accountId) {

        Account account = getAccount(accountId);

        TwitterApplication application = twitterApplicationRepository.findByAccountId(account.getId());

        if (application==null) {
            application = new TwitterApplication();
            application.setApiKey("NA");
            application.setDescription("Not defined");
            application.setAccountId(accountId);
            twitterApplicationRepository.save(application);
        }

        return application;
    }


    @RequestMapping(method = RequestMethod.POST )
    @Transactional
    @ResponseBody
    public TwitterApplication save(@PathVariable("accountId") Long accountId, @RequestBody TwitterApplication twitterApplication) {

        Account account = getAccount(accountId);

        TwitterApplication application = twitterApplicationRepository.findByAccountId(account.getId());

        if (application==null) {
            throw new TwitterIntegrationException("Twitter Application not found!");
        }
        application.setApiKey(twitterApplication.getApiKey());
        application.setApiSecret(twitterApplication.getApiSecret());
        application.setDescription(twitterApplication.getDescription());

        return application;
    }

}
