package com.pluralsense.mkktools.twitter.friendcrawl;

import javax.persistence.Embeddable;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 6/8/14
 * Time: 9:54 PM
 * To change this template use File | Settings | File Templates.
 */
@Embeddable
public class FriendCrawlExecutionParams {


    private boolean active;
    private int intervalInSeconds;
    private int resultsSizeLimit;
    private int runIfRateLimitHigherThan;
    private Long nextRun;
    private Long lastRun;


    public boolean isActive() {
        return active;
    }


    public void setActive(boolean active) {
        this.active = active;
    }

    public int getResultsSizeLimit() {
        return resultsSizeLimit;
    }

    public void setResultsSizeLimit(int resultsSizeLimit) {
        this.resultsSizeLimit = resultsSizeLimit;
    }

    public int getRunIfRateLimitHigherThan() {
        return runIfRateLimitHigherThan;
    }

    public void setRunIfRateLimitHigherThan(int runIfRateLimitHigherThan) {
        this.runIfRateLimitHigherThan = runIfRateLimitHigherThan;
    }

    public Long getNextRun() {
        return nextRun;
    }

    public void setNextRun(Long nextRun) {
        this.nextRun = nextRun;
    }

    public Long getLastRun() {
        return lastRun;
    }

    public void setLastRun(Long lastRun) {
        this.lastRun = lastRun;
    }

    public int getIntervalInSeconds() {
        return intervalInSeconds;
    }

    public void setIntervalInSeconds(int intervalInSeconds) {
        this.intervalInSeconds = intervalInSeconds;
    }
}
