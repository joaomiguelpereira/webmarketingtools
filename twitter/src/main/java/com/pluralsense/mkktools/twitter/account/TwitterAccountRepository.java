package com.pluralsense.mkktools.twitter.account;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 6/7/14
 * Time: 8:07 PM
 * To change this template use File | Settings | File Templates.
 */
public interface TwitterAccountRepository extends CrudRepository<TwitterAccount, Long>{

    @Query("select acc from TwitterAccount acc where acc.accountId=?")
    Collection<TwitterAccount> findByAccountId(Long accountId);


    TwitterAccount findByAccountIdAndTwitterId(long id, long twitterId);
    TwitterAccount findByIdAndAccountId(long id, long accountId);

}
