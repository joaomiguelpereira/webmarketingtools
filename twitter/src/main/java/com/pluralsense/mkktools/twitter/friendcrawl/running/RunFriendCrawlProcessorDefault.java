package com.pluralsense.mkktools.twitter.friendcrawl.running;

import com.pluralsense.mkktools.twitter.TwitterIntegrationException;
import com.pluralsense.mkktools.twitter.friendcrawl.FriendCrawl;
import com.pluralsense.mkktools.twitter.friendcrawl.FriendCrawlRepository;
import com.pluralsense.mkktools.twitter.friendcrawl.FriendCrawlStrategy;
import com.pluralsense.mkktools.twitter.friendcrawl.strategyrunning.ProcessStrategyMessage;
import org.apache.camel.EndpointInject;
import org.apache.camel.Exchange;
import org.apache.camel.ProducerTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 6/10/14
 * Time: 5:50 PM
 * To change this template use File | Settings | File Templates.
 */
@Component
public class RunFriendCrawlProcessorDefault implements RunFriendCrawlProcessor {

    @Autowired
    private FriendCrawlRepository friendCrawlRepository;

    @EndpointInject(uri = "jms:twitter_friend_crawl_strategy_run_queue")
    private ProducerTemplate producerTemplate;

    private Logger LOG = LoggerFactory.getLogger(RunFriendCrawlProcessorDefault.class);

    @Override
    @Transactional
    public void process(Exchange exchange) throws Exception {

        if (exchange.getIn().getBody() != null) {
            Long friendCrawlId = Long.valueOf(exchange.getIn().getBody().toString());
            LOG.info("Running friend crawl for:  " + friendCrawlId);
            process(friendCrawlId);
        } else {
            LOG.info("Nothing to process");
        }

    }


    private void process(long friendCrawlId) {

        FriendCrawl friendCrawl = friendCrawlRepository.findOne(friendCrawlId);
        if (friendCrawl == null) {
            throw new TwitterIntegrationException("The FriendCrawl does not exists");
        }

        for (FriendCrawlStrategy strategy : friendCrawl.getStrategies() ) {
            if ( strategy.isActive() ) {
                producerTemplate.sendBody(new ProcessStrategyMessage(friendCrawl.getId(), strategy));
            }
        }

    }
}
