package com.pluralsense.mkktools.twitter.friendcrawl;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 6/8/14
 * Time: 7:41 PM
 * To change this template use File | Settings | File Templates.
 */
public interface FriendCrawlRepository extends CrudRepository<FriendCrawl, Long>{
    FriendCrawl findByTwitterAccountId(Long twitterAccountId);

    @Query("select f from FriendCrawl f where f.friendCrawlExecutionParams.active=true and (f.friendCrawlExecutionParams.lastRun=null or f.friendCrawlExecutionParams.nextRun < ?)")
    List<FriendCrawl> findToRun(long currDate);
}
