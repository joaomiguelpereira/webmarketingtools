package com.pluralsense.mkktools.twitter.friendcrawl.running;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 6/10/14
 * Time: 5:01 PM
 * To change this template use File | Settings | File Templates.
 */
@Component
public class RunFriendCrawlRoute extends RouteBuilder{


    @Autowired
    private RunFriendCrawlProcessor runFriendCrawlProcessor;

    @Override
    public void configure() throws Exception {
       // from("jms:twitter_friend_crawl_run_queue").process(runFriendCrawlProcessor);

    }
}
