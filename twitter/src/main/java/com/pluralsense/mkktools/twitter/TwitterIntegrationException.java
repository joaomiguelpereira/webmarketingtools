package com.pluralsense.mkktools.twitter;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 6/7/14
 * Time: 4:08 PM
 * To change this template use File | Settings | File Templates.
 */
public class TwitterIntegrationException extends RuntimeException {
    public TwitterIntegrationException(String message) {
        super(message);
    }

    public TwitterIntegrationException(Throwable e) {
        super(e);
    }
}
