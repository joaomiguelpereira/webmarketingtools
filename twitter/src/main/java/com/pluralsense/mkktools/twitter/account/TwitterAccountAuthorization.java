package com.pluralsense.mkktools.twitter.account;

import javax.persistence.Embeddable;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 6/7/14
 * Time: 8:11 PM
 * To change this template use File | Settings | File Templates.
 */
@Embeddable
public class TwitterAccountAuthorization {

    private String accessToken;
    private String accessTokenSecret;

    public TwitterAccountAuthorization() {
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getAccessTokenSecret() {
        return accessTokenSecret;
    }

    public void setAccessTokenSecret(String accessTokenSecret) {
        this.accessTokenSecret = accessTokenSecret;
    }
}
