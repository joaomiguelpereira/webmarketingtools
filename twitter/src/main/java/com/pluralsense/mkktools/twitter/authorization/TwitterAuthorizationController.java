package com.pluralsense.mkktools.twitter.authorization;

import com.pluralsense.mkktools.accounts.Account;
import com.pluralsense.mkktools.accounts.web.AccountUserAwareController;
import com.pluralsense.mkktools.twitter.TwitterIntegrationException;
import com.pluralsense.mkktools.twitter.account.TwitterAccount;
import com.pluralsense.mkktools.twitter.account.TwitterAccountFactory;
import com.pluralsense.mkktools.twitter.account.TwitterAccountRepository;
import com.pluralsense.mkktools.twitter.application.TwitterApplication;
import com.pluralsense.mkktools.twitter.application.TwitterApplicationRepository;
import com.pluralsense.mkktools.twitter.integration.model.ApplicationRequestToken;
import com.pluralsense.mkktools.twitter.integration.TwitterApi;
import com.pluralsense.mkktools.twitter.integration.TwitterApiFactory;
import com.pluralsense.mkktools.twitter.integration.model.ApplicationRequestTokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import twitter4j.auth.RequestToken;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 6/7/14
 * Time: 7:19 PM
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class TwitterAuthorizationController extends AccountUserAwareController{


    @Autowired
    private TwitterApplicationRepository twitterApplicationRepository;

    @Autowired
    private ApplicationRequestTokenRepository applicationRequestTokenRepository;
    
    @Autowired
    private TwitterAccountRepository twitterAccountRepository;

    @Autowired
    private TwitterApiFactory twitterApiFactory;

    @Autowired
    private TwitterAccountFactory twitterAccountFactory;

    @RequestMapping(value = "integration/twitter/authorizationCallback", method = RequestMethod.GET, produces = "*/*", consumes = "*/*")
    @ResponseBody
    @Transactional
    public String authorizationCallback(@RequestParam("oauth_token") String requestToken, @RequestParam("oauth_verifier") String verifier) {


        ApplicationRequestToken token = applicationRequestTokenRepository.findByToken(requestToken);
        if ( token ==  null ) {
            return "Something went wrong. The Token was not found here";            
        }
        Account account = accountRepository.findOne(token.getAccountId());
        if ( account == null ) {
            return "Something went wrong. The Token do not belong to any account!";
        }



        TwitterAccount twitterAccount = twitterAccountFactory.createFor(account, token,verifier);
        TwitterAccount existingTwitterAccount = twitterAccountRepository.findByAccountIdAndTwitterId(account.getId(), twitterAccount.getTwitterId());
        applicationRequestTokenRepository.delete(token);
        if (existingTwitterAccount!=null) {
            return "The current account already have this Twitter Account configured";
        }

        twitterAccountRepository.save(twitterAccount);


        return "Ok. You can close this window";
    }


    @Transactional
    @RequestMapping(value = "/api/accounts/{accountId}/twitter/authorization",method = RequestMethod.GET)
    @ResponseBody
    public AuthorizationDetails getAuthorizationDetails(@PathVariable("accountId") Long accountId) {
        //Used to check if account belong to logged user
        Account account = getAccount(accountId);

        TwitterApplication twitterApplication = twitterApplicationRepository.findByAccountId(accountId);
        if ( twitterApplication == null ) {
            throw new TwitterIntegrationException("No application configured for this account!");
        }

        TwitterApi api =  twitterApiFactory.get(twitterApplication.getApiKey(), twitterApplication.getApiSecret());

        RequestToken requestToken = api.getRequestToken();
        ApplicationRequestToken applicationRequestToken = new ApplicationRequestToken(accountId, requestToken.getAuthenticationURL(), requestToken.getAuthorizationURL(), requestToken.getToken(), requestToken.getTokenSecret());

        applicationRequestTokenRepository.save(applicationRequestToken);

        return new AuthorizationDetails(requestToken.getAuthorizationURL(),requestToken.getAuthenticationURL());




    }


}
