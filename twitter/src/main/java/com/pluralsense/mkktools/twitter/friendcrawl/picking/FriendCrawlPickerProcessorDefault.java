package com.pluralsense.mkktools.twitter.friendcrawl.picking;

import com.pluralsense.mkktools.twitter.TwitterIntegrationException;
import com.pluralsense.mkktools.twitter.friendcrawl.FriendCrawl;
import com.pluralsense.mkktools.twitter.friendcrawl.FriendCrawlRepository;
import org.apache.camel.EndpointInject;
import org.apache.camel.Exchange;
import org.apache.camel.ProducerTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.Calendar;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 6/10/14
 * Time: 5:56 PM
 * To change this template use File | Settings | File Templates.
 */
@Component
public class FriendCrawlPickerProcessorDefault implements FriendCrawlPickerProcessor {

    private static final Logger LOG = LoggerFactory.getLogger(FriendCrawlPickerProcessorDefault.class);

    @Autowired
    private EntityManager em;
    @Autowired
    private FriendCrawlRepository friendCrawlRepository;

    @EndpointInject(uri = "jms:twitter_friend_crawl_run_queue")
    private ProducerTemplate producerTemplate;


    @Override
    @Transactional
    public void process(Exchange exchange) throws Exception {
        long time = Calendar.getInstance().getTimeInMillis();
        LOG.info("Running........."+time);

        List<FriendCrawl> friendCrawls = friendCrawlRepository.findToRun(time );

        for (FriendCrawl friendCrawl : friendCrawls) {
            process(friendCrawl);

        }


    }

    private void process(FriendCrawl friendCrawl) {
      FriendCrawl updateFriendCrawl = friendCrawlRepository.findOne(friendCrawl.getId());
        if ( updateFriendCrawl == null) {
            throw new TwitterIntegrationException("Could not find FriendCrawl in DB");
        }

        long currendDate = Calendar.getInstance().getTimeInMillis();
        int intervalSeconds = friendCrawl.getFriendCrawlExecutionParams().getIntervalInSeconds();

        long lastRun = currendDate;

        long nextRun = lastRun + (intervalSeconds * 1000);

        updateFriendCrawl.getFriendCrawlExecutionParams().setNextRun(nextRun);
        updateFriendCrawl.getFriendCrawlExecutionParams().setLastRun(currendDate);
        updateFriendCrawl.setRuns(friendCrawl.getRuns() + 1);
        //friendCrawlRepository.save(updateFriendCrawl);


        //em.flush();

        //updateFriendCrawl.setStatus(FriendCrawlStatus.IN_TRANSIT);
        producerTemplate.sendBody(friendCrawl.getId());
    }
}
