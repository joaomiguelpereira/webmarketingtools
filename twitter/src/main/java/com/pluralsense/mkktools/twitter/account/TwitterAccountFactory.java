package com.pluralsense.mkktools.twitter.account;

import com.pluralsense.mkktools.accounts.Account;
import com.pluralsense.mkktools.twitter.TwitterIntegrationException;
import com.pluralsense.mkktools.twitter.account.TwitterAccount;
import com.pluralsense.mkktools.twitter.account.TwitterAccountAuthorization;
import com.pluralsense.mkktools.twitter.application.TwitterApplication;
import com.pluralsense.mkktools.twitter.application.TwitterApplicationRepository;
import com.pluralsense.mkktools.twitter.integration.TwitterApi;
import com.pluralsense.mkktools.twitter.integration.TwitterApiFactory;
import com.pluralsense.mkktools.twitter.integration.model.ApplicationRequestToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.User;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 6/7/14
 * Time: 11:58 PM
 * To change this template use File | Settings | File Templates.
 */
@Component
public class TwitterAccountFactory {

    @Autowired
    private TwitterApplicationRepository twitterApplicationRepository;

    @Autowired
    private TwitterApiFactory apiFactory;

    public TwitterAccount createFor(Account account, ApplicationRequestToken token, String verifier) {


        TwitterApplication twitterApplication = twitterApplicationRepository.findByAccountId(account.getId());
        if (twitterApplication == null) {
            throw new TwitterIntegrationException("Could not find configuration for twitter app!");
        }
        RequestToken requestToken = new RequestToken(token.getToken(), token.getTokenSecret());

        AccessToken accessToken;

        Twitter twitter = apiFactory.getTwitter(twitterApplication.getApiKey(), twitterApplication.getApiSecret());

        try {
            accessToken = twitter.getOAuthAccessToken(requestToken, verifier);
        } catch (TwitterException e) {
            throw new TwitterIntegrationException(e);
        }


        TwitterAccount twitterAccount = new TwitterAccount();
        twitterAccount.setAccountId(account.getId());
        TwitterAccountAuthorization twitterAccountAuthorization = new TwitterAccountAuthorization();
        twitterAccountAuthorization.setAccessToken(accessToken.getToken());
        twitterAccountAuthorization.setAccessTokenSecret(accessToken.getTokenSecret());
        twitterAccount.setTwitterAccountAuthorization(twitterAccountAuthorization);


        TwitterApi api = apiFactory.get(twitterApplication.getApiKey(), twitterApplication.getApiSecret(), twitterAccount.getTwitterAccountAuthorization().getAccessToken(), twitterAccount.getTwitterAccountAuthorization().getAccessTokenSecret());

        return sychronizeAccount(twitterAccount, api);


    }

    public TwitterAccount sychronizeAccount(TwitterAccount twitterAccount, TwitterApi twitterApi) {

        User user = twitterApi.getUser();
        twitterAccount.setScreenName(user.getScreenName());
        twitterAccount.setName(user.getName());
        twitterAccount.setStatus(user.getStatus().getText());
        twitterAccount.setStatusDate(user.getStatus().getCreatedAt());

        twitterAccount.setStatusesCount(user.getStatusesCount());
        twitterAccount.setRateLimit(user.getRateLimitStatus().getLimit());
        twitterAccount.setRateLimitRemaining(user.getRateLimitStatus().getRemaining());
        twitterAccount.setRateLimitResetTimeInSecond(user.getRateLimitStatus().getResetTimeInSeconds());
        twitterAccount.setRateLimitSecondsUntilReset(user.getRateLimitStatus().getSecondsUntilReset());
        twitterAccount.setProfileImageUrl(user.getProfileImageURL());
        twitterAccount.setFriendsCount(user.getFriendsCount());
        twitterAccount.setFollowersCount(user.getFollowersCount());
        twitterAccount.setTwitterId(user.getId());

        return twitterAccount;

    }
}
