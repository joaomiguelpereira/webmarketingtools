package com.pluralsense.mkktools.twitter.friendcrawl.logging;

import com.pluralsense.mkktools.accounts.Account;
import com.pluralsense.mkktools.accounts.AccountRepository;
import com.pluralsense.mkktools.twitter.TwitterIntegrationException;
import com.pluralsense.mkktools.twitter.account.TwitterAccount;
import com.pluralsense.mkktools.twitter.account.TwitterAccountRepository;
import com.pluralsense.mkktools.twitter.friendcrawl.FriendCrawl;
import com.pluralsense.mkktools.twitter.friendcrawl.FriendCrawlRepository;
import com.pluralsense.mkktools.twitter.friendcrawl.FriendCrawlStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 6/10/14
 * Time: 10:34 PM
 * To change this template use File | Settings | File Templates.
 */
@Component
public class FriendCrawlLoggerDefault implements FriendCrawlLogger {

    @Autowired
    private FriendCrawlRepository friendCrawlRepository;

    @Autowired
    private FriendCrawlLogRespository friendCrawlLogRespository;

    @Autowired
    private TwitterAccountRepository twitterAccountRepository;

    @Autowired
    private AccountRepository accountRepository;

    @Override
    public void logEvent(Long friendCrawlId, FriendCrawlStrategy strategy, String message) {

        FriendCrawl friendCrawl = friendCrawlRepository.findOne(friendCrawlId);
        if ( friendCrawl == null ) {
            throw new TwitterIntegrationException("Could not find a FriendCrawl with ID: "+friendCrawlId);
        }
        TwitterAccount twitterAccount = twitterAccountRepository.findOne(friendCrawl.getTwitterAccountId());
        if ( twitterAccount == null) {
            throw new TwitterIntegrationException("Could not find a Twitter Account with id: "+friendCrawl.getTwitterAccountId());
        }

        Account account = accountRepository.findOne(twitterAccount.getAccountId());
        if ( account == null) {

            throw new TwitterIntegrationException("Could not find a Account with id: "+twitterAccount.getAccountId());
        }
        friendCrawl.getTwitterAccountId();

        FriendCrawlLog log = new FriendCrawlLog(account.getId(), account.getName(),twitterAccount.getId(), twitterAccount.getScreenName(), strategy.getName().name(), message);

        friendCrawlLogRespository.save(log);


    }
}
