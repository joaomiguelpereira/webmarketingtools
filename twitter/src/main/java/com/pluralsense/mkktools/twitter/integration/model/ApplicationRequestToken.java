package com.pluralsense.mkktools.twitter.integration.model;

import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 6/7/14
 * Time: 7:35 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "TWITTER_APPLICATION_REQUEST_TOKEN")
public class ApplicationRequestToken {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TWITTER_APPLICATION_REQUEST_TOKEN_ID_SEQ")
    @SequenceGenerator(name = "TWITTER_APPLICATION_REQUEST_TOKEN_ID_SEQ", sequenceName = "TWITTER_APPLICATION_REQUEST_TOKEN_ID_SEQ")
    private Long id;

    private Long accountId;
    private String authenticationURL;
    private String authorizationURL;
    private String token;
    private String tokenSecret;

    public ApplicationRequestToken() {

    }

    public ApplicationRequestToken(Long accountId, String authenticationURL, String authorizationURL, String token, String tokenSecret) {

        this.accountId = accountId;
        this.authenticationURL = authenticationURL;
        this.authorizationURL = authorizationURL;
        this.token = token;
        this.tokenSecret = tokenSecret;

    }

    public String getAuthenticationURL() {
        return authenticationURL;
    }

    public String getAuthorizationURL() {
        return authorizationURL;
    }

    public String getToken() {
        return token;
    }

    public String getTokenSecret() {
        return tokenSecret;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAccountId() {
        return accountId;
    }
}
