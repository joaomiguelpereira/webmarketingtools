package com.pluralsense.mkktools.twitter.friendcrawl.strategyrunning;

import com.pluralsense.mkktools.twitter.friendcrawl.StrategyName;
import com.pluralsense.mkktools.twitter.friendcrawl.logging.FriendCrawlLogger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 6/10/14
 * Time: 10:08 PM
 * To change this template use File | Settings | File Templates.
 */
@Component(value = "OTHER_USER_FRIENDS")
public class OtherUserFriendsFriendCrawlStrategyProcessorImpl extends AbstractFriendCrawlStrategyProcessor {

    @Autowired
    private FriendCrawlLogger crawlLogger;

    private final Logger LOG = LoggerFactory.getLogger(OtherUserFriendsFriendCrawlStrategyProcessorImpl.class);
    @Override
    protected void process(ProcessStrategyMessage message) {
        LOG.info("Processing {}",message);
        crawlLogger.logEvent(message.getFriendCrawlId(), message.getStrategy(), "Processing");


    }

    @Override
    protected boolean supports(StrategyName name) {
        return name.equals(StrategyName.OTHER_USER_FRIENDS);
    }
}
