package com.pluralsense.mkktools.twitter.friendcrawl.strategyrunning;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 6/10/14
 * Time: 9:52 PM
 * To change this template use File | Settings | File Templates.
 */
@Component
public class RunStrategyRouter extends RouteBuilder {

    @Autowired
    @Qualifier(value = "STATUS_KEYWORDS")
    private FriendCrawlStrategyProcessor keywordFriendCrawlStrategyProcessor;

    @Autowired
    @Qualifier(value = "OTHER_USER_FRIENDS")
    private FriendCrawlStrategyProcessor otherFriendsFriendCrawlStrategyProcessor;


    @Autowired
    @Qualifier(value = "OTHER_USER_FOLLOWERS")
    private FriendCrawlStrategyProcessor otherFriendsFollowersCrawlStrategyProcessor;


    @Override
    public void configure() throws Exception {
       /* from("jms:twitter_friend_crawl_strategy_run_queue").process(keywordFriendCrawlStrategyProcessor).
                process(otherFriendsFriendCrawlStrategyProcessor).process(otherFriendsFollowersCrawlStrategyProcessor);*/

    }
}
