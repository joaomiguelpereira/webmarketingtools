package com.pluralsense.mkktools.twitter.integration;

import com.pluralsense.mkktools.twitter.TwitterIntegrationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.User;
import twitter4j.auth.RequestToken;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 6/7/14
 * Time: 7:28 PM
 * To change this template use File | Settings | File Templates.
 */


public class TwitterApi {
    private final Twitter twitter4j;

    private final static Logger LOG = LoggerFactory.getLogger(TwitterApi.class);
    public TwitterApi(Twitter instance) {
        this.twitter4j = instance;
    }

    public RequestToken getRequestToken() {
        RequestToken requestToken;
        try {
            requestToken = this.twitter4j.getOAuthRequestToken();
        } catch (TwitterException e) {
               LOG.error("Error in twitte ",e);
            throw new TwitterIntegrationException(e);

        }
        return requestToken;




    }

    public User getUser() {
        String currentUsername = null;
        try {
            currentUsername = twitter4j.getScreenName();
            return twitter4j.showUser(currentUsername);
        } catch (TwitterException e) {
            throw new TwitterIntegrationException(e);

        }


    }
}
