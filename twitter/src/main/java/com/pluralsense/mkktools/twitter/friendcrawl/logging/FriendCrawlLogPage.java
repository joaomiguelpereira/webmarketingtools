package com.pluralsense.mkktools.twitter.friendcrawl.logging;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 6/12/14
 * Time: 12:38 AM
 * To change this template use File | Settings | File Templates.
 */
public class FriendCrawlLogPage {

    private final List<FriendCrawlLog> logs;
    private final int currentPage;
    private final long totalRecords;

    public FriendCrawlLogPage(List<FriendCrawlLog> logs, int currentPage, long totalRecords) {
        this.logs = logs;
        this.currentPage = currentPage;
        this.totalRecords = totalRecords;

    }

    public List<FriendCrawlLog> getLogs() {
        return logs;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public long getTotalRecords() {
        return totalRecords;
    }
}
