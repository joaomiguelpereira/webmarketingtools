package com.pluralsense.mkktools.twitter.integration;

import org.springframework.stereotype.Component;
import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 6/7/14
 * Time: 7:32 PM
 * To change this template use File | Settings | File Templates.
 */
@Component
public class TwitterApiFactory {

    public TwitterApi get(String apiKey, String apiSecret) {

        ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
        configurationBuilder.setOAuthConsumerKey(apiKey);
        configurationBuilder.setOAuthConsumerSecret(apiSecret);
        TwitterFactory twitterFactory = new TwitterFactory(configurationBuilder.build());
        return new TwitterApi(twitterFactory.getInstance());


    }

    public TwitterApi get(String apiKey, String apiSecret, String token, String tokenSecret) {
        ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
        configurationBuilder.setOAuthConsumerKey(apiKey);
        configurationBuilder.setOAuthConsumerSecret(apiSecret);
        configurationBuilder.setOAuthAccessToken(token);
        configurationBuilder.setOAuthAccessTokenSecret(tokenSecret);
        TwitterFactory twitterFactory = new TwitterFactory(configurationBuilder.build());
        return new TwitterApi(twitterFactory.getInstance());


    }

    public Twitter getTwitter(String apiKey, String apiSecret) {
        ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
        configurationBuilder.setOAuthConsumerKey(apiKey);
        configurationBuilder.setOAuthConsumerSecret(apiSecret);
        TwitterFactory twitterFactory = new TwitterFactory(configurationBuilder.build());
        return twitterFactory.getInstance();

    }
}
