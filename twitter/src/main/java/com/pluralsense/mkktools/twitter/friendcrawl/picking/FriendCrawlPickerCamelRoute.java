package com.pluralsense.mkktools.twitter.friendcrawl.picking;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 6/8/14
 * Time: 8:07 PM
 * To change this template use File | Settings | File Templates.
 */
@Component
public class FriendCrawlPickerCamelRoute extends RouteBuilder {

    @Autowired
    private FriendCrawlPickerProcessor friendCrawlPickerProcessor;


    private String cronExpression = "0/30+*+*+*+*+?";

    @Override
    public void configure() throws Exception {
        //from("quartz2://myGroup/myTimerName?cron=" + cronExpression).process(friendCrawlPickerProcessor);

    }
}
