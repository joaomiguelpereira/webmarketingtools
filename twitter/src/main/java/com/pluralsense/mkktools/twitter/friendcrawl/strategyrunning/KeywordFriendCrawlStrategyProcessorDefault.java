package com.pluralsense.mkktools.twitter.friendcrawl.strategyrunning;

import com.pluralsense.mkktools.twitter.TwitterApiBuilder;
import com.pluralsense.mkktools.twitter.friendcrawl.StrategyName;
import com.pluralsense.mkktools.twitter.friendcrawl.logging.FriendCrawlLogger;
import com.pluralsense.mkktools.twitter.integration.TwitterApi;
import com.pluralsense.mkktools.twitter.integration.TwitterApiFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 6/10/14
 * Time: 9:56 PM
 * To change this template use File | Settings | File Templates.
 */
@Component(value = "STATUS_KEYWORDS")
public class KeywordFriendCrawlStrategyProcessorDefault extends AbstractFriendCrawlStrategyProcessor {

    @Autowired
    private FriendCrawlLogger crawlLogger;


    @Autowired
    private TwitterApiBuilder twitterApiBuilder;



    private static final Logger LOG = LoggerFactory.getLogger(KeywordFriendCrawlStrategyProcessorDefault.class);
    @Override
    protected void process(ProcessStrategyMessage message) {
        LOG.info("Processing "+message);
        crawlLogger.logEvent(message.getFriendCrawlId(), message.getStrategy(), "Processing");

        TwitterApi api = twitterApiBuilder.buildFromFriendCrawlId(message.getFriendCrawlId());

        api.getUser();

    }

    @Override
    protected boolean supports(StrategyName name) {
        return name.equals(StrategyName.STATUS_KEYWORDS);
    }
}
