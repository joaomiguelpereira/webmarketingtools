package com.pluralsense.mkktools.twitter.friendcrawl.logging;

import com.pluralsense.mkktools.accounts.Account;
import com.pluralsense.mkktools.accounts.web.AccountUserAwareController;
import com.pluralsense.mkktools.twitter.TwitterIntegrationException;
import com.pluralsense.mkktools.twitter.account.TwitterAccount;
import com.pluralsense.mkktools.twitter.account.TwitterAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 6/12/14
 * Time: 12:10 AM
 * To change this template use File | Settings | File Templates.
 */

@Controller
@RequestMapping("/api/accounts/{accountId}/twitter/accounts/{id}/friendCrawl/log")
public class FriendCrawlLogController extends AccountUserAwareController {


    private static final int PAGE_SIZE = 50;
    @Autowired
    private TwitterAccountRepository twitterAccountRepository;

    @Autowired
    private FriendCrawlLogRespository friendCrawlLogRespository;

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    @Transactional
    public FriendCrawlLogPage index(@PathVariable("accountId") Long accountId, @PathVariable("id") Long id, @RequestParam(defaultValue = "0", required = false, value = "page") int page) {
        Account account = getAccount(accountId);

        TwitterAccount twitterAccount = twitterAccountRepository.findByIdAndAccountId(id,account.getId());
        if ( twitterAccount == null ) {
            throw new TwitterIntegrationException("Account not found or do not belong to this user");
        }
        return new FriendCrawlLogPage(friendCrawlLogRespository.findAll(page, PAGE_SIZE), page, friendCrawlLogRespository.countAll());

    }
    @RequestMapping(method = RequestMethod.DELETE)
    @ResponseBody
    @Transactional
    public void remove(@PathVariable("accountId") Long accountId, @PathVariable("id") Long id) {
        Account account = getAccount(accountId);

        TwitterAccount twitterAccount = twitterAccountRepository.findByIdAndAccountId(id,account.getId());
        if ( twitterAccount == null ) {
            throw new TwitterIntegrationException("Account not found or do not belong to this user");
        }

        friendCrawlLogRespository.removeAll();

    }

}
