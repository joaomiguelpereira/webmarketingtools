package com.pluralsense.mkktools.twitter.friendcrawl;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 6/10/14
 * Time: 7:02 PM
 * To change this template use File | Settings | File Templates.
 */
public enum FriendCrawlStatus {
    IN_TRANSIT,
    RUNNING,
    STOPPED
}
