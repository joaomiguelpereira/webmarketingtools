package com.pluralsense.mkktools.twitter.friendcrawl.logging;

import com.pluralsense.mkktools.persistence.AbstractMongoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 6/10/14
 * Time: 10:32 PM
 * To change this template use File | Settings | File Templates.
 */
@Component
public class FriendCrawlLogRespository extends AbstractMongoRepository<FriendCrawlLog>{

    @Autowired
    protected FriendCrawlLogRespository(MongoTemplate mongoTemplate) {
        super(mongoTemplate);
    }

    public void removeAll() {
        Query query = Query.query(Criteria.where("id").exists(true));
        mongoTemplate.remove(query, FriendCrawlLog.class);

    }
}
