package com.pluralsense.mkktools.twitter.friendcrawl;

import com.pluralsense.mkktools.accounts.Account;
import com.pluralsense.mkktools.accounts.web.AccountUserAwareController;
import com.pluralsense.mkktools.twitter.TwitterIntegrationException;
import com.pluralsense.mkktools.twitter.account.TwitterAccount;
import com.pluralsense.mkktools.twitter.account.TwitterAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 6/8/14
 * Time: 7:28 PM
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping(value = "/api/accounts/{accountId}/twitter/accounts/{twitterAccountId}/friendCrawl")
public class FriendCrawlController extends AccountUserAwareController {


    @Autowired
    private FriendCrawlRepository friendCrawlRepository;
    @Autowired
    private TwitterAccountRepository twitterAccountRepository;

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    @Transactional
    public FriendCrawl get(@PathVariable("accountId") Long accountId, @PathVariable("twitterAccountId") Long twitterAccountId) {
        Account account = getAccount(accountId);
        TwitterAccount twitterAccount = twitterAccountRepository.findOne(twitterAccountId);
        if (twitterAccount==null) {
            throw new TwitterIntegrationException("Twitter Account not found");
        }
        if (account.getId() != twitterAccount.getAccountId() ) {
            throw new TwitterIntegrationException("This Twitter Account does not belong to selected Account");
        }

        FriendCrawl friendCrawl = friendCrawlRepository.findByTwitterAccountId(twitterAccountId);


        if ( friendCrawl == null ) {
            friendCrawl = createDefaultFriendCrawl(twitterAccountId);
            friendCrawlRepository.save(friendCrawl);
        }

        return friendCrawl;

    }
    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    @Transactional
    public FriendCrawl update(@PathVariable("accountId") Long accountId, @PathVariable("twitterAccountId") Long twitterAccountId, @RequestBody FriendCrawl theFriendCrawl) {
        Account account = getAccount(accountId);
        TwitterAccount twitterAccount = twitterAccountRepository.findOne(twitterAccountId);
        if (twitterAccount==null) {
            throw new TwitterIntegrationException("Twitter Account not found");
        }
        if (account.getId() != twitterAccount.getAccountId() ) {
            throw new TwitterIntegrationException("This Twitter Account does not belong to selected Account");
        }

        FriendCrawl friendCrawl = friendCrawlRepository.findByTwitterAccountId(twitterAccountId);


        if ( friendCrawl == null ) {
            throw new TwitterIntegrationException("This account does not contains a default FriendCrawl!!");
        }
        friendCrawlRepository.save(theFriendCrawl);

        return friendCrawl;

    }

    private FriendCrawl createDefaultFriendCrawl(Long twitterAccountId) {
        FriendCrawl friendCrawl = new FriendCrawl();
        friendCrawl.setTwitterAccountId(twitterAccountId);

        for (StrategyName strategyName : StrategyName.values() ) {
            FriendCrawlStrategy strategy = new FriendCrawlStrategy();
            strategy.setName(strategyName);
            strategy.setOptions("");
            friendCrawl.addStrategy(strategy);
        }


        FriendCrawlExecutionParams executionParams = new FriendCrawlExecutionParams();

        executionParams.setActive(false);
        executionParams.setIntervalInSeconds(60);
        executionParams.setResultsSizeLimit(500);
        executionParams.setRunIfRateLimitHigherThan(90);
        friendCrawl.setFriendCrawlExecutionParams(executionParams);
        friendCrawl.setStatus(FriendCrawlStatus.STOPPED);
        friendCrawl.setRuns(0);
        return friendCrawl;

    }


}
