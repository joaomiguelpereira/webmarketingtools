package com.pluralsense.mkktools.webapp.config;


import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.jms.ConnectionFactory;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 6/10/14
 * Time: 5:10 PM
 * To change this template use File | Settings | File Templates.
 */
//@Configuration
public class JmsConfig {

    @Bean
    public ConnectionFactory connectionFactory() {
        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory();
        activeMQConnectionFactory.setUserName("admin");
        activeMQConnectionFactory.setPassword("admin");
        activeMQConnectionFactory.setClientID("Pluralsense APP");
        activeMQConnectionFactory.setBrokerURL("http://localhost:61616");

        return activeMQConnectionFactory;

    }
}
