package com.pluralsense.mkktools.webapp.config;

import org.apache.commons.dbcp.BasicDataSource;
import org.hibernate.dialect.PostgreSQL9Dialect;
import org.postgresql.util.PSQLDriverVersion;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.sql.Driver;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 6/2/14
 * Time: 11:35 PM
 * To change this template use File | Settings | File Templates.
 */

@Configuration
@EnableJpaRepositories(basePackages = "com.pluralsense.mkktools")
@EnableTransactionManagement
public class JpaPersistenceConfig {


    @Bean
    public DataSource dataSource() {

        BasicDataSource basicDataSource = new BasicDataSource();
        String dbUrl = "jdbc:postgresql://localhost:5432/pluralsense";
         basicDataSource.setDriverClassName("org.postgresql.Driver");
        basicDataSource.setUrl(dbUrl);
        basicDataSource.setUsername("pluralsense");
        basicDataSource.setPassword("pluralsense4all");
        return basicDataSource;

    }

    @Bean
    public EntityManagerFactory entityManagerFactory() {


        Map<String, String> jpaPropertiesMap = new HashMap();
        jpaPropertiesMap.put ("hibernate.hbm2ddl.auto", "validate");
        jpaPropertiesMap.put ("hibernate.dialect", "org.hibernate.dialect.PostgreSQL9Dialect");
        jpaPropertiesMap.put ("hibernate.format_sql", "true");
        jpaPropertiesMap.put ("hibernate.format_sql", "true");

        jpaPropertiesMap.put ("show_sql", "true");

        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        vendorAdapter.setGenerateDdl(false);

        LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
        factory.setJpaPropertyMap(jpaPropertiesMap);
        factory.setJpaVendorAdapter(vendorAdapter);
        factory.setPackagesToScan("com.pluralsense.mkktools");
        factory.setDataSource(dataSource());
        factory.afterPropertiesSet();

        return factory.getObject();
    }

    @Bean
    public PlatformTransactionManager transactionManager() {

        JpaTransactionManager txManager = new JpaTransactionManager();
        txManager.setEntityManagerFactory(entityManagerFactory());
        return txManager;
    }


}
