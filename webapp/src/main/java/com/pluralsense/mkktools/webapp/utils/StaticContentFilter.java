package com.pluralsense.mkktools.webapp.utils;

import javax.servlet.*;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 5/12/14
 * Time: 11:05 PM
 * To change this template use File | Settings | File Templates.
 */
public class StaticContentFilter implements Filter {
    private static final String DEFAULT_SERVLET_NAME = "default";
    private ServletContext servletContext;

    /**
     * {@inheritDoc}
     */
    public void destroy() {
    }

    /**
     * {@inheritDoc}
     */
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        servletContext.getNamedDispatcher(DEFAULT_SERVLET_NAME).forward(req, resp);
    }

    /**
     * {@inheritDoc}
     */
    public void init(FilterConfig config) throws ServletException {
        servletContext = config.getServletContext();
    }

}