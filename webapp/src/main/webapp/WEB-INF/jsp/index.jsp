<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<!DOCTYPE HTML>
<html>
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>Pluralsense.com</title>
    <!--JQuery-->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    -
    <!--Jquery Libs-->
    <script src="js/lib/jquery.pubsub.js"></script>

    <!--Include Bootstrap 3.0.3-->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="bootstrap/css/bootstrap-theme.min.css">
    <script src="bootstrap/js/bootstrap.min.js"></script>

    <!--Include Bootstrap 2-->
    <!--<link rel="stylesheet" href="bootstrap2/css/bootstrap.min.css">
    <script src="bootstrap2/js/bootstrap.min.js"></script>-->
    <!--Application CSS-->
    <link rel="stylesheet" href="css/pluralsense.css">

    <meta name="_csrf" content="${_csrf.token}"/>
    <!-- default header name is X-CSRF-TOKEN -->
    <meta name="_csrf_header" content="${_csrf.headerName}"/>


</head>
<body ng-app='application' ng-controller='ApplicationCtrl'>
<div growl></div>

<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">JfxTrade</a>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="#">Home</a></li>

                <li class="dropdown">
                    <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">Trading Instruments<b
                            class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="#/trading/instruments/">Manage</a></li>
                        <li><a href="#/twitter/accounts/">Accounts</a></li>
                        <li class="divider"></li>
                        <li><a href="#/twitter/followbots/">Follow Bots</a></li>
                        <li><a href="#/twitter/unfollowbots/">unFollow Bots</a></li>
                    </ul>
                </li>

                <li class="dropdown">
                    <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">Sheduler<b
                            class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="#/scheduler/eventjobs/">Event Jobs</a></li>
                        <li><a href="#/scheduler/activeEventJobTriggers/">Active Event Job Triggers</a></li>
                    </ul>
                </li>

                <li><a href="#contact">Contact</a></li>
            </ul>
        </div>
        <!--/.nav-collapse -->
    </div>
</div>


<div class="container">
    <div ng-view class="view-container">

    </div>
    <!-- /.container -->

    <!-- /.modal -->
</div>

</body>

<script src="js/lib/angular-file-upload/dist/angular-file-upload-shim.min.js"></script>



<!--Include AngularJS-->
<script src="js/angular/angular.js"></script>
<script src="js/angular/angular-route.js"></script>
<script src="js/angular/angular-resource.js"></script>
<script src="js/angular/ui-bootstrap-tpls-0.10.0.min.js"></script>
<script src="js/lib/truncate.js"></script>


<script src="js/lib/zeroclipboard/ZeroClipboard.min.js"></script>
<script src="js/lib/ng-clip/ng-clip.min.js"></script>

<script src="js/lib/angular-file-upload/dist/angular-file-upload.min.js"></script>

<script src="js/lib/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<link href="js/lib/datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">


<script src="js/lib/angular-growl/angular-growl.min.js"></script>
<link rel="stylesheet" href="js/lib/angular-growl/angular-growl.min.css">


<!--Application Level providers -->
<script src="js/app/services/application.providers.js"></script>
<script src="js/app/directives/percentage.js"></script>

<!--Accounts-->
<script src="js/app/accounts/accounts.js"></script>
<script src="js/app/accounts/accounts.services.js"></script>
<script src="js/app/accounts/accounts.controllers.js"></script>



<!--Twitter Application-->
<script src="js/app/twitter/application/twitter.application.js"></script>
<script src="js/app/twitter/application/twitter.application.services.js"></script>
<script src="js/app/twitter/application/twitter.application.controllers.js"></script>


<!--Twitter Accounts-->
<script src="js/app/twitter/accounts/twitter.accounts.js"></script>
<script src="js/app/twitter/accounts/twitter.accounts.services.js"></script>
<script src="js/app/twitter/accounts/twitter.accounts.controllers.js"></script>


<!--Trading Accounts-->
<script src="js/app/accounts/instruments/trading.instruments.js"></script>
<script src="js/app/accounts/instruments/trading.instruments.services.js"></script>
<script src="js/app/accounts/instruments/trading.instruments.controllers.js"></script>


<!--Trades-->
<script src="js/app/accounts/trades/account.trades.js"></script>
<script src="js/app/accounts/trades/account.trades.services.js"></script>
<script src="js/app/accounts/trades/account.trades.controllers.js"></script>

<!--Application-->
<script src="js/app/application.js"></script>
<script src="js/app/controllers/application.controllers.js"></script>




<script type="text/javascript">

    $('body').on('focus',".form_datetime", function(){
        $(this).datetimepicker({
            //language:  'fr',
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            forceParse: 0,
            showMeridian: 1
        });
    });

    $('body').on('focus',".form_date", function(){
        $(this).datetimepicker({
            language: 'fr',
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0
        });
    });
    $('body').on('focus',".form_time", function(){
        $(this).datetimepicker({

        });
    });


</script>






</html>