'use strict';

var providers = angular.module('application.providers', []);


//Refactore
var _$app = function () {

    //Register handlers for messages


    var _showFeedbackMessage = function (cssClass, message) {

        $('#messagesModal div div div div').addClass(cssClass);
        $('#messagesModal div div div div span').html(message);
        $('#messagesModal').modal('show');


    };
    $('#messagesModal').on('hidden.bs.modal', function (e) {
        $('#messagesModal div div div div').removeClass("alert-danger").removeClass("alert-success");
        $('#messagesModal div div div div span').html("");
    });


    return {
        subscribe: function (topic, funct) {
            jQuery.pubsub.subscribe(topic, funct);
        },
        publish: function (topic, msg) {
            jQuery.pubsub.publish(topic, msg);
        },
        publishSuccessMessage: function (message) {
            _showFeedbackMessage("alert-success", message);

        },
        handleResponseError: function (error) {
            _showFeedbackMessage("alert-danger", error.data.message);

        },
        module: function (aModule) {

            var module = aModule;
            return {
                viewPath: function (viewName) {
                    return "js/app/" + module + "/views/" + viewName
                }
            }
        }
    }
}();




providers.provider('UserFlashes', function () {
    this.$get = function () {
        return {
            success: function (text) {
                _$app.publishSuccessMessage(text);
            }

        }
    }

});

providers.provider('viewResolver', [function () {

    this.$get = function () {
        return {
            module: function (moduleName) {
                _$app.module(moduleName);
            }
        }
    }

}]);







