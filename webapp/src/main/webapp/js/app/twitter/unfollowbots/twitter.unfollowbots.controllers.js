var twitterUnfollowbotsControllers = angular.module("twitter.unfollowbots.controllers", []);
/**
 * Unfollowe Bots Controller
 */
twitterUnfollowbotsControllers.controller('UnfollowbotsCtrl', ['$scope', 'UnfollowbotCollection', 'UserFlashes',
    function ($scope, UnfollowbotCollection, UserFlashes) {


        $scope.unfollowbots = UnfollowbotCollection.query();


        $scope.edit = function (unfollowbot) {
            $scope.unfollowbotInstance = unfollowbot;

        }

        $scope.newUnfollowbot = function () {
            //Initialize an instance
            $scope.unfollowbotInstance = {
                name: '',
                twitterUserScreenerOptions: {
                    followingToFollowerRatio: 0,
                    languageCode: 'EN',

                    activityMatcherOptions: {
                        minimumLastTweetAgeInDays: 0,
                        averageTweetsPerDay: 0,
                        minimumTweets: 0

                    },
                    keywordMatcherOptions: {
                        keywords: '',
                        updatesToAnalyse: 0,
                        minimumKeyWordCount: 0
                    },
                    socialActivityMatcherOptions: {
                        minFollowingFollowerRatio: 0.5,
                        maxFollowingFollowerRatio: 1.3
                    }

                }

            };
        }

        $scope.remove = function (unfollowbot) {
            UnfollowbotCollection.delete({id: unfollowbot.id}, function (data) {
                UserFlashes.success(data.message);

                $scope.unfollowbots = $scope.unfollowbots.filter(function (filter) {
                    return unfollowbot.id != filter.id;

                })

            })
        };

        $scope.$on('twitter.unfollowbot.clear.unfollowbotInstance', function (event) {
            $scope.unfollowbotInstance = undefined;
        });
        $scope.$on('twitter.unfollowbot.created', function (event, data) {
            $scope.unfollowbots.push(data);
        });


    }
]);


twitterUnfollowbotsControllers.controller('newUnfollowbotFormCtrl',
    ['$scope',
        'UnfollowbotCollection',
        'UserFlashes',
        function ($scope, UnfollowbotCollection, UserFlashes) {


            $scope.update = function () {
                UnfollowbotCollection.update({id: $scope.unfollowbotInstance.id},
                    $scope.unfollowbotInstance,
                    function (data) {
                        UserFlashes.success("Updated");
                        $scope.$emit('twitter.unfollowbot.clear.unfollowbotInstance');
                        $scope.$emit('twitter.unfollowbot.updated', data);

                    });
            }
            $scope.create = function () {
                UnfollowbotCollection.save({}, $scope.unfollowbotInstance, function (data) {
                    $scope.$emit('twitter.unfollowbot.clear.unfollowbotInstance');
                    $scope.$emit('twitter.unfollowbot.created', data);
                    UserFlashes.success("Created");

                });
            }
            $scope.cancel = function () {
                $scope.$emit('twitter.unfollowbot.clear.unfollowbotInstance');

            }

        }]);
