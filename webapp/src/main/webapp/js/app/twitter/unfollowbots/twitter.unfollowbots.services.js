'use strict';


/* Controllers */

var twitterUnfollowbotsServices = angular.module('twitter.unfollowbots.services', ['ngResource']);


twitterUnfollowbotsServices.factory('UnfollowbotCollection', ['$resource', function($resource) {
    return $resource("/api/twitter/unfollowbots/:id", {id: '@id'},
        {
            "update" : {method: 'PUT'}
        });
}]);




