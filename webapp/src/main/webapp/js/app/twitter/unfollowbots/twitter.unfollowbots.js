'use strict';


// Declare app level module which depends on filters, and services
var twitterUnfollowbotsModule = angular.module('twitter.unfollowbots', [
    'application.providers',
    'twitter.unfollowbots.controllers',
    'twitter.unfollowbots.services',
    'ngRoute',
    'ui.bootstrap'

]);

twitterUnfollowbotsModule.config(['$routeProvider',  function ($routeProvider) {

    $routeProvider.when('/twitter/unfollowbots',
        {
            templateUrl: _$app.module('twitter/unfollowbots').viewPath('unfollowbots.html'),controller: 'UnfollowbotsCtrl'
        }
    );

}]);
