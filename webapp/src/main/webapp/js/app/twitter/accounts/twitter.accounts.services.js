'use strict';

/* Controllers */

var twitterAccountsServices = angular.module('twitter.accounts.services', ['ngResource']);


twitterAccountsServices.factory('ApplicationAuthorizationDetails', ['$resource', function($resource) {
    return $resource("/api/accounts/:accountId/twitter/authorization");
}]);




twitterAccountsServices.factory('TwitterAccountCollection', ['$resource', function($resource) {
    return $resource("/api/accounts/:accountId/twitter/accounts/:id",{});
}]);

twitterAccountsServices.factory('TwitterAccountFriendCrawl', ['$resource', function($resource) {
    return $resource("/api/accounts/:accountId/twitter/accounts/:id/friendCrawl",{});
}]);

twitterAccountsServices.factory('TwitterAccountFriendCrawlLogCollection', ['$resource', function($resource) {
    return $resource("/api/accounts/:accountId/twitter/accounts/:id/friendCrawl/log",{});
}]);


/*

twitterAccountsServices.factory('Account', ['$resource', function($resource) {
    return $resource("/api/twitter/accounts/:id",{},{
        createUpdate : {
            method: 'POST',
            url: "/api/twitter/accounts/:id/statuses"
        }
    });
}]);*/

