/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 1/21/14
 * Time: 9:35 PM
 * To change this template use File | Settings | File Templates.
 */


var twitterAccountsControllers = angular.module('twitter.accounts.controllers', ['twitter.accounts.services', 'accounts.services']);

/**
 * Accounts Controller
 */
twitterAccountsControllers.controller("TwitterAccountsCtrl",
    ['TwitterAccountCollection',
        'AccountCollection',
        'ApplicationAuthorizationDetails',
        '$scope',
        '$routeParams',
        'growl',
        '$window',

        function (TwitterAccountCollection, AccountCollection, ApplicationAuthorizationDetails, $scope, $routeParams, growl, $window) {
            if (!$routeParams.accountId) {
                growl.addErrorMessage("No Account selected1");
                return;
            }
            $scope.twitterAccounts = TwitterAccountCollection.query({accountId: $routeParams.accountId})
            $scope.account = AccountCollection.get({id: $routeParams.accountId});


            $scope.authorization = function () {
                $scope.authorizationDetails = ApplicationAuthorizationDetails.get({accountId: $routeParams.accountId}, function (data) {
                    $window.open(data.authorizationURL);
                });
            }
            $scope.removeAccount = function (twitterAccount) {

                TwitterAccountCollection.delete({accountId: $scope.account.id, id: twitterAccount.id}, function () {
                    $scope.twitterAccounts = TwitterAccountCollection.query({accountId: $scope.account.id})
                    growl.addSuccessMessage("Twitter account removed");
                });

            }
            $scope.refresh = function () {
                $scope.twitterAccounts = TwitterAccountCollection.query({accountId: $scope.account.id})
            }
        }]);


twitterAccountsControllers.controller("TwitterAccountFriendCrawlCtrl",
    ['TwitterAccountCollection',
        'AccountCollection',
        'TwitterAccountFriendCrawl',
        '$scope',
        '$routeParams',
        'growl',
        function (TwitterAccountCollection, AccountCollection, TwitterAccountFriendCrawl, $scope, $routeParams, growl) {

            if (!$routeParams.accountId) {
                growl.addErrorMessage("No Account selected!");
                return;
            }
            if (!$routeParams.id) {
                growl.addErrorMessage("No Twitter Account selected!");
                return;
            }

            $scope.twitterAccount = TwitterAccountCollection.get({accountId: $routeParams.accountId, id: $routeParams.id})
            $scope.account = AccountCollection.get({id: $routeParams.accountId});
            $scope.friendCrawl = TwitterAccountFriendCrawl.get({accountId: $routeParams.accountId, id: $routeParams.id});

            $scope.updateUsersCrawConfiguration = function () {
                TwitterAccountFriendCrawl.save({accountId: $routeParams.accountId, id: $routeParams.id}, $scope.friendCrawl, function (data) {
                    growl.addSuccessMessage("Update!")
                })
            }


        }]);


twitterAccountsControllers.controller("TwitterAccountFriendViewCtrl",
    ['TwitterAccountCollection',
        'AccountCollection',
        '$scope',
        '$routeParams',
        'growl',

        function (TwitterAccountCollection, AccountCollection, $scope, $routeParams, growl) {

            if (!$routeParams.accountId) {
                growl.addErrorMessage("No Account selected!");
                return;
            }
            if (!$routeParams.id) {
                growl.addErrorMessage("No Twitter Account selected!");
                return;
            }


            $scope.twitterAccount = TwitterAccountCollection.get({accountId: $routeParams.accountId, id: $routeParams.id})
            $scope.account = AccountCollection.get({id: $routeParams.accountId});
            $scope.refresh = function () {
                $scope.twitterAccount = TwitterAccountCollection.get({accountId: $routeParams.accountId, id: $routeParams.id, refresh: true}, function () {
                    growl.addSuccessMessage("Account synchronized!");

                })
            }


        }]);




twitterAccountsControllers.controller("TwitterAccountFriendCrawlLogCtrl",
    ['$scope', '$routeParams','TwitterAccountFriendCrawlLogCollection','TwitterAccountCollection', 'AccountCollection',
        'growl', function ($scope, $routeParams, TwitterAccountFriendCrawlLogCollection, TwitterAccountCollection, AccountCollection, growl) {

        if (!$routeParams.accountId) {
            growl.addErrorMessage("No Account selected!");
            return;
        }
        if (!$routeParams.id) {
            growl.addErrorMessage("No Twitter Account selected!");
            return;
        }



        $scope.twitterAccount = TwitterAccountCollection.get({accountId: $routeParams.accountId, id: $routeParams.id})
        $scope.account = AccountCollection.get({id: $routeParams.accountId});

        $scope.logsPage = TwitterAccountFriendCrawlLogCollection.get({accountId: $routeParams.accountId, id: $routeParams.id},function(data) {
            setUpPage($scope);
        });

        $scope.removeAll = function() {

            TwitterAccountFriendCrawlLogCollection.remove({accountId: $routeParams.accountId, id: $routeParams.id}, function(data) {

                growl.addSuccessMessage("Log Cleared!");
                $scope.logsPage = {}

            });
        }

        $scope.nextPage = function() {

            $scope.logsPage = TwitterAccountFriendCrawlLogCollection.get({accountId: $routeParams.accountId, id: $routeParams.id, page : ($scope.logsPage.currentPage+1)},function(data) {
                    setUpPage($scope);
            });

        };


        $scope.previousPage = function() {

            $scope.logsPage = TwitterAccountFriendCrawlLogCollection.get({accountId: $routeParams.accountId, id: $routeParams.id, page : ($scope.logsPage.currentPage-1)},function(data) {
                setUpPage($scope);
            });

        };

        $scope.refresh= function() {
            $scope.logsPage = TwitterAccountFriendCrawlLogCollection.get({accountId: $routeParams.accountId, id: $routeParams.id, page : $scope.logsPage.currentPage},function(data) {
                setUpPage($scope);
            });

        }


        var setUpPage = function (scope) {

            //setUpPage($scope);
            scope.logsPage.pageSize = 50; //FIX
            scope.logsPage.numberOfPages = scope.logsPage.totalRecords/scope.logsPage.pageSize;

            scope.isLastPage = (scope.logsPage.currentPage == scope.logsPage.numberOfPages);
            scope.isFirstPage = (scope.logsPage.currentPage == 0);
            scope.percentage = scope.logsPage.currentPage / scope.logsPage.numberOfPages;


        }


    }]);




