'use strict';


// Declare app level module which depends on filters, and services
var twitterAccountsModule = angular.module('twitter.accounts', [
    'application.providers',
    'twitter.accounts.controllers',
    'twitter.accounts.services',
    'ngRoute',
    'ui.bootstrap'

]);

twitterAccountsModule.config(['$routeProvider',  function ($routeProvider) {
    $routeProvider.when('/accounts/:accountId/twitter/accounts', {templateUrl:_$app.module('twitter/accounts').viewPath('accounts.html'), controller: 'TwitterAccountsCtrl'});
    $routeProvider.when('/accounts/:accountId/twitter/accounts/:id/friendcrawl', {templateUrl:_$app.module('twitter/accounts').viewPath('friendcrawl.html'), controller: 'TwitterAccountFriendCrawlCtrl'});
    $routeProvider.when('/accounts/:accountId/twitter/accounts/:id/friendcrawl/log', {templateUrl:_$app.module('twitter/accounts').viewPath('friendcrawllog.html'), controller: 'TwitterAccountFriendCrawlLogCtrl'});
    $routeProvider.when('/accounts/:accountId/twitter/accounts/:id', {templateUrl:_$app.module('twitter/accounts').viewPath('view.html'), controller: 'TwitterAccountFriendViewCtrl'});

}]);
