'use strict';


/* Controllers */

var twitterFollowbotScheduledEventsServices = angular.module('twitter.followbotScheduledEvents.services', ['ngResource']);


twitterFollowbotScheduledEventsServices.factory('FollowbotScheduledEventCollection', ['$resource', function($resource) {
    return $resource("/api/twitter/followbots/:followbotId/followbotScheduledEvents/:id", {followbotId:'@followbotId', id: '@id'},
        {
            "update" : {method: 'PUT'}
        });
}]);


twitterFollowbotScheduledEventsServices.factory('FollowbotScheduledEventActivation', ['$resource', function($resource) {
    return $resource("/api/twitter/followbots/:followbotId/followbotScheduledEvents/:id/activation", {followbotId:'@followbotId', id: '@id'},
        {

        });
}]);


twitterFollowbotScheduledEventsServices.factory('FollowbotScheduledEventScreenQueue', ['$resource', function($resource) {
    return $resource("/api/twitter/followbots/:followbotId/followbotScheduledEvents/:id/screenQueue", {followbotId:'@followbotId', id: '@id'},
        {

        });
}]);

twitterFollowbotScheduledEventsServices.factory('FollowbotScheduledEventFollowQueue', ['$resource', function($resource) {
    return $resource("/api/twitter/followbots/:followbotId/followbotScheduledEvents/:id/followQueue", {followbotId:'@followbotId', id: '@id'},
        {

        });
}]);


twitterFollowbotScheduledEventsServices.factory('FollowbotScheduledEventQueueCommands', ['$resource', function($resource) {
    return $resource("/api/twitter/followbots/:followbotId/followbotScheduledEvents/:id/queues/commands", {followbotId:'@followbotId', id: '@id'},
        {

        });
}]);


twitterFollowbotScheduledEventsServices.factory('IgnoredUsersCache', ['$resource', function($resource) {
    return $resource("/api/twitter/followbots/:followbotId/followbotScheduledEvents/:id/ignoredUsersCache", {followbotId:'@followbotId', id: '@id'},
        {

        });
}]);
twitterFollowbotScheduledEventsServices.factory('IgnoredUsersCacheOperations', ['$resource', function($resource) {
    return $resource("/api/twitter/followbots/:followbotId/followbotScheduledEvents/:id/ignoredUsersCache/commands", {followbotId:'@followbotId', id: '@id'},
        {

        });
}]);














