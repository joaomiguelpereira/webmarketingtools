var twitterFollowbotScheduledEventsControllers = angular.module("twitter.followbotScheduledEvents.controllers", []);

twitterFollowbotScheduledEventsControllers.controller('FollowbotsScheduleCtrl', ['$scope', '$routeParams', 'FollowbotCollection',
    'FollowbotScheduledEventCollection', function ($scope, $routeParams, FollowbotCollection, FollowbotScheduledEventCollection) {


        if ($routeParams.id) {


            $scope.followbot = FollowbotCollection.get({id: $routeParams.id}, function (data) {

                $scope.followbotScheduledEvents = FollowbotScheduledEventCollection.query({followbotId: data.id}, function (data) {

                });
            });
            console.log($routeParams);
            console.dir($routeParams);
        } else {
            console.log("No route params");
        }

        $scope.newFollowbotScheduledEventInstance = function () {
            $scope.followbotScheduledEventInstance = {
                followbotId: $scope.followbot.id,
                name: '',
                applicationId: '',
                accountId: '',
                maxFollowingToFollowerRatio: 1.1,
                trigger: {
                    days: [],
                    hourCronExpression: '0',
                    minuteCronExpression: '30'
                },
                followbotScreenQueueOptions: {
                    maxSize: 100,
                    dequeueBatchSize: 5,
                    minuteCronExpression: "*/5"

                },
                followbotFollowQueueOptions: {
                    maxSize: 100,
                    dequeueBatchSize: 5,
                    minuteCronExpression: "*/5"
                }
            }
        };
        $scope.$on('twitter.followbotSheduledEvent.newInstance.destroy', function (event) {
            $scope.followbotScheduledEventInstance = undefined;
        });
        $scope.$on('twitter.followbotSheduledEvent.deleted', function (event, followbotInstance) {

            $scope.followbotScheduledEvents = $scope.followbotScheduledEvents.filter(function (filteredObject) {
                return filteredObject.id != followbotInstance.id;
            });

        });
        $scope.$on('twitter.followbotSheduledEvent.newInstance.saved', function (newInstantce) {
            //BUG
            //$scope.followbotScheduledEvents.push(newInstantce);
            //Reloading...instead
            console.log("reloadding...");
            $scope.followbotScheduledEvents = FollowbotScheduledEventCollection.query({followbotId: $scope.followbot.id}, function (data) {

            });


            $scope.followbotScheduledEventInstance = undefined;
        });

        $scope.$on('twitter.followbotSheduledEvent.instance.updated', function (event, updatedInstance) {


            console.log("reloadding...");


            for (var i = 0; i < $scope.followbotScheduledEvents.length; i++) {
                if ($scope.followbotScheduledEvents[i].id == updatedInstance.id) {

                    console.log("Found index: " + i);
                    $scope.followbotScheduledEvents[i] = updatedInstance;
                    break;

                }
            }
            //bug
            //$scope.followbotScheduledEvents = FollowbotScheduledEventCollection.query({followbotId: $scope.followbot.id}, function (data) {

            //});

            $scope.followbotScheduledEventInstance = undefined;

        });
        $scope.$on('twitter.followbotScheduledEvent.edit', function (event, data) {

            //BUG
            console.log("Setting for editing....");
            console.dir(data);
            $scope.followbotScheduledEventInstance = data;
        });


    }]);
twitterFollowbotScheduledEventsControllers.controller('newFollowbotScheduledEventFormCtrl', [
    '$scope',
    'ApplicationCollection',
    'AccountCollection',
    'FollowbotScheduledEventCollection',
    'UserFlashes',
    function ($scope, ApplicationCollection, AccountCollection, FollowbotScheduledEventCollection, UserFlashes) {

        $scope.applications = ApplicationCollection.query();

        $scope.DAYS = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];


        $scope.daysSelectorHelper = {
            allDaysSelected: false,

            toggleAll: function () {
                $scope.daysSelectorHelper.allDaysSelected = !$scope.daysSelectorHelper.allDaysSelected;
                console.log("Selceting All: " + $scope.daysSelectorHelper.allDaysSelected);
                if ($scope.daysSelectorHelper.allDaysSelected) {
                    angular.forEach($scope.DAYS, function (day) {
                        $scope.followbotScheduledEventInstance.trigger.days.push(day);
                    });

                } else {
                    $scope.followbotScheduledEventInstance.trigger.days = [];
                }
                console.dir($scope.followbotScheduledEventInstance.trigger.days);

            },
            toggleDay: function (day) {
                console.log("Seletecting : " + day);
                var idx = $scope.followbotScheduledEventInstance.trigger.days.indexOf(day);

                // is currently selected
                if (idx > -1) {
                    $scope.followbotScheduledEventInstance.trigger.days.splice(idx, 1);
                }

                // is newly selected
                else {
                    $scope.followbotScheduledEventInstance.trigger.days.push(day);
                }
                console.dir($scope.followbotScheduledEventInstance.trigger.days);

            }};
        $scope.create = function () {
            console.log("Updating...." + $scope.followbotScheduledEventInstance.followbotId);
            FollowbotScheduledEventCollection.save($scope.followbotScheduledEventInstance, function (data) {
                $scope.$emit('twitter.followbotSheduledEvent.newInstance.saved', data);
                UserFlashes.success("Created");
            })
        };

        $scope.cancel = function () {
            $scope.$emit('twitter.followbotSheduledEvent.newInstance.destroy');

        };

        $scope.update = function () {

            console.log("Updating...." + $scope.followbotScheduledEventInstance.followbotId);
            FollowbotScheduledEventCollection.update($scope.followbotScheduledEventInstance, function (data) {
                $scope.$emit('twitter.followbotSheduledEvent.instance.updated', data);
                UserFlashes.success("Updated");
            })


        }


        $scope.$watch("followbotScheduledEventInstance.applicationId", function (data) {
            if ($scope.followbotScheduledEventInstance && $scope.followbotScheduledEventInstance.applicationId) {
                $scope.accounts = AccountCollection.query({applicationId: $scope.followbotScheduledEventInstance.applicationId});
            }

        });

    }]);


twitterFollowbotScheduledEventsControllers.controller('followbotScheduledEventEntryCtrl', ['$scope',
    '$cacheFactory',
    'ApplicationCollection',
    'Account',
    'FollowbotScheduledEventCollection',
    'UserFlashes',
    'FollowbotScheduledEventActivation',
    function ($scope, $cacheFactory, ApplicationCollection, Account, FollowbotScheduledEventCollection, UserFlashes, FollowbotScheduledEventActivation) {

        $scope.followbotScheduledEventEntry = angular.copy($scope.followbotScheduledEvent);


        $scope.followbotScheduledEventEntry.application = function () {

            var applicationId = $scope.followbotScheduledEvent.applicationId;
            console.log("Loading Application name by Id + " + applicationId);
            if (!applicationId) {
                return "N/A";
            }
            var applicationCache = $cacheFactory.get("twitter.applications");
            if (!applicationCache) {
                console.log("Creating cache....")
                applicationCache = $cacheFactory("twitter.applications");
            }

            var chachedApplication = applicationCache.get(applicationId);

            if (!chachedApplication) {
                ApplicationCollection.get({id: applicationId}, function (data) {
                    //Get cache created elsewhere
                    if (!applicationCache.get(applicationId)) {
                        console.log("Adding to cache: " + applicationId + " -- " + data.name)
                        applicationCache.put(applicationId, data);
                    }

                    $scope.followbotScheduledEventEntry.application = data.name;
                    //console.log(applicationCache.size);
                });
                return "Loading....";
            }
            return chachedApplication.name;
        }();

        $scope.followbotScheduledEventEntry.account = function () {

            var accountId = $scope.followbotScheduledEvent.accountId;
            if (!accountId) {
                return "N/A";
            }
            var accountsCache = $cacheFactory.get("twitter.accounts");
            if (!accountsCache) {
                accountsCache = $cacheFactory("twitter.accounts");
            }

            var chachedAccount = accountsCache.get(accountId);

            if (!chachedAccount) {
                Account.get({id: accountId}, function (data) {
                    //Get cache created elsewhere
                    if (!accountsCache.get(accountId)) {
                        accountsCache.put(accountId, data);
                    }
                    //accountsCache.put(accountId,data);
                    $scope.followbotScheduledEventEntry.account = data.name;
                    //console.log(accountsCache.size);
                });
                return "Loading....";
            }
            return chachedAccount.name;
        }();
        ;
        //$scope.followbotScheduledEventEntry.trigger = "Trivver";
        $scope.followbotScheduledEventEntry.queues = function () {
            return "Test";
        }();

        $scope.viewTrigger = function () {
            window.alert($scope.followbotScheduledEventEntry.trigger.days + "\nHour:" + $scope.followbotScheduledEventEntry.trigger.hourCronExpression + "\nMinute: " + $scope.followbotScheduledEventEntry.trigger.minuteCronExpression);
        };


        $scope.edit = function () {
            $scope.$emit("twitter.followbotScheduledEvent.edit", $scope.followbotScheduledEvent);
        }


        $scope.remove = function () {


            FollowbotScheduledEventCollection.delete({followbotId: $scope.followbot.id, id: $scope.followbotScheduledEvent.id}, function () {
                UserFlashes.success("Removed");
                $scope.$emit('twitter.followbotSheduledEvent.deleted', $scope.followbotScheduledEvent);
            });
        };
        $scope.activate = function () {
            FollowbotScheduledEventActivation.save({followbotId: $scope.followbot.id, id: $scope.followbotScheduledEvent.id}, function (data) {

                $scope.$emit('twitter.followbotSheduledEvent.instance.updated', data);


            })
        };
        $scope.deactivate = function () {
            FollowbotScheduledEventActivation.delete({followbotId: $scope.followbot.id, id: $scope.followbotScheduledEvent.id}, function (data) {
                $scope.$emit('twitter.followbotSheduledEvent.instance.updated', data);
            });
        }

    }]);


twitterFollowbotScheduledEventsControllers.controller('FollowbotScheduleEventQueuesCtrl', ['$scope', '$routeParams',
    'FollowbotScheduledEventScreenQueue',
    'FollowbotScheduledEventFollowQueue',
    'IgnoredUsersCache',
    'FollowbotScheduledEventCollection',
    'FollowbotScheduledEventQueueCommands',
    'IgnoredUsersCacheOperations',
    function ($scope, $routeParams, FollowbotScheduledEventScreenQueue, FollowbotScheduledEventFollowQueue, IgnoredUsersCache, FollowbotScheduledEventCollection, FollowbotScheduledEventQueueCommands, IgnoredUsersCacheOperations) {


        $scope.screens = {
            'screenQueue': true,
            'ignoredUsersCache': false,
            'followQueue': false

        };

        $scope.showScreenQueue = true;

        if ($routeParams.id && $routeParams.followbotId) {

            $scope.screenQueue = FollowbotScheduledEventScreenQueue.get({followbotId: $routeParams.followbotId, id: $routeParams.id});
            $scope.followQueue = FollowbotScheduledEventFollowQueue.get({followbotId: $routeParams.followbotId, id: $routeParams.id});
            $scope.ignoredUsersCache = IgnoredUsersCache.get({followbotId: $routeParams.followbotId, id: $routeParams.id});

            $scope.showIgnoredUsersCache = true;

            $scope.followbotId = $routeParams.followbotId;
            $scope.followbotScheduledEvent = FollowbotScheduledEventCollection.get({followbotId: $scope.followbotId, id: $routeParams.id});
        } else {
            console.log("No route params");
            alert("Opps! No followbotId or id found in route params!!")
        }

        //TODO: Improve this
        var onScreenActive = function (screen) {
            if (screen == 'ignoredUsersCache') {
                $scope.ignoredUsersCache = IgnoredUsersCache.get({followbotId: $scope.followbotId, id: $scope.followbotScheduledEvent.id});
            }
            if (screen == 'screenQueue') {
                $scope.screenQueue = FollowbotScheduledEventScreenQueue.get({followbotId: $scope.followbotId, id: $scope.followbotScheduledEvent.id});
            }
            if (screen == 'followQueue') {
                $scope.followQueue = FollowbotScheduledEventFollowQueue.get({followbotId: $scope.followbotId, id: $scope.followbotScheduledEvent.id});
            }

        }

        $scope.removeFromIgnoredUsersCache = function (ignoredUsersCache, user) {
            IgnoredUsersCacheOperations.save({followbotId: $scope.followbotId, id: $scope.followbotScheduledEvent.id},
                {
                    command: "RemoveUser",
                    scheduledFollowEventId: $scope.followbotScheduledEvent.id,
                    twitterUserId: user.id
                }, function (data) {
                    $scope.ignoredUsersCache.friends = $scope.ignoredUsersCache.friends.filter(function (filteredObject) {
                        return filteredObject.id != user.id;
                    });
                }
            );
            //IgnoredUsersCache.get({followbotId: $routeParams.followbotId, id: $routeParams.id})

        };
        $scope.clearIgnoredUsersCache = function (ignoreUsersCache) {
            IgnoredUsersCacheOperations.save({followbotId: $scope.followbotId, id: $scope.followbotScheduledEvent.id},
                {
                    command: "ClearCache",
                    scheduledFollowEventId: $scope.followbotScheduledEvent.id
                }, function (data) {
                    $scope.ignoredUsersCache.friends = [];
                }
            );
        };

        $scope.show = function (screenName) {
            for (var key in $scope.screens) {
                if (key == screenName) {
                    $scope.screens[key] = true;
                    onScreenActive(key)
                } else {
                    $scope.screens[key] = false;
                }
            }


        };

        $scope.moveUserFromScreenQueueToIgnoreList = function (queue, user) {
            FollowbotScheduledEventQueueCommands.save({followbotId: $scope.followbotId, id: $scope.followbotScheduledEvent.id},
                {
                    command: "MoveUserFromScreenQueueToIgnoreList",
                    queueKey: queue.key,
                    twitterUserId: user.id
                }, function (data) {
                    $scope.screenQueue.cachedTwitterUsers = $scope.screenQueue.cachedTwitterUsers.filter(function (filteredObject) {
                        return filteredObject.id != user.id;
                    });
                }
            );
        };
        $scope.moveUserFromFollowQueueToIgnoreList = function (queue, user) {
            FollowbotScheduledEventQueueCommands.save({followbotId: $scope.followbotId, id: $scope.followbotScheduledEvent.id},
                {
                    command: "MoveUserFromFollowQueueToIgnoreList",
                    queueKey: queue.key,
                    twitterUserId: user.id
                }, function (data) {
                    $scope.followQueue.cachedTwitterUsers = $scope.followQueue.cachedTwitterUsers.filter(function (filteredObject) {
                        return filteredObject.id != user.id;
                    });
                }
            );
        };

        $scope.clearScreenQueue = function(screenQueue) {
            FollowbotScheduledEventQueueCommands.save({followbotId: $scope.followbotId, id: $scope.followbotScheduledEvent.id},
                {
                    command: "ClearScreenQueue",
                    queueKey: screenQueue.key
                }, function (data) {
                    $scope.screenQueue.cachedTwitterUsers = [];
                }
            );
        };
        $scope.clearFollowQueue = function(followQueue) {

            FollowbotScheduledEventQueueCommands.save({followbotId: $scope.followbotId, id: $scope.followbotScheduledEvent.id},
                {
                    command: "ClearFollowQueue",
                    queueKey: followQueue.key
                }, function (data) {
                    $scope.followQueue.cachedTwitterUsers = [];
                }
            );
        }



    }]);


