'use strict';


// Declare app level module which depends on filters, and services
var twitterFollowbotScheduledEventsModule = angular.module('twitter.followbotScheduledEvents', [
    'application.providers',
    'twitter.followbotScheduledEvents.controllers',
    'twitter.followbotScheduledEvents.services',
    'ngRoute',
    'ui.bootstrap'

]);

twitterFollowbotScheduledEventsModule.config(['$routeProvider',  function ($routeProvider) {

    $routeProvider.when('/twitter/followbots/:id/schedule',{templateUrl: _$app.module('twitter/followbotScheduledEvents').viewPath('followbotScheduledEvents.html'),controller: 'FollowbotsScheduleCtrl'});
    $routeProvider.when('/twitter/followbots/:followbotId/schedule/scheduledEvent/:id/queues',{templateUrl: _$app.module('twitter/followbotScheduledEvents').viewPath('queues.html'),controller: 'FollowbotScheduleEventQueuesCtrl'});

}]);
