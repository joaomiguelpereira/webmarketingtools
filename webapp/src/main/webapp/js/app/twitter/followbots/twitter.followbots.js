'use strict';


// Declare app level module which depends on filters, and services
var twitterFollowbotsModule = angular.module('twitter.followbots', [
    'application.providers',
    'twitter.followbots.controllers',
    'twitter.followbots.services',
    'ngRoute',
    'ui.bootstrap'

]);

twitterFollowbotsModule.config(['$routeProvider',  function ($routeProvider) {
    $routeProvider.when('/twitter/followbots',{templateUrl: _$app.module('twitter/followbots').viewPath('followbots.html'),controller: 'FollowbotsCtrl'});
}]);
