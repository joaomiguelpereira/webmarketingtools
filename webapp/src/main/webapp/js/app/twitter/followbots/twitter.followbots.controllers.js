var twitterFollowbotsControllers = angular.module("twitter.followbots.controllers", []);
/**
 * Applications Controller
 */
twitterFollowbotsControllers.controller('FollowbotsCtrl', ['$scope', 'FollowbotCollection', 'UserFlashes', function ($scope, FollowbotCollection, UserFlashes) {



    $scope.followbots = FollowbotCollection.query();


    $scope.edit = function (followbot) {
        $scope.followbotInstance = followbot;

    }

    $scope.newFollowbot = function () {
        //Initialize an instance
        alert("crete new");
        $scope.followbotInstance = {
            name: '',
            twitterUserRetrievalStrategy: {

                type: '',
                statusKeywordSearchOptions: {
                    keywords: '',
                    language: ''
                },
                otherUserFollowersOptions: {
                    userScreenName: ''

                },
                otherUserFriendsOptions: {
                    userScreenName: ''
                }
            },
            twitterUserScreenerOptions: {
                followingToFollowerRatio: 0,
                languageCode: 'EN',

                activityMatcherOptions: {
                    minimumLastTweetAgeInDays: 0,
                    averageTweetsPerDay: 0,
                    minimumTweets: 0

                },
                keywordMatcherOptions: {
                    keywords: '',
                    updatesToAnalyse: 100,
                    minimumKeyWordCount: 5
                },
                socialActivityMatcherOptions: {
                    minFollowingFollowerRatio: 0.5,
                    maxFollowingFollowerRatio: 1.3
                }

            }

        };
    }

    $scope.remove = function (followbot) {
        FollowbotCollection.delete({id: followbot.id}, function (data) {
            UserFlashes.success(data.message);

            $scope.followbots = $scope.followbots.filter(function (filter) {
                return followbot.id != filter.id;

            })

        })
    };

    $scope.$on('twitter.followbot.clear.followbotInstance', function (event) {
        $scope.followbotInstance = undefined;
    });
    $scope.$on('twitter.followbot.created', function (event, data) {
        $scope.followbots.push(data);
    });


}
]);



twitterFollowbotsControllers.controller('newFollowbotFormCtrl', ['$scope', 'FollowbotCollection', 'UserFlashes', function ($scope, FollowbotCollection, UserFlashes) {


    $scope.update = function () {
        FollowbotCollection.update({id: $scope.followbotInstance.id}, $scope.followbotInstance, function (data) {
            UserFlashes.success("Updated");
            $scope.$emit('twitter.followbot.clear.followbotInstance');
            $scope.$emit('twitter.followbot.updated', data);

        });
    }
    $scope.create = function () {
        FollowbotCollection.save({}, $scope.followbotInstance, function (data) {
            $scope.$emit('twitter.followbot.clear.followbotInstance');
            $scope.$emit('twitter.followbot.created', data);
            UserFlashes.success("Created");

        });
    }
    $scope.cancel = function () {
        $scope.$emit('twitter.followbot.clear.followbotInstance');

    }

}]);
