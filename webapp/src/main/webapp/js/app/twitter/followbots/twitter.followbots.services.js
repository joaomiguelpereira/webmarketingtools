'use strict';


/* Controllers */

var twitterFollowbotsServices = angular.module('twitter.followbots.services', ['ngResource']);


twitterFollowbotsServices.factory('FollowbotCollection', ['$resource', function($resource) {
    return $resource("/api/twitter/followbots/:id", {id: '@id'},
        {
            "update" : {method: 'PUT'}
        });
}]);




