//////////////////////
/// UGLY IS COPY PASTE. FIX IN THE FUTURE

var twitterUnfollowbotScheduledEventsControllers = angular.module("twitter.unfollowbotScheduledEvents.controllers", []);

twitterUnfollowbotScheduledEventsControllers.controller('UnfollowbotsScheduleCtrl', ['$scope', '$routeParams', 'UnfollowbotCollection',
    'UnfollowbotScheduledEventCollection', function ($scope, $routeParams, UnfollowbotCollection, UnfollowbotScheduledEventCollection) {


        if ($routeParams.id) {
            $scope.unfollowbot = UnfollowbotCollection.get({id: $routeParams.id}, function (data) {
                $scope.unfollowbotScheduledEvents = UnfollowbotScheduledEventCollection.query({unfollowbotId: data.id}, function (data) {
                });
            });
        }

        $scope.newUnfollowbotScheduledEventInstance = function () {

            $scope.unfollowbotScheduledEventInstance = {
                unfollowbotId: $scope.unfollowbot.id,
                name: '',
                applicationId: '',
                accountId: '',
                minFollowingToFollowerRatio: 0.6,

                trigger: {
                    days: [],
                    hourCronExpression: '0',
                    minuteCronExpression: '30'
                },
                screenerExecutionParameters : {
                    maxUsersToScreen: 10
                },
                unfollowQueueOptions: {
                    maxSize: 100,
                    dequeueBatchSize: 5,
                    minuteCronExpression: "*/5"

                }

            }
        };
        $scope.$on('twitter.unfollowbotSheduledEvent.newInstance.destroy', function (event) {
            $scope.unfollowbotScheduledEventInstance = undefined;
        });
        $scope.$on('twitter.unfollowbotSheduledEvent.deleted', function (event, unfollowbotInstance) {

            $scope.unfollowbotScheduledEvents = $scope.unfollowbotScheduledEvents.filter(function (filteredObject) {
                return filteredObject.id != unfollowbotInstance.id;
            });

        });
        $scope.$on('twitter.unfollowbotSheduledEvent.newInstance.saved', function (newInstantce) {
            //BUG
            //$scope.followbotScheduledEvents.push(newInstantce);
            //Reloading...instead

            $scope.unfollowbotScheduledEvents = UnfollowbotScheduledEventCollection.query({unfollowbotId: $scope.unfollowbot.id}, function (data) {

            });


            $scope.unfollowbotScheduledEventInstance = undefined;
        });

        $scope.$on('twitter.unfollowbotSheduledEvent.instance.updated', function (event, updatedInstance) {

            for ( var i = 0; i<$scope.unfollowbotScheduledEvents.length; i++) {
                if ( $scope.unfollowbotScheduledEvents[i].id == updatedInstance.id) {
                    $scope.unfollowbotScheduledEvents[i] = updatedInstance;
                    break;

                }
            }
            $scope.unfollowbotScheduledEventInstance = undefined;

        });
        $scope.$on('twitter.unfollowbotScheduledEvent.edit', function (event, data) {

            //BUG
            $scope.unfollowbotScheduledEventInstance = data;
        });


    }]);
twitterUnfollowbotScheduledEventsControllers.controller('newUnfollowbotScheduledEventFormCtrl', [
    '$scope',
    'ApplicationCollection',
    'AccountCollection',
    'UnfollowbotScheduledEventCollection',
    'UserFlashes',
    function ($scope, ApplicationCollection, AccountCollection, UnfollowbotScheduledEventCollection, UserFlashes) {

        $scope.applications = ApplicationCollection.query();

        $scope.DAYS = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];


        $scope.daysSelectorHelper = {
            allDaysSelected: false,

            toggleAll: function () {
                $scope.daysSelectorHelper.allDaysSelected = !$scope.daysSelectorHelper.allDaysSelected;
                console.log("Selceting All: " + $scope.daysSelectorHelper.allDaysSelected);
                if ($scope.daysSelectorHelper.allDaysSelected) {
                    angular.forEach($scope.DAYS, function (day) {
                        $scope.unfollowbotScheduledEventInstance.trigger.days.push(day);
                    });

                } else {
                    $scope.unfollowbotScheduledEventInstance.trigger.days = [];
                }
                console.dir($scope.unfollowbotScheduledEventInstance.trigger.days);

            },
            toggleDay: function (day) {

                var idx = $scope.unfollowbotScheduledEventInstance.trigger.days.indexOf(day);

                // is currently selected
                if (idx > -1) {
                    $scope.unfollowbotScheduledEventInstance.trigger.days.splice(idx, 1);
                }

                // is newly selected
                else {
                    $scope.unfollowbotScheduledEventInstance.trigger.days.push(day);
                }


            }};
        $scope.create = function () {

            UnfollowbotScheduledEventCollection.save($scope.unfollowbotScheduledEventInstance, function (data) {
                $scope.$emit('twitter.unfollowbotSheduledEvent.newInstance.saved', data);
                UserFlashes.success("Created");
            })
        };

        $scope.cancel = function () {
            $scope.$emit('twitter.unfollowbotSheduledEvent.newInstance.destroy');

        };

        $scope.update = function () {


            UnfollowbotScheduledEventCollection.update($scope.unfollowbotScheduledEventInstance, function (data) {
                $scope.$emit('twitter.unfollowbotSheduledEvent.instance.updated', data);
                UserFlashes.success("Updated");
            })


        }


        $scope.$watch("unfollowbotScheduledEventInstance.applicationId", function (data) {
            if ($scope.unfollowbotScheduledEventInstance && $scope.unfollowbotScheduledEventInstance.applicationId) {
                $scope.accounts = AccountCollection.query({applicationId: $scope.unfollowbotScheduledEventInstance.applicationId});
            }

        });

    }]);


twitterUnfollowbotScheduledEventsControllers.controller('UnfollowbotScheduledEventEntryCtrl', ['$scope',
    '$cacheFactory',
    'ApplicationCollection',
    'Account',
    'UnfollowbotScheduledEventCollection',
    'UserFlashes',
    'UnfollowbotScheduledEventActivation',
    function ($scope, $cacheFactory, ApplicationCollection, Account, UnfollowbotScheduledEventCollection, UserFlashes,UnfollowbotScheduledEventActivation) {

        $scope.unfollowbotScheduledEventEntry = angular.copy($scope.unfollowbotScheduledEvent);

        $scope.unfollowbotScheduledEventEntry.application = function () {

            var applicationId = $scope.unfollowbotScheduledEvent.applicationId;

            if (!applicationId) {
                return "N/A";
            }
            var applicationCache = $cacheFactory.get("twitter.applications");
            if (!applicationCache) {
                applicationCache = $cacheFactory("twitter.applications");
            }

            var chachedApplication = applicationCache.get(applicationId);

            if (!chachedApplication) {
                ApplicationCollection.get({id: applicationId}, function (data) {
                    //Get cache created elsewhere
                    if (!applicationCache.get(applicationId)) {
                        applicationCache.put(applicationId, data);
                    }

                    $scope.unfollowbotScheduledEventEntry.application = data.name;

                });
                return "Loading....";
            }
            return chachedApplication.name;
        }();

        $scope.unfollowbotScheduledEventEntry.account = function () {

            var accountId = $scope.unfollowbotScheduledEvent.accountId;
            if (!accountId) {
                return "N/A";
            }
            var accountsCache = $cacheFactory.get("twitter.accounts");
            if (!accountsCache) {
                accountsCache = $cacheFactory("twitter.accounts");
            }

            var cachedAccount = accountsCache.get(accountId);

            if (!cachedAccount) {
                Account.get({id: accountId}, function (data) {
                    //Get cache created elsewhere
                    if (!accountsCache.get(accountId)) {
                        accountsCache.put(accountId, data);
                    }
                    //accountsCache.put(accountId,data);
                    $scope.unfollowbotScheduledEventEntry.account = data.name;
                    //console.log(accountsCache.size);
                });
                return "Loading....";
            }
            return cachedAccount.name;
        }();
        ;
        $scope.unfollowbotScheduledEventEntry.trigger = "Trigger";
        $scope.unfollowbotScheduledEventEntry.queues = function () {
            return "Test";
        }();


        $scope.edit = function () {
            $scope.$emit("twitter.unfollowbotScheduledEvent.edit", $scope.unfollowbotScheduledEvent);
        }


        $scope.remove = function () {


            UnfollowbotScheduledEventCollection.delete({unfollowbotId: $scope.unfollowbot.id, id: $scope.unfollowbotScheduledEvent.id}, function () {
                UserFlashes.success("Removed");
                $scope.$emit('twitter.unfollowbotSheduledEvent.deleted', $scope.unfollowbotScheduledEvent);
            });
        };
        $scope.activate = function() {
            UnfollowbotScheduledEventActivation.save({unfollowbotId: $scope.unfollowbot.id, id: $scope.unfollowbotScheduledEvent.id}, function(data) {

                $scope.$emit('twitter.unfollowbotSheduledEvent.instance.updated', data);




            })
        };
        $scope.deactivate = function() {
            UnfollowbotScheduledEventActivation.delete({unfollowbotId: $scope.unfollowbot.id, id: $scope.unfollowbotScheduledEvent.id}, function(data) {
                $scope.$emit('twitter.unfollowbotSheduledEvent.instance.updated', data);
            });
        }

    }]);


