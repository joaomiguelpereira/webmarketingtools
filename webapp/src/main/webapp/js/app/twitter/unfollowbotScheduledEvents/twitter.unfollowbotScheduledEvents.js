'use strict';


// Declare app level module which depends on filters, and services
var twitterUnfollowbotScheduledEventsModule = angular.module('twitter.unfollowbotScheduledEvents', [
    'application.providers',
    'twitter.unfollowbotScheduledEvents.controllers',
    'twitter.unfollowbotScheduledEvents.services',
    'ngRoute',
    'ui.bootstrap'

]);

twitterUnfollowbotScheduledEventsModule.config(['$routeProvider',  function ($routeProvider) {

    $routeProvider.when('/twitter/unfollowbots/:id/schedule',{templateUrl: _$app.module('twitter/unfollowbotScheduledEvents').viewPath('unfollowbotScheduledEvents.html'),controller: 'UnfollowbotsScheduleCtrl'});

}]);
