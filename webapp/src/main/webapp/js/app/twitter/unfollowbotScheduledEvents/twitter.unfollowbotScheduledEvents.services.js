'use strict';


/* Controllers */

var twitterUnfollowbotScheduledEventsServices = angular.module('twitter.unfollowbotScheduledEvents.services', ['ngResource']);


twitterUnfollowbotScheduledEventsServices.factory('UnfollowbotScheduledEventCollection', ['$resource', function($resource) {
    return $resource("/api/twitter/unfollowbots/:unfollowbotId/unfollowbotScheduledEvents/:id", {unfollowbotId:'@unfollowbotId', id: '@id'},
        {
            "update" : {method: 'PUT'}
        });
}]);

twitterUnfollowbotScheduledEventsServices.factory('UnfollowbotScheduledEventActivation', ['$resource', function($resource) {
    return $resource("/api/twitter/unfollowbots/:unfollowbotId/unfollowbotScheduledEvents/:id/activation", {unfollowbotId:'@unfollowbotId', id: '@id'},
        {

        });
}]);








