'use strict';


/* Controllers */

var twitterApplicationServices = angular.module('twitter.application.services', ['ngResource']);

twitterApplicationServices.factory('TwitterApplication', ['$resource', function($resource) {
    return $resource("/api/accounts/:accountId/twitter/application");

}]);




