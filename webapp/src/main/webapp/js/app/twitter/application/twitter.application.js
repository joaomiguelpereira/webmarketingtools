'use strict';


// Declare app level module which depends on filters, and services
var twitterApplicationModule = angular.module('twitter.application', [
    'application.providers',
    'twitter.application.controllers',
    'twitter.application.services',
    'ngRoute',
    'ui.bootstrap'

]);

twitterApplicationModule.config(['$routeProvider',  function ($routeProvider) {
    $routeProvider.when('/accounts/:accountId/twitter/application',{templateUrl: _$app.module('twitter/application').viewPath('application.html'),controller: 'TwitterApplicationCtrl'});

}]);
