var twitterApplicationControllers = angular.module("twitter.application.controllers", [
    'accounts.services']);
/**
 * Applications Controller
 */
twitterApplicationControllers.controller('TwitterApplicationCtrl', [
    '$scope',
    'TwitterApplication',
    'AccountCollection',
    '$routeParams',
    'growl',

    function ($scope,TwitterApplication, AccountCollection, $routeParams, growl) {

        if (!$routeParams.accountId ) {
            growl.addErrorMessage("No Account selected in URL!")
        } else {
            $scope.twitterApplication = TwitterApplication.get({accountId : $routeParams.accountId});
            $scope.account = AccountCollection.get({id:$routeParams.accountId});

            $scope.save = function() {
                TwitterApplication.save({accountId: $routeParams.accountId}, $scope.twitterApplication, function() {
                    growl.addSuccessMessage("Application Configuration saved!")
                });
            }
        }




}]);



