/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 1/21/14
 * Time: 9:35 PM
 * To change this template use File | Settings | File Templates.
 */


var tradingInstrumentsControllers = angular.module('trading.instruments.controllers',
    ['trading.instruments.services',
        'accounts.services'
    ]);


/**
 * Accounts Controller
 */
tradingInstrumentsControllers.controller("TradingInstrumentsCtrl",
    [
        'Instruments',
        'AccountCollection',
        '$scope',
        '$routeParams',
        'growl',
        '$window',
        '$location',

        function (Instruments, AccountCollection, $scope, $routeParams, growl, $window, $location) {

            if (!$routeParams.id && !$routeParams.accountId) {
                growl.addErrorMessage("No Account selected!!");
            } else {
                var accountId = $routeParams.id || $routeParams.accountId;
                $scope.instruments = Instruments.query({accountId: accountId});
                $scope.account = AccountCollection.get({id: accountId});


            }


            $scope.delete = function (symbol) {

                if (!$routeParams.id && !$routeParams.accountId) {
                    growl.addErrorMessage("No Account selected!!");
                } else {
                    var accountId = $routeParams.id || $routeParams.accountId;
                    if ($window.confirm("Are you sure you want to remove the symbol: " + symbol)) {
                        Instruments.delete({accountId: accountId, symbol: symbol}, function () {
                            $scope.instruments = Instruments.query({accountId: accountId});
                        });

                    }
                    $scope.instruments = Instruments.query({accountId: accountId});
                }

            };

            $scope.save = function () {
                if (!$routeParams.id && !$routeParams.accountId) {
                    growl.addErrorMessage("No Account selected!!");
                } else {
                    var accountId = $routeParams.id || $routeParams.accountId;
                    $scope.instrument.accountId = accountId;
                    Instruments.save($scope.instrument, function (done) {
                        $location.path("/accounts/" + accountId + "/instruments");
                    })

                    $scope.instruments = Instruments.query({accountId: accountId});

                }


            }

        }]);


tradingInstrumentsControllers.controller("TradingInstrumentViewCtrl",
    [
        'Instruments',
        '$scope',
        '$routeParams',
        'growl',
        '$window',
        '$location',

        function (Instruments, $scope, $routeParams, growl, $window, $location) {

            if ((!$routeParams.id && !$routeParams.accountId) || !$routeParams.symbol) {
                growl.addErrorMessage("No Account selected or symbol!!");
            } else {
                var accountId = $routeParams.id || $routeParams.accountId;
                $scope.instrument = Instruments.get({accountId:accountId, symbol: $routeParams.symbol});

            }


            $scope.save = function () {
                if ((!$routeParams.id && !$routeParams.accountId)) {
                    growl.addErrorMessage("No Account selected!!");

                } else {
                    var accountId = $routeParams.id || $routeParams.accountId;
                    Instruments.save({accountId: accountId, symbol: $scope.instrument.symbol}, $scope.instrument);
                    $location.path("/accounts/"+accountId+"/instruments");
                }
                }

            $scope.cancel = function () {
                if ((!$routeParams.id && !$routeParams.accountId) ) {
                    growl.addErrorMessage("No Account selected!!");
                } else {

                    var accountId = $routeParams.id || $routeParams.accountId;
                    $location.path("/accounts/:accountId/instruments");
                }



            }

        }]);




tradingInstrumentsControllers.controller("pipPriceCalculatorCtrl",
    [
        '$scope',

        function ($scope) {

            $scope.calculator = {
                spread:0,
                pips: 0,
                pl :0,
                price: 0,
                lotSize: 0
            };

            $scope.updateResult = function() {
                if ( $scope.calculator.pips>0 && $scope.calculator.pl>0 && $scope.calculator.lotSize>0 ) {
                    var points = new Number($scope.calculator.spread)+ new Number($scope.calculator.pips);
                    $scope.calculator.price = (new Number($scope.calculator.pl)  /points) /$scope.calculator.lotSize;

                }
            }
            $scope.$watch("calculator.spread", function(newValue, oldValue) {
                $scope.updateResult();

            });

            $scope.$watch("calculator.pips", function(newValue, oldValue) {
                $scope.updateResult();

            });
            $scope.$watch("calculator.lotSize", function(newValue, oldValue) {
                $scope.updateResult();

            });
            $scope.$watch("calculator.pl", function(newValue, oldValue) {
                $scope.updateResult();

            });


        }]);

