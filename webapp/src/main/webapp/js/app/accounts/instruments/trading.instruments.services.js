'use strict';

/* Controllers */

var tradingInstrumentsServices = angular.module('trading.instruments.services', ['ngResource']);


tradingInstrumentsServices.factory('Instruments', ['$resource', function($resource) {
    return $resource("/api/trading/account/:accountId/instruments/:symbol",{accountId: '@accountId', symbol: '@symbol'});

}]);


