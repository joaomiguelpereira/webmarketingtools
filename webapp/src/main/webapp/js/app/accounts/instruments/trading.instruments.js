'use strict';


// Declare app level module which depends on filters, and services
var tradingInstrumentsModule = angular.module('trading.instruments', [
    'application.providers',
    'trading.instruments.services',
    'trading.instruments.controllers',
    'accounts.services',
    'ngRoute',
    'ui.bootstrap'

]);

tradingInstrumentsModule.config(['$routeProvider',  function ($routeProvider) {

    $routeProvider.when('/accounts/:accountId/instruments', {templateUrl:_$app.module('accounts/instruments').viewPath('instruments.html'), controller: 'TradingInstrumentsCtrl'});

    $routeProvider.when('/accounts/:accountId/instruments/new', {templateUrl:_$app.module('accounts/instruments').viewPath('instrument.html'), controller: 'TradingInstrumentsCtrl'});
    $routeProvider.when('/accounts/:accountId/instruments/:symbol', {templateUrl:_$app.module('accounts/instruments').viewPath('instrument.html'), controller: 'TradingInstrumentViewCtrl'});


}]);
