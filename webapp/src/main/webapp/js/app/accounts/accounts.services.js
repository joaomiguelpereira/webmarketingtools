'use strict';

/* Controllers */

var accountsServices = angular.module('accounts.services', ['ngResource']);


accountsServices.factory('AccountCollection', ['$resource', function($resource) {
    return $resource("/api/accounts/:id",{});
}]);

accountsServices.factory('RiskParameters', ['$resource', function($resource) {
    return $resource("/api/trading/account/:accountId/risk",{accountId: '@accountId'});

}]);

accountsServices.factory('AccountPlan', ['$resource', function($resource) {
    return $resource("/api/trading/account/:accountId/plan",{accountId: '@accountId'});

}]);

accountsServices.factory('Movements', ['$resource', function($resource) {
    return $resource("/api/trading/account/:accountId/movements/:id",{accountId: '@accountId'});

}]);


accountsServices.factory('BalanceSummary', ['$resource', function($resource) {
    return $resource("/api/trading/account/:accountId/balance",{accountId: '@accountId'});

}]);

accountsServices.factory('AccountSummary', ['$resource', function($resource) {
    return $resource("/api/trading/account/:accountId/summary",{accountId: '@accountId'});

}]);

accountsServices.factory('ClosedTradesSummary', ['$resource', function($resource) {
    return $resource("/api/trading/account/:accountId/trades/closeSummary",{});
}]);










