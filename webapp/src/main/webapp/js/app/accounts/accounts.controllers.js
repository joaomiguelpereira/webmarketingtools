/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 1/21/14
 * Time: 9:35 PM
 * To change this template use File | Settings | File Templates.
 */


var accountsControllers = angular.module('accounts.controllers', ['accounts.services']);

/**
 * Accounts Controller
 */
accountsControllers.controller("AccountsCtrl",
    ['AccountCollection','AccountSummary',
        '$scope', '$location', '$window',

        function (AccountCollection, AccountSummary, $scope, $location, $window) {

            $scope.accounts = AccountCollection.query({},function(accounts) {
               angular.forEach(accounts, function(account) {
                   account.accountSummary = AccountSummary.get({accountId: account.id}, function(summary) {

                   });
               });

            });

            $scope.viewAccount = function (account) {
                $location.path('/accounts/' + account.id)
            }
            $scope.remove = function (accountId) {
                if ($window.confirm("Are you sure??")) {
                    AccountCollection.delete({id: accountId}, function () {
                        $scope.accounts = AccountCollection.query();
                    });

                }


            };


        }]);



accountsControllers.controller("AccountsViewCtrl", ['ClosedTradesSummary', 'BalanceSummary', 'AccountPlan', 'Trades', 'RiskParameters', 'AccountCollection', '$scope', '$location', 'growl', '$routeParams', '$window',

   function (ClosedTradesSummary, BalanceSummary, AccountPlan, Trades, RiskParameters, AccountCollection, $scope, $location, growl, $routeParams, $window) {


        $scope.openTradePageRequestInfo = {
            type: "OPEN",
            page : 0,
            pageSize: 10,
            direction: "DESC",
            fieldName: "openTime"
        };

        $scope.plannedTradePageRequestInfo = {
            type: "PLANNED",
            page : 0,
            pageSize: 10,
            direction: "DESC",
            fieldName: "openTime"
        };

        $scope.closedTradePageRequestInfo = {
            type: "CLOSED",
            page : 0,
            pageSize: 10,
            direction: "DESC",
            fieldName: "openTime"
        };

        if (!$routeParams.id) {
            growl.addErrorMessage("Need account!")
            $location.path("/accounts");
        }

        $scope.account = AccountCollection.get({id: $routeParams.id});
        $scope.closedSummary = ClosedTradesSummary.get({accountId: $routeParams.id});
        $scope.plan = AccountPlan.get({accountId: $routeParams.id});

        $scope.updateTrades = function () {

            $scope.openTrades = Trades.query( $scope.openTradePageRequestInfo, {accountId: $routeParams.id}, function (trades) {
                angular.forEach(trades, function (trade) {

                    $scope.accountOpenRisk += trade.riskAmmount / $scope.balanceSummary.balance;
                })
            });
            $scope.plannedTrades = Trades.query($scope.plannedTradePageRequestInfo, {accountId: $routeParams.id},function (trades) {
                angular.forEach(trades, function (trade) {

                    $scope.accountPlannedRisk += trade.riskAmmount / $scope.balanceSummary.balance;
                })
            });
            $scope.closedTrades = Trades.query($scope.closedTradePageRequestInfo, {accountId: $routeParams.id}, function (trades) {
            });
            $scope.closedSummary = ClosedTradesSummary.get({accountId: $routeParams.id});

        }
        $scope.balanceSummary = BalanceSummary.get({accountId: $routeParams.id}, function (bs) {

            $scope.accountOpenRisk = 0.0;
            $scope.accountPlannedRisk = 0.0;
            $scope.updateTrades();


        });


        $scope.riskParameters = RiskParameters.get({accountId: $routeParams.id});
        $scope.accountOpenRisk = 0;
        $scope.deleteTrade = function (tradeId) {

            if ($window.confirm("Are you sure you want to remove the Trade: " + tradeId)) {
                Trades.delete({accountId: $routeParams.id, id: tradeId}, function () {
                    $scope.trades = Trades.query({accountId: $routeParams.id}, function () {

                        $scope.updateTrades();
                    });
                });
            }
        };


       $scope.nextPage = function() {
           $scope.closedTradePageRequestInfo.page +=1;
           $scope.closedTrades = Trades.query($scope.closedTradePageRequestInfo, {accountId: $scope.account.id}, function (trades) {
           });
       }
       $scope.previousPage = function() {

           $scope.closedTradePageRequestInfo.page -=1;
           $scope.closedTrades = Trades.query($scope.closedTradePageRequestInfo, {accountId: $scope.account.id}, function (trades) {
           });
       }
       $scope.$watch("closedTradePageRequestInfo.pageSize", function(newValue, oldValue) {

           if (newValue!=oldValue) {
               alert("updating");
           }

           /*
           if ( newValue && newValue != $scope.closedTradePageRequestInfo.pageSize ) {

               $scope.closedTradePageRequestInfo.pageSize = newValue;
               $scope.closedTrades = Trades.query($scope.closedTradePageRequestInfo, {accountId: $scope.account.id}, function (trades) {
               });
               alert("Seeting new val:"+newValue);

               //$scope.updateTrades();
           } */


       });


    }]);


accountsControllers.controller("AccountsNewCtrl", ['AccountCollection', '$scope', '$location', 'growl', '$routeParams', function (AccountCollection, $scope, $location, growl, $routeParams) {
    if ($routeParams.id) {
        $scope.account = AccountCollection.get({id: $routeParams.id});

    } else {
        $scope.account = {
            name: "",
            description: ""
        }


    }
    $scope.create = function () {
        if (!$scope.account.name) {
            growl.addErrorMessage("Name is invalid");
        } else {
            AccountCollection.save($scope.account, function (done) {
                $location.path("/accounts");
            })
        }

    }
    $scope.cancel = function () {
        $location.path("/accounts");
    }

}]);

accountsControllers.controller("AccountMenuCtrl", ['AccountCollection', '$scope', '$location', '$routeParams', 'growl', function (AccountCollection, $scope, $location, $routeParams, growl) {

    if (!$routeParams.id && !$routeParams.accountId) {
        growl.addErrorMessage("No Account selected!!");
    } else {
        var accountId = $routeParams.id || $routeParams.accountId;
        $scope.account = AccountCollection.get({id: accountId});
    }


}]);

accountsControllers.controller("AccountsRiskCtrl", ['RiskParameters', 'AccountCollection', '$scope', '$location', '$routeParams', 'growl', function (RiskParameters, AccountCollection, $scope, $location, $routeParams, growl) {

    if (!$routeParams.id && !$routeParams.accountId) {
        growl.addErrorMessage("No Account selected!!");
    } else {
        var accountId = $routeParams.id || $routeParams.accountId;

        $scope.riskParameters = RiskParameters.get({accountId: accountId});

    }
    $scope.cancel = function () {
        if ($routeParams.id || $routeParams.accountId) {
            var accountId = $routeParams.id || $routeParams.accountId;
            $location.path("/accounts/" + accountId);
        } else {

        }
    };

    $scope.save = function () {
        if (!$routeParams.id && !$routeParams.accountId) {
            growl.addErrorMessage("No Account selected!!");
        } else {
            var accountId = $routeParams.id || $routeParams.accountId;
            RiskParameters.save($scope.riskParameters, function (newRiskParameters) {
                //alert(newRiskParameters);

                //$scope.riskParameters = newRiskParameters;
                growl.addSuccessMessage("Risk Parameters Saved");

            });
            $location.path("/accounts/" + accountId);

        }


    }


}]);

accountsControllers.controller("AccountsPlanCtrl", ['AccountPlan', 'AccountCollection', '$scope', '$location', '$routeParams', 'growl', function (AccountPlan, AccountCollection, $scope, $location, $routeParams, growl) {

    if (!$routeParams.id && !$routeParams.accountId) {
        growl.addErrorMessage("No Account selected!!");
    } else {
        var accountId = $routeParams.id || $routeParams.accountId;
        $scope.account = AccountCollection.get({id: accountId});
        $scope.plan = AccountPlan.get({accountId: accountId});

    }
    $scope.cancel = function () {
        if ($routeParams.id || $routeParams.accountId) {
            var accountId = $routeParams.id || $routeParams.accountId;
            $location.path("/accounts/" + accountId);
        } else {

        }
    };

    $scope.save = function () {
        if (!$routeParams.id && !$routeParams.accountId) {
            growl.addErrorMessage("No Account selected!!");
        } else {
            var accountId = $routeParams.id || $routeParams.accountId;
            AccountPlan.save($scope.plan, function (data) {
                growl.addSuccessMessage("Account Plan Saved");
                $location.path("/accounts/" + accountId);

            });


        }


    }


}]);


accountsControllers.controller("MovementsCtrl", ['BalanceSummary', 'Movements',
    'AccountPlan',
    'AccountCollection',
    '$scope',
    '$location',
    '$routeParams',
    'growl',
    '$window',
    function (BalanceSummary, Movements, AccountPlan, AccountCollection, $scope, $location, $routeParams, growl, $window) {

        if (!$routeParams.id && !$routeParams.accountId) {
            growl.addErrorMessage("No Account selected!!");
        } else {
            var accountId = $routeParams.id || $routeParams.accountId;
            $scope.movements = Movements.query({accountId: accountId});
            $scope.account = AccountCollection.get({id: accountId});
            $scope.movement = {
                accountId: accountId
            }
            $scope.balanceSummary = BalanceSummary.get({accountId: accountId});

            //$scope.plan = AccountPlan.get({accountId: accountId});

        }


        $scope.remove = function (movementId) {
            if (!$routeParams.id && !$routeParams.accountId) {
                growl.addErrorMessage("No Account selected!!");
            } else {
                var accountId = $routeParams.id || $routeParams.accountId;

                if ($window.confirm("Are you sure you want to remove movement: " + movementId)) {

                    Movements.remove({accountId: accountId, id: movementId}, function () {
                        $scope.movements = Movements.query({accountId: accountId}, function (data) {
                            $scope.balanceSummary = BalanceSummary.get({accountId: accountId});

                        });

                    });

                }
            }

        };
        $scope.save = function () {
            if (!$routeParams.id && !$routeParams.accountId) {
                growl.addErrorMessage("No Account selected!!");
            } else {
                var accountId = $routeParams.id || $routeParams.accountId;


                Movements.save($scope.movement, function (data) {
                    $scope.movements = Movements.query({accountId: accountId});
                    if (data.id) {
                        growl.addSuccessMessage("Added " + data.ammount);
                        $scope.balanceSummary = BalanceSummary.get({accountId: accountId});
                    }
                    else {
                        growl.addErrorMessage("Something went wront!");
                    }

                });
                $scope.movement = {
                    accountId: accountId
                }
                //$location.path("/accounts/" + accountId);

            }


        }


    }]);



