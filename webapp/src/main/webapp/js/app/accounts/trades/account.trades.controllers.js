/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 1/21/14
 * Time: 9:35 PM
 * To change this template use File | Settings | File Templates.
 */


var tradesControllers = angular.module('account.trades.controllers',
    ['account.trades.services',
        'accounts.services', 'angularFileUpload', 'ngClipboard'
    ]);

/**
 * Accounts Controller
 */
tradesControllers.controller("TradesCtrl",
    [
        'TradeScreenshots',
        'AccountPlan',
        'RiskParameters',
        'Trades',
        'BalanceSummary',
        'Instruments',
        'AccountCollection',
        '$scope',
        '$routeParams',
        'growl',
        '$window',
        '$location',
        '$upload',

        function (TradeScreenshots, AccountPlan, RiskParameters, Trades, BalanceSummary, Instruments, AccountCollection, $scope, $routeParams, growl, $window, $location, $upload) {


            if (!$routeParams.accountId) {
                growl.addErrorMessage("No Account selected!!");
                return;
            }
            $scope.tradedPageInfo = {
                type: "ALL",
                page : 0,
                pageSize: 10,
                direction: "DESC",
                fieldName: "openTime"
            };
            var accountId = $routeParams.accountId;
            $scope.instruments = Instruments.query({accountId: accountId});
            $scope.account = AccountCollection.get({id: accountId});
            $scope.trades = Trades.query($scope.tradedPageInfo, {accountId: accountId}, function () {
            });
            $scope.riskParameters = RiskParameters.get({accountId: accountId});
            $scope.accountPlan = AccountPlan.get({accountId: accountId});
            $scope.balanceSummary = BalanceSummary.get({accountId: accountId});


            //Defaults
            $scope.instrumentPipSize = 0.00001;
            $scope.pipStringLength = 5;
            $scope.instrumentPipPrice = 0;
            $scope.instrumentSymbol = "EURUSD";

            $scope.loadingFinished = false;

            if ($routeParams.id) {

                $scope.trade = Trades.get({accountId: accountId, id: $routeParams.id}, function (data) {

                    if ($scope.trade.status == "CLOSED") {
                        $scope.instrumentPipPrice = $scope.trade.instrumentAtClose.pipPrice;
                        $scope.instrumentPipSize = $scope.trade.instrumentAtClose.pipSize;
                        $scope.pipStringLength = ("" + $scope.trade.instrumentAtClose.pipSize).length - 2;
                        $scope.instrumentSymbol = $scope.trade.instrumentAtClose.symbol;
                        $scope.loadingFinished = true;
                        console.dir($scope.trade);
                        $scope.update();
                    } else {
                        Instruments.get({accountId: accountId, symbol: data.symbol}, function (instrument) {
                            $scope.instrumentPipPrice = instrument.pipPrice;
                            $scope.instrumentPipSize = instrument.pipSize;
                            $scope.pipStringLength = ("" + instrument.pipSize).length - 2;


                            $scope.instrumentSymbol = instrument.symbol;
                            $scope.loadingFinished = true;
                            $scope.update();
                        });
                    }

                });

            } else {
                $scope.loadingFinished = true;
                $scope.trade = {
                    slackEntry: 10,
                    slackExit: 5,
                    entryPrice: 0,
                    slPrice: 0,
                    risk: 0,
                    orders: [],
                    rmultiple: 0,
                    riskAmmount: 0,
                    status: "PLANNED",
                    openTime: "",
                    closeTime: "",
                    openRisk: 0,
                    closeReward: 0.0,
                    spread: 0.0,
                    comission: 0.0,
                    swap: 0.0,
                    profitLoss: 0.0,
                    rollover: 0.0,
                    duration: 0,
                    maximumAdverseExcursion: 0,
                    log: "",
                    instrumentAtClose: null,
                    transactionCostCurrency: 0,
                    transactionCostPips: 0,
                    afterCostsRmultiple: 0
                };

            }

            //Sreenshots
            $scope.deleteScreenshot = function (screenshotId) {

                if ($window.confirm("Are you sure you want to remove the screenshot: " + screenshotId)) {
                    TradeScreenshots.delete({accountId: accountId, tradeId: $scope.trade.id, id: screenshotId}, function () {
                        var index;
                        for (index = 0; index < $scope.trade.screenshots.length; index++) {
                            if ($scope.trade.screenshots[index].id == screenshotId) {
                                break;
                            }
                        }
                        $scope.trade.screenshots.splice(index, 1);
                    });
                }
            };

            $scope.onFileSelect = function ($files) {
                //$files: an array of files selected, each file has name, size, and type.
                for (var i = 0; i < $files.length; i++) {
                    var file = $files[i];
                    $scope.upload = $upload.upload({
                        url: '/api/trading/account/' + $scope.account.id + '/trades/' + $scope.trade.id + '/screenshots/upload',
                        method: 'POST',
                        //headers: {'header-key': 'header-value'},
                        withCredentials: true,
                        data: {account: $scope.account,
                            trade: $scope.trade
                        },
                        file: file // or list of files ($files) for html5 only
                        //fileName: 'doc.jpg' or ['1.jpg', '2.jpg', ...] // to modify the name of the file(s)
                        // customize file formData name ('Content-Disposition'), server side file variable name.
                        //fileFormDataName: myFile, //or a list of names for multiple files (html5). Default is 'file'
                        // customize how data is added to formData. See #40#issuecomment-28612000 for sample code
                        //formDataAppender: function(formData, key, val){}
                    }).progress(function (evt) {
                            console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));
                        }).success(function (data, status, headers, config) {
                            $scope.trade.screenshots.push(data);
                            // file is uploaded successfully

                        });
                }
            }

            $scope.$watch('trade.symbol', function (newValue, oldValue) {
                if ($scope.trade.status == 'CLOSED') {
                    return;
                }
                if (newValue) {
                    Instruments.get({accountId: accountId, symbol: newValue}, function (instrument) {
                        $scope.instrumentPipPrice = instrument.pipPrice;
                        $scope.instrumentPipSize = instrument.pipSize;
                        $scope.pipStringLength = ("" + instrument.pipSize).length - 2;
                        //  $scope.pipStringLength = 1;
                        $scope.instrumentSymbol = instrument.symbol;
                    });
                }
            });


            $scope.$watch('trade.slackEntry', function (newValue, oldValue) {
                $scope.update();

            });
            $scope.$watch('trade.type', function (newValue, oldValue) {
                $scope.update();

            });
            $scope.$watch('trade.slackExit', function (newValue, oldValue) {
                $scope.update();

            });
            $scope.$watch('trade.type', function (newValue, oldValue) {
                $scope.update();

            });

            $scope.$watch('trade.previousPeriodHigh', function (newValue, oldValue) {

                $scope.update();

            });
            $scope.$watch('trade.previousPeriodLow', function (newValue, oldValue) {

                $scope.update();

            });

            $scope.$watch('trade.entryPrice', function (newValue, oldValue) {
                $scope.$broadcast("tradeUpdated", $scope.trade)

            });
            $scope.$watch('trade.slPrice', function (newValue, oldValue) {
                $scope.$broadcast("tradeUpdated", $scope.trade)

            });

            $scope.$watch('trade.openTime', function (newValue, oldValue) {
                angular.forEach($scope.trade.orders, function (order) {
                    order.openTime = newValue;
                });

            });
            $scope.$watch('trade.closeTime', function (newValue, oldValue) {
                angular.forEach($scope.trade.orders, function (order) {
                    if (!order.closeTime) {
                        order.closeTime = newValue;
                    }
                });

            });


            $scope.updateSummary = function () {
                var rmultiple = 0.0;
                var orderCount = $scope.trade.orders.length;

                var risk = 0.0;
                var riskAmmount = 0.0;
                var closeReward = new Number(0.0);
                var spread = 0.0;
                var comission = 0.0;
                var swap = 0.0;
                var profitLoss = 0.0;
                var rollover = 0.0;
                var maximmumAdverseExcursion = 0.0;
                var transactionCostPips = 0.0;
                var transactionCostCurrency = 0.0;
                var afterCostsRmultiple = 0.0;


                var getAfterCostsRmultiple = function (order) {

                    //Reafactor this

                    var risk_;
                    var reward_;
                    if (order.openRisk > 0) {

                        risk_ = order.openRisk;
                    } else {
                        risk_ = order.risk;
                    }

                    if (order.closePrice > 0) {

                        reward_ = order.closeReward;
                    } else {
                        reward_ = order.expectedReward;
                    }

                    var adjustedReward_ = new Number(reward_) + new Number(order.transactionCostPips);
                    rmultiple_ = (adjustedReward_ / risk_);

                    return rmultiple_;

                }
                var getRmultiple = function (order) {

                    //Reafactor this

                    var risk__;
                    var reward__;
                    if (order.openRisk > 0) {

                        risk__ = order.openRisk;
                    } else {
                        risk__ = order.risk;
                    }

                    if (order.closePrice > 0) {

                        reward__ = order.closeReward;
                    } else {
                        reward__ = order.expectedReward;
                    }

                    rmultiple__ = (reward__ / risk__);
                    return rmultiple__;

                }
                angular.forEach($scope.trade.orders, function (order) {
                    //var r = (order.expectedReward / order.risk) / orderCount;
                    rmultiple += (getRmultiple(order) / orderCount);
                    afterCostsRmultiple += (getAfterCostsRmultiple(order) / orderCount);


                    transactionCostPips += new Number(order.transactionCostPips);
                    transactionCostCurrency += new Number(order.transactionCostCurrency);

                    riskAmmount += new Number(order.riskAmmount);
                    risk += new Number(order.risk);

                    if (order.closeReward != 0) {
                        closeReward += new Number(order.closeReward);
                    }
                    spread += new Number(order.spread);
                    swap += new Number(order.swap);
                    comission += new Number(order.comission);
                    profitLoss += new Number(order.profitLoss);
                    rollover += new Number(order.rollover);
                    if (order.maximumAdverseExcursion != 0) {
                        maximmumAdverseExcursion += order.maximumAdverseExcursion / orderCount;
                    }
                });

                $scope.trade.maximumAdverseExcursion = maximmumAdverseExcursion;
                $scope.trade.rollover = rollover;
                $scope.trade.spread = spread;
                $scope.trade.swap = swap;
                $scope.trade.comission = comission;
                if (profitLoss) {
                    $scope.trade.profitLoss = profitLoss;
                } else {
                    $scope.trade.profitLoss = 0;
                }
                $scope.trade.closeReward = closeReward;
                $scope.trade.risk = risk;
                $scope.trade.rmultiple = rmultiple.toFixed(2);
                if (riskAmmount) {
                    $scope.trade.riskAmmount = riskAmmount;
                }
                $scope.trade.transactionCostPips = transactionCostPips;
                $scope.trade.transactionCostCurrency = transactionCostCurrency;
                $scope.trade.afterCostsRmultiple = afterCostsRmultiple.toFixed(2);
                ;
                $scope.alerts = {
                    risk: false
                }
                /*if (accountAtRisk > $scope.riskParameters.allowedRiskPerTrade) {
                 $scope.alerts = {
                 risk: true
                 }
                 } */


            }
            $scope.$on('ordersUpdated', function (order) {
                $scope.updateSummary();

            });
            $scope.update = function () {
                if ($scope.instrumentSymbol && $scope.trade.type && $scope.loadingFinished) {


                    if ($scope.trade.slackEntry && $scope.trade.slackExit && $scope.trade.previousPeriodLow && $scope.trade.previousPeriodHigh) {


                        var slackEntry = new Number($scope.trade.slackEntry) * $scope.instrumentPipSize;
                        var slackExit = new Number($scope.trade.slackExit) * $scope.instrumentPipSize;

                        var high = $scope.trade.previousPeriodHigh;
                        var low = $scope.trade.previousPeriodLow;

                        if ($scope.trade.type == 'SELL') {

                            try {
                                $scope.trade.entryPrice = ((new Number(low) - slackEntry).toFixed($scope.pipStringLength));
                                $scope.trade.slPrice = ((new Number(high) + slackExit).toFixed($scope.pipStringLength));
                            } catch (ex) {
                                console.log("Not able to set entry price")
                            }

                            var distance = new Number($scope.trade.slPrice) - $scope.trade.entryPrice;
                            var risk = distance / $scope.instrumentPipSize;
                            $scope.trade.risk = Math.abs(risk.toFixed(0));


                        } else {

                            try {
                                $scope.trade.slPrice = ((new Number(low) - slackExit).toFixed($scope.pipStringLength));
                                $scope.trade.entryPrice = ((new Number(high) + slackEntry).toFixed($scope.pipStringLength));

                            } catch (ex) {
                                console.log("Not able to set sl price")
                            }
                            var distance = new Number($scope.trade.entryPrice) - $scope.trade.slPrice;

                            var risk = distance / $scope.instrumentPipSize
                            $scope.trade.risk = Math.abs(risk);

                        }
                    }
                }

            }


            $scope.delete = function (tradeId) {

                if ($window.confirm("Are you sure you want to remove the Trade: " + tradeId)) {
                    Trades.delete({accountId: accountId, id: tradeId}, function () {
                        $scope.trades = Trades.query($scope.tradedPageInfo, {accountId: accountId}, function () {
                        });
                    });
                }
            };


            $scope.openTrade = function () {
                $scope.trade.status = 'OPEN';
                $scope.save();

            };
            $scope.closeTrade = function () {
                $scope.trade.status = 'CLOSED';
                $scope.save();
            };
            $scope.cancelTrade = function () {
                $scope.trade.status = 'NOT_TRIGGERED';
                $scope.save();
            };


            $scope.save = function () {
                if (!$routeParams.id && !$routeParams.accountId) {
                    growl.addErrorMessage("No Account selected!!");
                } else {
                    var accountId = $routeParams.accountId;
                    $scope.trade.accountId = accountId;
                    Trades.save($scope.trade, function (trade) {
                        $scope.trade = trade;
                        growl.addSuccessMessage("Trade Saved!");
                        //$location.path("/accounts/" + accountId + "/trades/"+trade.id);
                    })

                    $scope.trades = Trades.query($scope.tradedPageInfo,{accountId: accountId});
                }
            };

            $scope.deleteOrder = function (number) {
                var orders = $scope.trade.orders;
                for (var i = 0; i < orders.length; i++) {
                    var theOrder = orders[i];
                    if (theOrder.number == number) {
                        orders.splice(i, 1);
                        break;
                    }
                }

                var count = 1;
                for (var i = 0; i < orders.length; i++) {

                    var theOrder = orders[i];
                    theOrder.number = count++;
                }

                $scope.updateSummary();

            }


            $scope.newOrder = function () {
                var orderCount = $scope.trade.orders.length;


                //calculate new order risk

                var order = {
                    profitTarget: 0,
                    entryPrice: $scope.trade.entryPrice,
                    slPrice: $scope.trade.slPrice,
                    number: orderCount + 1,
                    size: 0.01,
                    profitTarget: 0,
                    risk: 0,
                    rmultiple: 0,
                    expectedReward: 0,
                    closePrice: 0,
                    openTime: $scope.trade.openTime,
                    openRisk: $scope.trade.openRisk,
                    riskAmmount: 0,
                    closeTime: "",
                    closeReward: 0,
                    spread: 0,
                    comission: 0,
                    swap: 0,
                    rollover: 0.0,
                    profitLoss: 0.0,
                    duration: 0,
                    adversePrice: 0,
                    maximumAdverseExcursion: 0,
                    transactionCostPips: 0,
                    transactionCostCurrency: 0,
                    afterCostsRmultiple: 0

                }

                var risk;
                risk = Math.abs($scope.trade.entryPrice - order.slPrice) / $scope.instrumentPipSize;
                risk = risk.toFixed(0);
                order.risk = risk;


                var slackRisk = new Number(order.risk) * $scope.instrumentPipSize;
                if ($scope.trade.type == 'SELL') {
                    slackRisk = -slackRisk;
                }


                var recommendedTakeProfit = (new Number(order.entryPrice) + slackRisk);


                var expectedReward = (Math.abs(recommendedTakeProfit - order.entryPrice) / $scope.instrumentPipSize).toFixed(0);
                order.profitTarget = recommendedTakeProfit.toFixed($scope.pipStringLength);
                order.expectedReward = expectedReward;

                var rmultiple = expectedReward / risk;

                order.rmultiple = rmultiple;
                $scope.trade.orders.push(order);
                $scope.$emit('ordersUpdated', order);

            }

        }
    ])
;


tradesControllers.controller("OrderController",
    [
        '$scope',

        function ($scope) {


            $scope.getEntryPrice = function () {
                return "" + $scope.order.entryPrice;
            };
            $scope.getSlPrice = function () {
                return "" + $scope.order.slPrice;
            }
            $scope.getProfitTarget = function () {
                return "" + $scope.order.profitTarget;
            }

            $scope.updateRisk = function () {
                if (!$scope.loadingFinished) {
                    return;
                }
                var risk;
                risk = Math.abs($scope.order.entryPrice - $scope.order.slPrice) / $scope.instrumentPipSize;
                risk = risk.toFixed(0);
                $scope.order.risk = risk;


            }
            $scope.updateOrder = function () {
                if (!$scope.loadingFinished) {
                    return;
                }
                if ($scope.instrumentSymbol && $scope.instrumentPipSize && $scope.instrumentPipPrice) {

                    var expectedReward = Math.abs(((new Number($scope.order.entryPrice) - $scope.order.profitTarget) / $scope.instrumentPipSize).toFixed(0));
                    $scope.order.expectedReward = expectedReward;


                    var riskAmmount = $scope.order.risk * $scope.order.size * $scope.instrumentPipPrice;


                    //$scope.order.accountAtRisk = riskAmmount / $scope.balanceSummary.balance;

                    if ($scope.order.closePrice > 0) {
                        if ($scope.trade.type == "SELL") {
                            var closeReward = (($scope.order.entryPrice - $scope.order.closePrice) / $scope.instrumentPipSize).toFixed(0);
                            $scope.order.closeReward = closeReward;
                        } else {
                            var closeReward = (($scope.order.closePrice - $scope.order.entryPrice ) / $scope.instrumentPipSize).toFixed(0);

                            $scope.order.closeReward = closeReward;
                        }
                    } else {
                        $scope.order.closeReward = 0;
                    }
                    var profitLoss = new Number($scope.order.rollover);
                    profitLoss += new Number($scope.order.comission);
                    profitLoss += new Number($scope.order.swap);
                    profitLoss += new Number($scope.order.spread * $scope.order.size * $scope.instrumentPipPrice);

                    var orderPL = ($scope.order.closeReward) * $scope.order.size * $scope.instrumentPipPrice;

                    profitLoss += orderPL;

                    var transactionCosts = new Number($scope.order.rollover);
                    transactionCosts += new Number($scope.order.comission);
                    transactionCosts += new Number($scope.order.swap);
                    transactionCosts += new Number($scope.order.spread * $scope.order.size * $scope.instrumentPipPrice);

                    $scope.order.transactionCostCurrency = transactionCosts.toFixed(2);
                    // $scope.order.transactionCostPips = (((transactionCosts * $scope.instrumentPipPrice) * $scope.instrumentPipSize ) * $scope.order.size).toFixed(2);

                    $scope.order.transactionCostPips = ((transactionCosts / $scope.instrumentPipPrice) / $scope.order.size).toFixed(0);


                    $scope.order.profitLoss = profitLoss.toFixed(3);
                    $scope.updateRisk();
                    $scope.updateRMultiple();


                    $scope.order.riskAmmount = ($scope.order.risk * $scope.instrumentPipPrice * $scope.order.size).toFixed(2);


                    $scope.$emit('ordersUpdated', $scope.order);


                }
            };

            $scope.updateRMultiple = function () {
                //Reafactor this

                var risk;
                var reward;
                if ($scope.order.openRisk > 0) {
                    risk = $scope.order.openRisk;
                } else {
                    risk = $scope.order.risk;
                }

                if ($scope.order.closePrice > 0) {
                    reward = new Number($scope.order.closeReward);
                } else {
                    reward = $scope.order.expectedReward;
                }
                rmultiple = (reward / risk).toFixed(2);
                $scope.order.afterCostsRmultiple = ((reward + new Number($scope.order.transactionCostPips)) / risk).toFixed(2);
                $scope.order.rmultiple = rmultiple;
            };

            $scope.$on('tradeUpdated', function (trade) {
                $scope.order.entryPrice = $scope.trade.entryPrice;
                $scope.order.slPrice = $scope.trade.slPrice;
                $scope.updateOrder();
            });


            $scope.$watch('order.adversePrice', function (newValue, oldValue) {

                if (newValue > 0 && $scope.trade.openRisk > 0) {

                    var variation = 0;
                    $scope.order.maximumAdverseExcursion = 0;
                    variation = $scope.trade.entryPrice - newValue;
                    variation = variation / $scope.instrumentPipSize;
                    $scope.order.maximumAdverseExcursion = Math.abs((variation / $scope.order.openRisk)).toFixed(2);
                    $scope.$emit('ordersUpdated', $scope.order);

                } else {
                    $scope.order.maximumAdverseExcursion = 0.0;
                    $scope.$emit('ordersUpdated', $scope.order);
                }


            });
            $scope.$watch('order.spread', function (newValue, oldValue) {
                $scope.$emit('ordersUpdated', $scope.order);

            });
            $scope.$watch('order.swap', function (newValue, oldValue) {
                $scope.$emit('ordersUpdated', $scope.order);

            });
            $scope.$watch('order.comission', function (newValue, oldValue) {
                $scope.$emit('ordersUpdated', $scope.order);

            });
            $scope.$watch('order.profitLoss', function (newValue, oldValue) {
                $scope.$emit('ordersUpdated', $scope.order);

            });

            $scope.$watch('order.closePrice', function (newValue, oldValue) {
                $scope.updateOrder();

            });


            $scope.$watch('order.profitTarget', function (newValue, oldValue) {

                $scope.updateOrder();

            });

            $scope.$watch('order.slPrice', function (newValue, oldValue) {
                $scope.updateOrder();

            });

            $scope.$watch('order.size', function (newValue, oldValue) {
                $scope.updateOrder();

            });
            $scope.$watch('order.rollover', function (newValue, oldValue) {
                $scope.updateOrder();

            });

            $scope.$watch('order.swap', function (newValue, oldValue) {
                $scope.updateOrder();

            });


            $scope.$watch('order.comission', function (newValue, oldValue) {
                $scope.updateOrder();

            });

            $scope.$watch('order.spread', function (newValue, oldValue) {
                $scope.updateOrder();

            });


        }]);


tradesControllers.controller("TradeViewCtrl",
    [
        'Trades',
        'Instruments',
        '$scope',
        '$routeParams',
        'growl',
        '$window',
        '$location',

        function (Trades, Instruments, $scope, $routeParams, growl, $window, $location) {


            if (!$routeParams.id || !$routeParams.accountId) {
                growl.addErrorMessage("No Account selected or symbol and trade!!");
            } else {
                var accountId = $routeParams.accountId;
                var tradeId = $routeParams.id;
                $scope.trade = Trades.get({accountId: accountId, id: tradeId });

            }


            $scope.save = function () {
                if ((!$routeParams.id || !$routeParams.accountId)) {
                    growl.addErrorMessage("No Account selected or Trade!!");

                } else {
                    var accountId = $routeParams.accountId;
                    var tradeId = $routeParams.id;
                    $scope.trade.id = tradeId;
                    $scope.trade.accountId = accountId;
                    Trades.save($scope.instrument);
                    $location.path("/accounts/" + accountId + "/trades");
                }
            }

            $scope.cancel = function () {
                if (!$routeParams.accountId) {
                    growl.addErrorMessage("No Account selected!!");
                } else {

                    var accountId = $routeParams.id || $routeParams.accountId;
                    $location.path("/accounts/:accountId/trades");
                }


            }

        }]);

