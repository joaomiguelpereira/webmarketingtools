'use strict';


// Declare app level module which depends on filters, and services
var accountTradesModule = angular.module('account.trades', [
    'application.providers',
    'account.trades.services',
    'account.trades.controllers',
    'accounts.services',
    'ngRoute',
    'ui.bootstrap',

    'ngClipboard'

]);

accountTradesModule.config(['$routeProvider',  function ($routeProvider) {

    $routeProvider.when('/accounts/:accountId/trades', {templateUrl:_$app.module('accounts/trades').viewPath('trades.html'), controller: 'TradesCtrl'});

    $routeProvider.when('/accounts/:accountId/trades/new', {templateUrl:_$app.module('accounts/trades').viewPath('trade.html'), controller: 'TradesCtrl'});
    $routeProvider.when('/accounts/:accountId/trades/:id', {templateUrl:_$app.module('accounts/trades').viewPath('trade.html'), controller: 'TradesCtrl'});


}]);

accountTradesModule.config(['ngClipProvider', function(ngClipProvider) {

    ngClipProvider.setPath("js/lib/zeroclipboard/ZeroClipboard.swf");
}]);
