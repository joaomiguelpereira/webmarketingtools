'use strict';

/* Controllers */

var tradesServices = angular.module('account.trades.services', ['ngResource']);


tradesServices.factory('Trades', ['$resource', function($resource) {
    return $resource("/api/trading/account/:accountId/trades/:id",{accountId: '@accountId', id: '@id'});

}]);

tradesServices.factory('TradeScreenshots', ['$resource', function($resource) {
    return $resource("/api/trading/account/:accountId/trades/:tradeId/screenshots/:id",{});
}]);




