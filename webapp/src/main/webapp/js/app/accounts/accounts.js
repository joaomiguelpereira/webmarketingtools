'use strict';


// Declare app level module which depends on filters, and services
var accountsModule = angular.module('accounts', [
    'application.providers',
    'accounts.controllers',
    'accounts.services',
    'account.trades.services',
    'ngRoute',
    'ui.bootstrap'

]);

accountsModule .config(['$routeProvider',  function ($routeProvider) {
    $routeProvider.when('/', {templateUrl:_$app.module('accounts').viewPath('accounts.html'), controller: 'AccountsCtrl'});
    $routeProvider.when('/accounts/new', {templateUrl:_$app.module('accounts').viewPath('new.html'), controller: 'AccountsNewCtrl'});
    $routeProvider.when('/accounts/:id', {templateUrl:_$app.module('accounts').viewPath('view.html'), controller: 'AccountsViewCtrl'});
    $routeProvider.when('/accounts/:id/edit', {templateUrl:_$app.module('accounts').viewPath('new.html'), controller: 'AccountsNewCtrl'});


    $routeProvider.when('/accounts/:id/risk', {templateUrl:_$app.module('accounts').viewPath('risk.html'), controller: 'AccountsRiskCtrl'});
    $routeProvider.when('/accounts/:id/plan', {templateUrl:_$app.module('accounts').viewPath('plan.html'), controller: 'AccountsPlanCtrl'});
    $routeProvider.when('/accounts/:id/movements', {templateUrl:_$app.module('accounts').viewPath('movements.html'), controller: 'MovementsCtrl'});
}]);
