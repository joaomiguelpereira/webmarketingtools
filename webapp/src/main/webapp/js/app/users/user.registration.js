'use strict';


// Declare app level module which depends on filters, and services
var userRegistrationModule = angular.module('user.registration', [
    'application.providers',
    'user.registration.controllers',
    'user.registration.services',
    'ngRoute',
    'ui.bootstrap'

]);

userRegistrationModule.config(['$routeProvider',  function ($routeProvider) {
    $routeProvider.when('/user/registration', {templateUrl:_$app.module('users/registration').viewPath('registration.html'), controller: 'UserRegistrationCtrl'});
}]);
