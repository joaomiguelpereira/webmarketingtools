'use strict';

/* Controllers */

var userRegistrationServices = angular.module('user.registration.services', ['ngResource']);


userRegistrationServices.factory('RegistrationCollection', ['$resource', function($resource) {
    return $resource("/api/users/registration");
}]);


