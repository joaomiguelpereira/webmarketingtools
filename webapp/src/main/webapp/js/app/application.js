'use strict';


// Declare app level module which depends on filters, and services
var pluralsenseApp = angular.module('application', [
    'ngRoute',
    'ui.bootstrap',
    'truncate',
    'application.providers',
    'application.controllers',
    'accounts',
    'trading.instruments',
    'account.trades',
    'twitter.application',
    'twitter.accounts',
    'angular-growl',
    'percentage',
    'ui.bootstrap',
    'angularFileUpload'

]);
pluralsenseApp.config(['$routeProvider', function ($routeProvider) {

    //Default view
    //$routeProvider.when('/', {templateUrl: _$app.module('').viewPath('home.html'), controller: 'HomeCtrl'});
    $routeProvider.otherwise({redirectTo: '/'});

}]);


pluralsenseApp.config(['growlProvider', function(growlProvider) {
    growlProvider.globalTimeToLive(5000);
}]);

pluralsenseApp.config(['$provide', '$httpProvider', 'growlProvider',function ($provide, $httpProvider, growlProvider) {



    // Intercept http calls.
    var myHttpInterceptor =  ['$q', 'growl', function ($q, growl) {
        return {
            // On request success
            request: function (config) {
                // Return the config or wrap it in a promise if blank.
                //Security
                var token = $("meta[name='_csrf']").attr("content");
                var header = $("meta[name='_csrf_header']").attr("content");

                config.headers[header] = token;
                return config || $q.when(config);
            },

            // On request failure
            requestError: function (rejection) {

                growl.addErrorMessage("Error");
                return $q.reject(rejection);
            },

            // On response success
            response: function (response) {
                // Return the response or promise.
                //console.dir(response);
                var data = response.data;
                if ( data && data.type && data.type == "TradingServerSuccessMessage") {
                    growl.addSuccessMessage(data.message);

                }
                if ( data && data.type && data.type == "TradingServerErrorMessage") {
                    growl.addErrorMessage(data.message);

                }


                if ( response.data )
                //

                /*if ( response.data.message ) {
                    //console.dir(growlProvider);

                    //_$app.publishSuccessMessage();
                } */
                return response || $q.when(response);
            },

            // On response failture
            responseError: function (rejection) {
                growl.addErrorMessage(rejection.data.message);
                return $q.reject(rejection);
            }
        };
    }];

    // Add the interceptor to the $httpProvider.
    $httpProvider.interceptors.push(myHttpInterceptor);


}]);
