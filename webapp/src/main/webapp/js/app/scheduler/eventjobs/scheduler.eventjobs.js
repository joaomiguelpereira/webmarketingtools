'use strict';


// Declare app level module which depends on filters, and services
var schedulerEventJobsModule = angular.module('scheduler.eventjobs', [
    'application.providers',
    'scheduler.eventjobs.controllers',
    'scheduler.eventjobs.services',
    'ngRoute',
    'ui.bootstrap'

]);

schedulerEventJobsModule.config(['$routeProvider',  function ($routeProvider) {
    $routeProvider.when('/scheduler/eventjobs', {templateUrl:_$app.module('scheduler/eventjobs').viewPath('eventjobs.html'), controller: 'EventJobsCtrl'});
}]);
