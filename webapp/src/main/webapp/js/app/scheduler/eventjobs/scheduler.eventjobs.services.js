'use strict';

/* Controllers */

var schedulerEventJobsService = angular.module('scheduler.eventjobs.services', ['ngResource']);


schedulerEventJobsService.factory('EventJobsCollection', ['$resource', function($resource) {
    return $resource("/api/scheduler/eventjobs/:id", {id: '@id'});
}]);


