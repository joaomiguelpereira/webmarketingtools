/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 1/21/14
 * Time: 9:35 PM
 * To change this template use File | Settings | File Templates.
 */


var schedulerEventJobsContoller = angular.module('scheduler.eventjobs.controllers', ['scheduler.eventjobs.services']);

/**
 * Accounts Controller
 */
schedulerEventJobsContoller.controller("EventJobsCtrl",
    ['EventJobsCollection',
        '$modal',
        '$scope',

        function (EventJobsCollection, $modal, $scope) {

            $scope.eventJobs = EventJobsCollection.query();

            $scope.viewEventJob = function(eventJob) {
                var viewModal = $modal.open({
                    templateUrl: 'viewEventJob.html',
                    controller: 'ViewEventJobModalInstanceCtrl',
                    resolve: {
                        eventJobInstance: function () {
                            eventJob.logEntries.reverse();
                            return eventJob

                        }
                    }
                });
                viewModal.result.then(function() {}, function() {})

            }

        }]);


twitterAccountsControllers.controller("ViewEventJobModalInstanceCtrl", ['$scope', '$modalInstance','eventJobInstance', function ($scope, $modalInstance, eventJobInstance) {

    $scope.eventJobInstance = eventJobInstance;
    $scope.cancel = function () {
        $modalInstance.dismiss();
    };


}]);


schedulerEventJobsContoller.controller("EventJobEntryCtrl",
    ['EventJobsCollection',
        '$scope',

        function (ActiveEventJobTrigger, $scope) {



        }]);





